<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Driftwood Distillery</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/fonts.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;900&family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="style.css">
  <!--- intlTelInput CSS for Country Code --->
  <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css">
  <link href="img/favicon/favicon.png" rel="apple-touch-icon" />
  <!-- FAVICONS -->
  <link rel="shortcut icon" href="img/favicon/favicon.png" type="image/png">
  <link rel="icon" href="img/favicon/favicon.png" type="image/png">
</head>

<body>
  <header>
    <div class="header-top">
      <div class="container">
        <div class="header-wrap">
          <div class="header-top-cont">
            <div class="row">
              <div class="col-lg-4 col-md-5 col-sm-6 col-6">
                <div class="header-contact">
                  <ul class="list-unstyled d-flex d-inline-flex">
                    <li><a href="https://goo.gl/maps/GCEgfU7tUfzwJzFz9"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
                    <li><a href="https://goo.gl/maps/GCEgfU7tUfzwJzFz9"><span>Driftwood Distillery & Bond, Lageweg 14A, 2222 LA Katwijk aan Zee, Netherlands</span></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-lg-4 col-md-3 d-none d-lg-block d-md-block d-none d-sm-none">
                <div class="header-logo text-center">
                  <a href="index.php"><img src="img/logo.png" class="img-fluid" alt="logo" /></a>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                <div class="header-contact hed-co">
                  <ul class="list-unstyled d-flex d-inline-flex">
                    <li><a href="tel:+31642066192"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                    <li><a href="tel:+31642066192"><span class="pho">+31642066192</span></a></li>
                    <!-- <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li> -->
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="mainmenu text-center">
            <nav class="navbar navbar-expand-md">
              <div class="container">
                <a class="navbar-brand d-block d-md-none d-lg-none d-sm-block" href="index.php"><img src="img/logo.png" class="img-fluid" alt="logo" /></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                      <a class="nav-link active" aria-current="page" href="index.php">Home </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#aboutSection">ABOUT </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#wholeSection">WHOLESALE </a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="https://www.driftwooddistillery.eu/collections/drinks">Our Products</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#ourSection"> SERVICES</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="apply"> APPLY</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#contactSection"> CONTACT</a>
                    </li>
                  </ul>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </div>

    <div class="header-bottom">
      <img src="img/banner/banner.jpg" class="img-fluid" width="100%" alt="banner">
      <div class="banner-contents text-center">
        <div class="container">
          <div class="banner-details">
            <div class="banner-info">
              <h2>Welcome To</h2>
              <h1>Driftwood </h1>
              <h3>Distillery </h3>
              <p>Turn your next drink idea into a reality and share your product with the world.</p>
              <p>Driftwood can bring your vision to life with our in-house teams. We are here to help you
                through the entire process of creating a unique spirit to complement your brand, from
                formulating the recipe and production to helping you market your newest drink.</p>
              <a href="#msform" class="btn">Get Started</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div class="holder">
    <div id="aboutSection" class="about-section">
      <div class="container">
        <div class="about-wrap">
          <div class="row">
            <div class="col-md-6">
              <div class="about-content heading text-left">
                <h3>ABOUT Driftwood Distillery</h3>
                <p>Driftwood Distillery, located in the Netherlands, has been mixing and creating
                  5-star-rated gin, cocktails, and making recipes since 2017. Now we want to use our
                  distilling expertise to help you launch your very own brand of
                  liquor.<br><br>Through our vast network, we have exclusive access to some of the
                  best whiskies, vodka, rum, gins, and wines in the world. We work directly with you
                  to develop the perfect recipe and produce your drink.</p>
                <a href="#ourSection" class="btn">Learn More</a>
              </div>
            </div>
            <div class="col-md-6">
              <div class="about-img text-center">
                <img src="img/others/about-img.png" class="img-fluid" alt="about-img" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div id="wholeSection" class="whole-section">
      <div class="container">
        <div class="whole-heading heading text-center">
          <h3>WHOLESALE</h3>
          <p>We have exclusive access to spirits from world-class distilleries globally producing
            award-winning beverages. We can supply large volumes of high-end spirits worldwide if you wish
            to bottle them in your facility.</p>
        </div>
        <div class="whole-sec-wrap heading text-center">
          <div class="row">
            <div class="col-md-4 col-sm-6 col-12">
              <div class="whole-content-wrap">
                <div class="whole-img">
                  <img src="img/others/whole-sale-img-1.png" class="img-fluid" alt="whole-sale-img" />
                </div>
                <div class="whole-content">
                  <h4>Whiskies</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
              <div class="whole-content-wrap">
                <div class="whole-img">
                  <img src="img/others/whole-sale-img-2.png" class="img-fluid" alt="whole-sale-img" />
                </div>
                <div class="whole-content">
                  <h4>Vodka</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
              <div class="whole-content-wrap">
                <div class="whole-img">
                  <img src="img/others/whole-sale-img-3.png" class="img-fluid" alt="whole-sale-img" />
                </div>
                <div class="whole-content">
                  <h4>Rum</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
              <div class="whole-content-wrap">
                <div class="whole-img">
                  <img src="img/others/whole-sale-img-4.png" class="img-fluid" alt="whole-sale-img" />
                </div>
                <div class="whole-content">
                  <h4>Gins</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
              <div class="whole-content-wrap">
                <div class="whole-img">
                  <img src="img/others/whole-sale-img-5.png" class="img-fluid" alt="whole-sale-img" />
                </div>
                <div class="whole-content">
                  <h4>Wines</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
              <div class="whole-content-wrap">
                <div class="whole-img">
                  <img src="img/others/whole-sale-img-6.png" class="img-fluid" alt="whole-sale-img" />
                </div>
                <div class="whole-content">
                  <h4>Champagnes</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="ourSection" class="our-section">
      <div class="container">
        <div class="our-heading heading text-center">
          <h3>Our SERVICES</h3>
        </div>
        <div class="our-wrap">
          <div class="row">
            <div class="col-md-6 cont-left">
              <div class="our-cont-wrap cont-1">
                <div class="row">
                  <div class="col-md-9 col-sm-12">
                    <div class="our-content our-con-1">
                      <h4>Concept to Shelf</h4>
                      <p>Our dedicated team assists you through the entire drink-making process to
                        perfect your vision.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-12 col-12">
                    <div class="our-icon">
                      <img src="img/icons/our-icon-3.png" class="img-fluid" alt="our-icon" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="our-cont-wrap cont-2">
                <div class="row">
                  <div class="col-md-9 col-sm-12 col-12">
                    <div class="our-content">
                      <h4>Trial Production</h4>
                      <p>Our team works with you to perfect your drink recipe to deliver the exact
                        taste you're looking for.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-12 col-12">
                    <div class="our-icon">
                      <img src="img/icons/our-icon-2.png" class="img-fluid" alt="our-icon" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="our-cont-wrap cont-3">
                <div class="row">
                  <div class="col-md-9 col-sm-12 col-12 cont3-lg-left">
                    <div class="our-content">
                      <h4>Recipe Development</h4>
                      <p>Our team works with you to perfect your drink recipe to deliver the exact
                        taste you're looking for.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-12 col-12">
                    <div class="our-icon">
                      <img src="img/icons/our-icon-1.png" class="img-fluid" alt="our-icon" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 cont-right">
              <div class="our-cont-wrap cont-4">
                <div class="row">

                  <div class="col-md-9 d-block d-lg-none d-md-none d-block d-sm-block">
                    <div class="our-content our-con-4">
                      <h4>Small Batch & Spirit Bottling</h4>
                      <p>As a fully operational distillery, we can continue to produce small
                        batches of your drink and bottle design.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-12 col-12">
                    <div class="our-icon">
                      <img src="img/icons/our-icon-4.png" class="img-fluid" alt="our-icon" />
                    </div>
                  </div>
                  <div class="col-md-9 d-none d-lg-block d-md-block d-none d-sm-none">
                    <div class="our-content our-con-4">
                      <h4>Small Batch & Spirit Bottling</h4>
                      <p>As a fully operational distillery, we can continue to produce small
                        batches of your drink and bottle design.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="our-cont-wrap cont-5">
                <div class="row">
                  <div class="col-md-9 col-sm-12 col-12 d-block d-lg-none d-md-none d-block d-sm-block">
                    <div class="our-content">
                      <h4>Canning Services</h4>
                      <p>We now also offer canning services for things like ready-made drinks in
                        various sizes.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-12 col-12">
                    <div class="our-icon">
                      <img src="img/icons/our-icon-5.png" class="img-fluid" alt="our-icon" />
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-12 col-12 d-none d-lg-block d-md-block d-none d-sm-none">
                    <div class="our-content">
                      <h4>Canning Services</h4>
                      <p>We now also offer canning services for things like ready-made drinks in
                        various sizes.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="our-cont-wrap cont-6">
                <div class="row">
                  <div class="col-md-9 col-sm-12 col-12 d-block d-lg-none d-md-none d-block d-sm-block">
                    <div class="our-content">
                      <h4>Branding</h4>
                      <p>We can help create a branding and marketing plan for your new product
                        based on your target demographic.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                  <div class="col-md-3 col-sm-12 col-12">
                    <div class="our-icon">
                      <img src="img/icons/our-icon-6.png" class="img-fluid" alt="our-icon" />
                    </div>
                  </div>
                  <div class="col-md-9 col-sm-12 col-12 d-none d-lg-block d-md-block d-none d-sm-none cont6-lg-right">
                    <div class="our-content">
                      <h4>Branding</h4>
                      <p>We can help create a branding and marketing plan for your new product
                        based on your target demographic.</p>
                      <a href="#faqSection" class="btn2">Learn More</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="faqSection" class="faq-section">
      <div class="faq-heading heading  text-center">
        <h3>Frequently Asked Questions</h3>
      </div>
      <div class="container">
        <div class="faq-wrap-cont">
          <div class="accordion" id="accordionExample">
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><span>01</span>
                  Where is Driftwood Distillery based?
                </button>
              </h2>
              <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <p>Driftwood Distillery is a craft gin distillery based on an old naval airbase near
                    Katwijk in the Netherlands.</p>
                  <p>Driftwood Distillery Building 326, Vliegkamp Valkenburg, 1e Mientlaan, Katwijk,
                    2223LA</p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                  <span>02</span>What if I am outside of the Netherlands? Can you still create my
                  drink?
                </button>
              </h2>
              <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <p>We serve customers from all over the world we can operate online and if required
                    we can visit you. We would like to welcome you to our location.</p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingThree">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><span>03</span>Is it required to have experience in
                  the Beverage industry to make a drink?
                </button>
              </h2>
              <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <p>Experience is absolutely not necessary. You have the desire, the idea and/or the
                    vision. We have the expertise to bring it to life.</p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingfour">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour"><span>04</span>What types of marketing and branding do
                  you offer?
                </button>
              </h2>
              <div id="collapsefour" class="accordion-collapse collapse" aria-labelledby="headingfour" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <p>In meetings with our team, we will help you workshop the perfect branding plan to
                    help get your product in front of the consumers' eyes in the best way possible.
                    We can assist you with everything from logo design to a marketing
                    plan.<br><br>Branding is arguably the most significant part of the process. You
                    can make the best-tasting drink in the world, but people will only know that if
                    they can find it.</p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingfive">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive"><span>05</span>Can your team help me meet the legal
                  requirements for selling alcohol?
                </button>
              </h2>
              <div id="collapsefive" class="accordion-collapse collapse" aria-labelledby="headingfive" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <p>For sure, per country we are always aware of the requirements.</p>
                </div>
              </div>
            </div>
            <div class="accordion-item">
              <h2 class="accordion-header" id="headingsix">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix"><span>06</span>What drinks can Driftwood Distillery help
                  me create?
                </button>
              </h2>
              <div id="collapsesix" class="accordion-collapse collapse" aria-labelledby="headingsix" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                  <p>Our offer is large and diverse, thanks to our extensive network. We would be
                    happy to hear your wishes.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div id="contactSection" class="contact-section">
      <div class="container">
        <div class="contact-heading heading text-center">
          <h3><span class="text-col"> <span class="text-bg-img">Contact us</h3>
        </div>
        <div class="contact-wrap">
          <div class="row">
            <div class="col-md-5 col-lg-4">

              <div class="contact-us">
                <ul class="list-unstyled d-flex d-inline-flex">
                  <li><a href="https://goo.gl/maps/GCEgfU7tUfzwJzFz9"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
                  <li><a href="https://goo.gl/maps/GCEgfU7tUfzwJzFz9">Location<span>Driftwood Distillery & Bond, Lageweg 14A, 2222 LA Katwijk aan Zee, Netherlands</span></a></li>
                </ul>
              </div>

              <div class="contact-us">
                <ul class="list-unstyled d-flex d-inline-flex">
                  <li><a href="tel:31642066192"><i class="fa fa-phone" aria-hidden="true"></i></a>
                  </li>
                  <li><a href="tel:31642066192">Phone<span>+31642066192</span></a></li>
                </ul>
              </div>

              <div class="contact-us">
                <ul class="list-unstyled d-flex d-inline-flex">
                  <li><a href="mailto:info@driftwooddistillery.com"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
                  <li><a href="mailto:info@driftwooddistillery.com">Email<span>info@driftwooddistillery.nl</span></a>
                  </li>
                </ul>
              </div>

            </div>
            <div class="col-md-7 col-lg-8">
              <div class="contact-form">
                <form id="msform" action="" method="post" autocomplete="off">
                  <div class="row">
                    <div class="col-md-6">
                      <div id="firstNameDiv" class="form-group">
                        <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name " required />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div id="lastNameDiv" class="form-group">
                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" required />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Company" required />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div id="phoneDiv" class="form-group">
                        <input type="text" name="phone" id="phone" class="form-control" placeholder="Telephone" />
                        <input type="hidden" name="country_code" id="country_code" value="44" />
                        <input type="hidden" name="country_name" id="country_name" value="United Kingdom" placeholder="Country">
                        <span id="valid-msg" class="hide">✓ Valid</span>
                        <span id="error-msg" class="hide"></span>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div id="emailDiv" class="form-group">
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <textarea name="message" id="message" class="form-control" placeholder="Message "></textarea>
                      </div>
                    </div>
                    <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>" />
                    <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>" />
                    <div class="col-md-6">
                      <div class="form-group1">
                        <button type="submit" id="btn-submit" class="btn">Submit</button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



  </div>

  <footer>
    <div class="footer-section">
      <div class="container">
        <div class="footer-wrap">
          <div class="row">
            <div class="col-md-3 col-lg-3 col-sm-12 col-12">
              <div class="footer-logo heading">
                <a href="index.php"><img src="img/footer-logo.png" class="img-fluid" alt="footer-logo" /></a>
                <p>Driftwood Distillery is a craft gin distillery based on an old naval airbase near
                  Katwijk in the Netherlands.</p>
              </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-12 col-12">
              <div class="footer-menu menu-1">
                <h4>Quick Links</h4>
                <img src="img/others/strip-1.png" class="img-fluid" alt="strip-img" />
                <ul class="list-unstyled">
                  <li><a href="index.php" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>Home</a></li>
                  <li><a href="#aboutSection"><i class="fa fa-chevron-right" aria-hidden="true"></i>About</a></li>
                  <li><a href="#wholeSection"><i class="fa fa-chevron-right" aria-hidden="true"></i>Wholesale</a></li>
                  <li><a href="https://www.driftwooddistillery.eu/collections/drinks"><i class="fa fa-chevron-right" aria-hidden="true"></i>Our Products</a></li>
                  <li><a href="#ourSection"><i class="fa fa-chevron-right" aria-hidden="true"></i>Services</a></li>
                  <li><a href="#contactSection"><i class="fa fa-chevron-right" aria-hidden="true"></i>Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-12 col-12">
              <div class="footer-menu menu-2">
                <h4>Follow us</h4>
                <img src="img/others/strip-1.png" class="img-fluid" alt="strip-img" />
                <ul class="list-unstyled">
                  <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i>Linkedin</a></li>
                  <li><a href="#"><i class="fab fa-tiktok"></i>Tiktok</a></li>
                  <li><a href="https://www.facebook.com/driftwooddistillery/"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a></li>
                  <li><a href="https://www.instagram.com/driftwooddistillery/"><i class="fa fa-instagram" aria-hidden="true"></i>Instagram</a></li>
                  <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-12 col-12">
              <div class="newsletter footer-menu">
                <h4>NewsLetter Updates</h4>
                <img src="img/others/strip-1.png" class="img-fluid" alt="strip-img" />
                <form>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Name*" required>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Email* " required>
                  </div>
                  <div class="form-group">
                    <a href="#" class="btn">Submit Now</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="copyrights text-center">
          <p>&copy; Copyrights 2023 <span>Driftwood Distillery </span> All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>



  <!-- JavaScript Libraries -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script src="https://kit.fontawesome.com/d547ebcf09.js" crossorigin="anonymous"></script>
  <!--- teleinput for country code js --->
  <script src="intlTelInput/js/intlTelInput.min.js"></script>
  <script src="intlTelInput/js/utils.js"></script>
  <script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=MQ8R-3CYE-EY32-ZRWE&load=InternationalTelephoneValidation,EmailValidation">
  </script>
  <script type="text/javascript">
    var validEmail = false;
    var validPhone = false;
    $("#btn-submit").click(function(e) {
      var first_name = $("#first_name").val();
      var last_name = $("#last_name").val();
      var email = $("#email").val();
      var phoneNumber = $("#phone").val();

      if (first_name.length <= 1) {
        $('#firstNameDiv p').remove('');
        $('#firstNameDiv').append(
          '<p style="color: #dc3545;font-size: 15px;" >First Name is required!</p>');
        return false;
      } else {
        $('#firstNameDiv p').remove('');
      }
      if (last_name.length <= 1) {
        $('#lastNameDiv p').remove('');
        $('#lastNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >Last Name is required!</p>');
        return false;
      } else {
        $('#lastNameDiv p').remove('');
      }
      if (email.length <= 1) {
        $('#emailDiv p').remove('');
        $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Email is required!</p>');
        return false;
      } else {
        $('#emailDiv p').remove('');
      }
      if (phoneNumber.length <= 1) {
        $('#phoneDiv p').remove('');
        $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Phone Number is required!</p>');
        return false;
      } else {
        $('#phoneDiv p').remove('');
      }

      if (validEmail == true && validPhone == true) {
        $.ajax({
          type: "POST",
          url: "formToLead.php",
          data: {
            first_name: $("#first_name").val(),
            last_name: $("#last_name").val(),
            email: $("#email").val(),
            phone: $("#phone").val(),
            country: $("#country_name").val(),
            description: $("#message").val(),
            lid: $("#lid").val(),
            aid: $("#aid").val()
          },
          success: function(result) {
            console.log(result);
            if (result == "success") {
              location.href = "index.php";
            }
          },
          error: function(result) {
            console.log(result);
            location.href = "index.php";
            // location.href = "Scottish Whisky Investment Prospectus.pdf";
          }
        });
      }
    });

    $(document).ready(function() {
      //country code script
      var input = document.querySelector("#phone"),
        errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg");

      // Here, the index maps to the error code returned from getValidationError
      var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

      // Get country code
      var countryData = window.intlTelInputGlobals.getCountryData(),
        country_code = document.querySelector("#country_code");
      // Initialise plugin
      var iti = window.intlTelInput(input, {
        utilsScript: "intlTelInput/js/utils.js",
        initialCountry: 'gb'
      });
      // Set it's initial value
      var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
      // console.log(all_countrycode);

      // Listen to the telephone input for changes
      input.addEventListener('countrychange', function(e) {
        var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
        // console.log(selected_countrycode);
        var selected_countryname = country_code.value = iti.getSelectedCountryData().name;
        // console.log(selected_countryname);
        $("#country_code").val(selected_countrycode);
        $("#country_name").val(selected_countryname);
      });

      var reset = function() {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
      };

      // On blur: validate
      input.addEventListener('blur', function() {
        reset();
      });

      // on keyup / change flag: reset
      input.addEventListener('change', reset);
      input.addEventListener('keyup', reset);

      // Validate email
      $("#email").change(function() {
        var email = $("#email").val();
        level = 'Address';
        // alert(email);
        var emailvalidation = new data8.emailvalidation();
        emailvalidation.isvalid(
          email,
          level,
          [

          ],
          showIsEmailValid
        );
      });

      function showIsEmailValid(result) {
        console.log(result.Result);
        if (result.Result == 'Valid') {
          validEmail = true;
          $('#emailDiv p').remove('');
          $("#btn-submit").attr('disabled', false);
        } else {
          validEmail = false;
          $('#emailDiv p').remove('');
          $('#emailDiv').append(
            '<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Email</p>');
          $("#btn-submit").attr('disabled', true);
        }
      }

      $('select#country_code').on('change', function() {
        var country_code = $(this).children("option:selected").text();
        var code = country_code.replace('+', '');
        // console.log(code);
        $('#country_code').val(code);
      })

      $("#phone").bind("change", function(e) {
        var telephoneNumber = $("#phone").val();
        if ($("#country_code").val() == 'gb') {
          defaultCountry = 'GB';
          var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
          internationaltelephonevalidation.isvalid(
            telephoneNumber,
            defaultCountry,
            [
              new data8.option('UseMobileValidation', 'false'),
              new data8.option('UseLineValidation', 'false'),
              new data8.option('RequiredCountry', ''),
              new data8.option('AllowedPrefixes', ''),
              new data8.option('BarredPrefixes', ''),
              new data8.option('UseUnavailableStatus', 'true'),
              new data8.option('UseAmbiguousStatus', 'true'),
              new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
              new data8.option('ExcludeUnlikelyNumbers', 'false')
            ],
            showIsPhoneNumberValid
          );
        } else {
          defaultCountry = 'GB';
          var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
          internationaltelephonevalidation.isvalid(
            telephoneNumber,
            $("#country_code").val(),
            [
              new data8.option('UseMobileValidation', 'false'),
              new data8.option('UseLineValidation', 'false'),
              new data8.option('RequiredCountry', ''),
              new data8.option('AllowedPrefixes', ''),
              new data8.option('BarredPrefixes', ''),
              new data8.option('UseUnavailableStatus', 'true'),
              new data8.option('UseAmbiguousStatus', 'true'),
              new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
              new data8.option('ExcludeUnlikelyNumbers', 'false')
            ],
            showIsPhoneNumberValid
          );
        }
      });

      function showIsPhoneNumberValid(result) {
        console.log(result.Result.ValidationResult);
        if (result.Result.ValidationResult == 'Valid') {
          validPhone = true;
          $('#phoneDiv p').remove('');
          $("#btn-submit").attr('disabled', false);
        } else {
          validPhone = false;
          $('#phoneDiv p').remove('');
          $('#phoneDiv').append(
            '<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Phone Number</p>');
          $("#btn-submit").attr('disabled', true);
        }
      }
    });
  </script>
</body>

</html>
