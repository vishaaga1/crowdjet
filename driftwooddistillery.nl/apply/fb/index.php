<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Driftwood Distillery</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/fonts.css">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@400;500;600;700&family=Montserrat:wght@400;500;600;700;900&family=Roboto:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="../style.css">
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="../img/favicon/favicon.png" type="image/png">
    <link rel="icon" href="../img/favicon/favicon.png" type="image/png">
</head>

<body>
    <header>
        <div class="header-top inner-page">
            <div class="container">
                <div class="header-wrap">
                    <div class="header-top-cont">
                        <div class="row">
                            <div class="col-lg-4 col-md-5 col-sm-6 col-6">
                                <div class="header-contact">
                                    <ul class="list-unstyled d-flex d-inline-flex">
                                        <li><a href="https://goo.gl/maps/GCEgfU7tUfzwJzFz9"><i class="fa fa-map-marker" aria-hidden="true"></i></a></li>
                                        <li><a href="https://goo.gl/maps/GCEgfU7tUfzwJzFz9"><span>Driftwood Distillery & Bond, Lageweg 14A, 2222 LA Katwijk aan Zee, Netherlands</span></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-3 d-none d-lg-block d-md-block d-none d-sm-none">
                                <div class="header-logo text-center">
                                    <a href="../index.php"><img src="../img/logo.png" class="img-fluid" alt="logo" /></a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-6">
                                <div class="header-contact hed-co">
                                    <ul class="list-unstyled d-flex d-inline-flex">
                                        <li><a href="tel:+31642066192"><i class="fa fa-phone" aria-hidden="true"></i></a></li>
                                        <li><a href="tel:+31642066192"><span class="pho">+31642066192</span></a></li>
                                        <li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mainmenu text-center">
                        <nav class="navbar navbar-expand-md">
                            <div class="container">
                                <a class="navbar-brand d-block d-md-none d-lg-none d-sm-block" href="../index.php"><img src="../img/logo.png" class="img-fluid" alt="logo" /></a>
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                        <li class="nav-item">
                                            <a class="nav-link" href="../../index.php">Home </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="../../index.php#aboutSection">ABOUT </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="../../index.php#wholeSection">WHOLESALE </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="https://www.driftwooddistillery.eu/collections/drinks">Our
                                                Products</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="../../index.php#ourSection"> SERVICES</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" aria-current="page" href="#"> APPLY</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="../../index.php#contactSection"> CONTACT</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-bottom">
            <img src="../img/banner/banner-apply.jpg" class="img-fluid" width="100%" alt="banner">
            <div class="banner-contents text-center">
                <div class="container">
                    <div class="banner-details">
                        <div class="banner-info">
                            <h1>APPLY </h1>
                            <p>If you think you have one of the next great spirits brands, we'd love to hear from you.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </header>

    <div class="holder">



        <div class="apply-section">
            <div class="container">
                <div class="apply-form heading text-center">
                    <h5>ABOUT THE BRAND</h5>
                    <div class="apply-fields">
                        <form id="msform" enctype="multipart/form-data" method="post" action="../formToLead.php" autocomplete="off">
                            <div class="form-group">
                                <label>We’d love to hear about what you’re building directly from you.
                                    Please upload either a 1 minute video of you or your brand deck. No production or
                                    editing needed*</label>
                                <input type="file" id="brand_deck" name="brand_deck">
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="brand_name">Brand Name*</label>
                                        <input type="text" name="brand_name" id="brand_name" required />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Website (Please provide protocol, eg. 'https://' or http://':*</label>
                                        <input type="url" placeholder="" name="website" id="website" required />
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="instagram">Social Media (Please provide protocol, eg. 'https://' or
                                            'http://':</label>
                                        <input type="url" placeholder="" name="instagram" id="instagram">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="other_social_media">Other Social Media:</label>
                                        <textarea name="social"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="product_stage">What stage is your product at?*</label>
                                        <select name="product_stage" id="product_stage" required />
                                        <option disabled="" selected="" value="">Please Select</option>
                                        <option value="Concept / product in development">Concept / product in
                                            development
                                        </option>
                                        <option value="Full product ready / launch planned">Full product ready / launch
                                            planned
                                        </option>
                                        <option value="Product in the market">Product in the market</option>
                                        <option value="Been in the market for more than 6 months">Been in the market for
                                            more
                                            than 6 months</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="year_launched">Year Launched:</label>
                                        <select name="year_launched" id="year_launched">
                                            <option value="1873">1873</option>
                                            <option value="1874">1874</option>
                                            <option value="1875">1875</option>
                                            <option value="1876">1876</option>
                                            <option value="1877">1877</option>
                                            <option value="1878">1878</option>
                                            <option value="1879">1879</option>
                                            <option value="1880">1880</option>
                                            <option value="1881">1881</option>
                                            <option value="1882">1882</option>
                                            <option value="1883">1883</option>
                                            <option value="1884">1884</option>
                                            <option value="1885">1885</option>
                                            <option value="1886">1886</option>
                                            <option value="1887">1887</option>
                                            <option value="1888">1888</option>
                                            <option value="1889">1889</option>
                                            <option value="1890">1890</option>
                                            <option value="1891">1891</option>
                                            <option value="1892">1892</option>
                                            <option value="1893">1893</option>
                                            <option value="1894">1894</option>
                                            <option value="1895">1895</option>
                                            <option value="1896">1896</option>
                                            <option value="1897">1897</option>
                                            <option value="1898">1898</option>
                                            <option value="1899">1899</option>
                                            <option value="1900">1900</option>
                                            <option value="1901">1901</option>
                                            <option value="1902">1902</option>
                                            <option value="1903">1903</option>
                                            <option value="1904">1904</option>
                                            <option value="1905">1905</option>
                                            <option value="1906">1906</option>
                                            <option value="1907">1907</option>
                                            <option value="1908">1908</option>
                                            <option value="1909">1909</option>
                                            <option value="1910">1910</option>
                                            <option value="1911">1911</option>
                                            <option value="1912">1912</option>
                                            <option value="1913">1913</option>
                                            <option value="1914">1914</option>
                                            <option value="1915">1915</option>
                                            <option value="1916">1916</option>
                                            <option value="1917">1917</option>
                                            <option value="1918">1918</option>
                                            <option value="1919">1919</option>
                                            <option value="1920">1920</option>
                                            <option value="1921">1921</option>
                                            <option value="1922">1922</option>
                                            <option value="1923">1923</option>
                                            <option value="1924">1924</option>
                                            <option value="1925">1925</option>
                                            <option value="1926">1926</option>
                                            <option value="1927">1927</option>
                                            <option value="1928">1928</option>
                                            <option value="1929">1929</option>
                                            <option value="1930">1930</option>
                                            <option value="1931">1931</option>
                                            <option value="1932">1932</option>
                                            <option value="1933">1933</option>
                                            <option value="1934">1934</option>
                                            <option value="1935">1935</option>
                                            <option value="1936">1936</option>
                                            <option value="1937">1937</option>
                                            <option value="1938">1938</option>
                                            <option value="1939">1939</option>
                                            <option value="1940">1940</option>
                                            <option value="1941">1941</option>
                                            <option value="1942">1942</option>
                                            <option value="1943">1943</option>
                                            <option value="1944">1944</option>
                                            <option value="1945">1945</option>
                                            <option value="1946">1946</option>
                                            <option value="1947">1947</option>
                                            <option value="1948">1948</option>
                                            <option value="1949">1949</option>
                                            <option value="1950">1950</option>
                                            <option value="1951">1951</option>
                                            <option value="1952">1952</option>
                                            <option value="1953">1953</option>
                                            <option value="1954">1954</option>
                                            <option value="1955">1955</option>
                                            <option value="1956">1956</option>
                                            <option value="1957">1957</option>
                                            <option value="1958">1958</option>
                                            <option value="1959">1959</option>
                                            <option value="1960">1960</option>
                                            <option value="1961">1961</option>
                                            <option value="1962">1962</option>
                                            <option value="1963">1963</option>
                                            <option value="1964">1964</option>
                                            <option value="1965">1965</option>
                                            <option value="1966">1966</option>
                                            <option value="1967">1967</option>
                                            <option value="1968">1968</option>
                                            <option value="1969">1969</option>
                                            <option value="1970">1970</option>
                                            <option value="1971">1971</option>
                                            <option value="1972">1972</option>
                                            <option value="1973">1973</option>
                                            <option value="1974">1974</option>
                                            <option value="1975">1975</option>
                                            <option value="1976">1976</option>
                                            <option value="1977">1977</option>
                                            <option value="1978">1978</option>
                                            <option value="1979">1979</option>
                                            <option value="1980">1980</option>
                                            <option value="1981">1981</option>
                                            <option value="1982">1982</option>
                                            <option value="1983">1983</option>
                                            <option value="1984">1984</option>
                                            <option value="1985">1985</option>
                                            <option value="1986">1986</option>
                                            <option value="1987">1987</option>
                                            <option value="1988">1988</option>
                                            <option value="1989">1989</option>
                                            <option value="1990">1990</option>
                                            <option value="1991">1991</option>
                                            <option value="1992">1992</option>
                                            <option value="1993">1993</option>
                                            <option value="1994">1994</option>
                                            <option value="1995">1995</option>
                                            <option value="1996">1996</option>
                                            <option value="1997">1997</option>
                                            <option value="1998">1998</option>
                                            <option value="1999">1999</option>
                                            <option value="2000">2000</option>
                                            <option value="2001">2001</option>
                                            <option value="2002">2002</option>
                                            <option value="2003">2003</option>
                                            <option value="2004">2004</option>
                                            <option value="2005">2005</option>
                                            <option value="2006">2006</option>
                                            <option value="2007">2007</option>
                                            <option value="2008">2008</option>
                                            <option value="2009">2009</option>
                                            <option value="2010">2010</option>
                                            <option value="2011">2011</option>
                                            <option value="2012">2012</option>
                                            <option value="2013">2013</option>
                                            <option value="2014">2014</option>
                                            <option value="2015">2015</option>
                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                            <option disabled="" selected="" value="">Please Select</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label>Please choose the relevant categories:*</label>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_whisky" name="relevant_categories[]" value="Whisk(e)y" data-dropdown="whisky-sub">
                                                    <label for="relevant_categories_whisky">Whisk(e)y</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_rum" name="relevant_categories[]" value="Rum" data-dropdown="rum-sub">
                                                    <label for="relevant_categories_rum">Rum</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_tequila" name="relevant_categories[]" value="Tequila">
                                                    <label for="relevant_categories_tequila">Tequila</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_mezcal" name="relevant_categories[]" value="Other agave spirit">
                                                    <label for="relevant_categories_mezcal">Other agave spirit</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_gin" name="relevant_categories[]" value="Gin" data-dropdown="gin-sub">
                                                    <label for="relevant_categories_gin">Gin</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_vodka" name="relevant_categories[]" value="Vodka">
                                                    <label for="relevant_categories_vodka">Vodka</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_liqueur" name="relevant_categories[]" value="Liqueur">
                                                    <label for="relevant_categories_liqueur">Liqueur</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_ready_to_drink" name="relevant_categories[]" value="RTD/Premixed Cocktail">
                                                    <label for="relevant_categories_ready_to_drink">RTD/Premixed
                                                        Cocktail</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_brandy" name="relevant_categories[]" value="Brandy / Cognac">
                                                    <label for="relevant_categories_brandy">Brandy / Cognac</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_aperitif" name="relevant_categories[]" value="Aperitif / Fortified Wine">
                                                    <label for="relevant_categories_aperitif">Aperitif / Fortified
                                                        Wine</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="check-cont">
                                                    <input type="checkbox" id="relevant_categories_non_alcoholic spirit" name="relevant_categories[]" value="Non Alcoholic Spirit/Aperitif">
                                                    <label for="relevant_categories_non_alcoholic spirit">Non Alcoholic
                                                        Spirit/Aperitif</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group check-cont">
                                        <input type="checkbox" id="relevant_categories_other" name="relevant_categories[]" value="Other">
                                        <label for="relevant_categories_other">Other (Please specify below):</label>
                                        <textarea placeholder="" name="relevant_categories_other_description"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Where is your business located?</label>
                                        <input type="text" name="location" id="location">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>How many founders are there?</label>
                                        <select name="founders_count" id="founders_count">
                                            <option selected="" value="">Please Select</option>
                                            <option value="One">One</option>
                                            <option value="Two">Two</option>
                                            <option value="Three">Three</option>
                                            <option value="More than three">More than three</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="pre_accel">Are you applying for the Investment?</label>
                                        <select name="pre_accel" id="pre_accel">
                                            <option selected="" value="">Please Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="is_certified">Is your brand certified as diverse by any independent
                                            institutions?:</label>
                                        <select name="is_certified" id="is_certified">
                                            <option selected="" value="">Please Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="is_diverse">Do you consider your company to be a diverse business
                                            because at least 51% is owned and operated by one or more underrepresented
                                            communitie(s)?</label>
                                        <select name="is_diverse" id="is_diverse">
                                            <option selected="" value="">Please Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5>About you</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="first_name">Contact First Name:*</label>
                                        <input type="text" name="first_name" id="first_name" required />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="last_name">Contact Last Name:*</label>
                                        <input type="text" name="last_name" id="last_name" required />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">Country:*</label>
                                        <input type="text" name="country" id="country" required />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="emailDiv" class="form-group">
                                        <label for="email">Email Address:*</label>
                                        <input type="email" name="email" id="email" required />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="bio">Please provide your brief bio (background, experience):</label>
                                        <textarea maxlength="750" name="bio" id="bio" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5>BRAND INFO</h5>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="about_you">Tell us a bit about you and your founding team:*</label>
                                        <textarea maxlength="750" placeholder="What's your story?" name="about_you" id="about_you" rows="5" required /></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="liquid_details">Briefly describe your liquid:*</label>
                                        <textarea placeholder="How is your liquid different and how should people drink it?" maxlength="750" name="liquid_details" id="liquid_details" rows="5" required /></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="about_brand">What are you trying to change or solve through your
                                            brand?:*</label>
                                        <textarea maxlength="750" placeholder="How would you describe your brand to a customer" name="about_brand" id="about_brand" rows="5" required /></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="audience">Who's your audience or who do you plan to focus on in the
                                            future?:</label>
                                        <textarea maxlength="750" name="audience" id="audience" rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="is_available_for_sale">Do you currently have product that is
                                            available for sale?</label>
                                        <select name="is_available_for_sale" id="is_available_for_sale">
                                            <option selected="" value="">Please Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="currency">Currency</label>
                                        <select name="currency" id="currency">
                                            <option selected="" value="">Please Select</option>
                                            <option value="GBP">United Kingdom Pounds (GBP)</option>
                                            <option value="EUR">Euro (EUR)</option>
                                            <option value="USD">United States Dollars (USD)</option>
                                            <option value="DZD">Algeria Dinars (DZD)</option>
                                            <option value="ARP">Argentina Pesos (ARP)</option>
                                            <option value="AUD">Australia Dollars (AUD)</option>
                                            <option value="ATS">Austria Schillings (ATS)</option>
                                            <option value="BSD">Bahamas Dollars (BSD)</option>
                                            <option value="BBD">Barbados Dollars (BBD)</option>
                                            <option value="BEF">Belgium Francs (BEF)</option>
                                            <option value="BMD">Bermuda Dollars (BMD)</option>
                                            <option value="BRR">Brazil Real (BRR)</option>
                                            <option value="BGL">Bulgaria Lev (BGL)</option>
                                            <option value="CAD">Canada Dollars (CAD)</option>
                                            <option value="CLP">Chile Pesos (CLP)</option>
                                            <option value="CNY">China Yuan Renmimbi (CNY)</option>
                                            <option value="CYP">Cyprus Pounds (CYP)</option>
                                            <option value="CSK">Czech Republic Koruna (CSK)</option>
                                            <option value="DKK">Denmark Kroner (DKK)</option>
                                            <option value="NLG">Dutch Guilders (NLG)</option>
                                            <option value="XCD">Eastern Caribbean Dollars (XCD)</option>
                                            <option value="EGP">Egypt Pounds (EGP)</option>
                                            <option value="FJD">Fiji Dollars (FJD)</option>
                                            <option value="FIM">Finland Markka (FIM)</option>
                                            <option value="FRF">France Francs (FRF)</option>
                                            <option value="DEM">Germany Deutsche Marks (DEM)</option>
                                            <option value="XAU">Gold Ounces (XAU)</option>
                                            <option value="GRD">Greece Drachmas (GRD)</option>
                                            <option value="HKD">Hong Kong Dollars (HKD)</option>
                                            <option value="HUF">Hungary Forint (HUF)</option>
                                            <option value="ISK">Iceland Krona (ISK)</option>
                                            <option value="INR">India Rupees (INR)</option>
                                            <option value="IDR">Indonesia Rupiah (IDR)</option>
                                            <option value="IEP">Ireland Punt (IEP)</option>
                                            <option value="ILS">Israel New Shekels (ILS)</option>
                                            <option value="ITL">Italy Lira (ITL)</option>
                                            <option value="JMD">Jamaica Dollars (JMD)</option>
                                            <option value="JPY">Japan Yen (JPY)</option>
                                            <option value="JOD">Jordan Dinar (JOD)</option>
                                            <option value="KRW">Korea (South) Won (KRW)</option>
                                            <option value="LBP">Lebanon Pounds (LBP)</option>
                                            <option value="LUF">Luxembourg Francs (LUF)</option>
                                            <option value="MYR">Malaysia Ringgit (MYR)</option>
                                            <option value="MXP">Mexico Pesos (MXP)</option>
                                            <option value="NLG">Netherlands Guilders (NLG)</option>
                                            <option value="NZD">New Zealand Dollars (NZD)</option>
                                            <option value="NOK">Norway Kroner (NOK)</option>
                                            <option value="PKR">Pakistan Rupees (PKR)</option>
                                            <option value="XPD">Palladium Ounces (XPD)</option>
                                            <option value="PHP">Philippines Pesos (PHP)</option>
                                            <option value="XPT">Platinum Ounces (XPT)</option>
                                            <option value="PLZ">Poland Zloty (PLZ)</option>
                                            <option value="PTE">Portugal Escudo (PTE)</option>
                                            <option value="ROL">Romania Leu (ROL)</option>
                                            <option value="RUR">Russia Rubles (RUR)</option>
                                            <option value="SAR">Saudi Arabia Riyal (SAR)</option>
                                            <option value="XAG">Silver Ounces (XAG)</option>
                                            <option value="SGD">Singapore Dollars (SGD)</option>
                                            <option value="SKK">Slovakia Koruna (SKK)</option>
                                            <option value="ZAR">South Africa Rand (ZAR)</option>
                                            <option value="KRW">South Korea Won (KRW)</option>
                                            <option value="ESP">Spain Pesetas (ESP)</option>
                                            <option value="XDR">Special Drawing Right (IMF) (XDR)</option>
                                            <option value="SDD">Sudan Dinar (SDD)</option>
                                            <option value="SEK">Sweden Krona (SEK)</option>
                                            <option value="CHF">Switzerland Francs (CHF)</option>
                                            <option value="TWD">Taiwan Dollars (TWD)</option>
                                            <option value="THB">Thailand Baht (THB)</option>
                                            <option value="TTD">Trinidad and Tobago Dollars (TTD)</option>
                                            <option value="TRL">Turkey Lira (TRL)</option>
                                            <option value="VEB">Venezuela Bolivar (VEB)</option>
                                            <option value="ZMK">Zambia Kwacha (ZMK)</option>
                                            <option value="EUR">Euro (EUR)</option>
                                            <option value="XCD">Eastern Caribbean Dollars (XCD)</option>
                                            <option value="XDR">Special Drawing Right (IMF) (XDR)</option>
                                            <option value="XAG">Silver Ounces (XAG)</option>
                                            <option value="XAU">Gold Ounces (XAU)</option>
                                            <option value="XPD">Palladium Ounces (XPD)</option>
                                            <option value="XPT">Platinum Ounces (XPT)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="indicators_of_interest">Do you have any indicators of interest or
                                            sales?</label>
                                        <textarea maxlength="750" name="indicators_of_interest" id="indicators_of_interest"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price_point">Price point</label>
                                        <input type="number" step="0.01" placeholder="00.00" name="price_point" id="price_point">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h5>ADDITIONAL INFORMATION</h5>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group infofile">
                                        <label for="supporting_information">Upload any supporting information
                                            here:</label>
                                        <input type="file" id="supporting_information" name="supporting_information">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="has_referrals">Finally, do you have a referral or know anyone at
                                            Driftwood:</label>
                                        <select name="has_referrals" id="has_referrals">
                                            <option disabled="" selected="" value="">Please Select</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Finally, do you have a referral or know anyone at Driftwood:</label>
                                        <div class="check-cont">
                                            <input type="checkbox" id="where_did_you_hear_social_media" name="where_did_you_hear[]" value="Social Media">
                                            <label for="where_did_you_hear_social_media">Social Media</label>
                                        </div>
                                        <div class="check-cont">
                                            <input type="checkbox" id="where_did_you_hear_word_of_mouth" name="where_did_you_hear[]" value="Word of Mouth">
                                            <label for="where_did_you_hear_word_of_mouth">Word of Mouth</label>
                                        </div>
                                        <div class="check-cont">
                                            <input type="checkbox" id="where_did_you_hear_event_or_seminar" name="where_did_you_hear[]" value="Event or Seminar">
                                            <label for="where_did_you_hear_event_or_seminar">Event or Seminar</label>
                                        </div>
                                        <div class="check-cont">
                                            <input type="checkbox" id="where_did_you_hear_google" name="where_did_you_hear[]" value="Google">
                                            <label for="where_did_you_hear_google">Google</label>
                                        </div>
                                        <div class="check-cont">
                                            <input type="checkbox" id="where_did_you_hear_article" name="where_did_you_hear[]" value="Article">
                                            <label for="where_did_you_hear_article">Article</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group check-cont">
                                        <input type="checkbox" id="where_did_you_hear_other" name="where_did_you_hear[]" value="Other">
                                        <label for="where_did_you_hear_other">Other (Please specify below):</label>
                                        <textarea placeholder="" name="where_did_you_hear_other_description" id="where_did_you_hear_other_description"></textarea>
                                    </div>
                                    <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>" />
                                    <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>" />
                                    <input type="hidden" name="Lead_source" value="DR FB" />
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group text-end">
                                        <button type="submit" id="btn-submit" class="btn">Submit Application <i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <footer>
        <div class="footer-section">
            <div class="container">
                <div class="footer-wrap">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-12 col-12">
                            <div class="footer-logo heading">
                                <a href="index.php"><img src="../img/footer-logo.png" class="img-fluid" alt="footer-logo" /></a>
                                <p>Driftwood Distillery is a craft gin distillery based on an old naval airbase near
                                    Katwijk in the Netherlands.</p>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-12 col-12">
                            <div class="footer-menu menu-1">
                                <h4>Quick Links</h4>
                                <img src="../img/others/strip-1.png" class="img-fluid" alt="strip-img" />
                                <ul class="list-unstyled">
                                    <li><a href="../../index.php#" class="active"><i class="fa fa-chevron-right" aria-hidden="true"></i>Home</a></li>
                                    <li><a href="../../index.php#aboutSection"><i class="fa fa-chevron-right" aria-hidden="true"></i>About</a></li>
                                    <li><a href="../../index.php#wholeSection"><i class="fa fa-chevron-right" aria-hidden="true"></i>Wholesale</a>
                                    </li>
                                    <li><a href="https://www.driftwooddistillery.eu/collections/drinks"><i class="fa fa-chevron-right" aria-hidden="true"></i>Our
                                            Products</a></li>
                                    <li><a href="../../index.php#ourSection"><i class="fa fa-chevron-right" aria-hidden="true"></i>Services</a>
                                    </li>
                                    <li><a href="../../index.php#contactSection"><i class="fa fa-chevron-right" aria-hidden="true"></i>Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-12 col-12">
                            <div class="footer-menu menu-2">
                                <h4>Follow us</h4>
                                <img src="../img/others/strip-1.png" class="img-fluid" alt="strip-img" />
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i>Linkedin</a></li>
                                    <li><a href="#"><i class="fab fa-tiktok"></i>Tiktok</a></li>
                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a></li>
                                    <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i>Instagram</a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i>Twitter</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-12 col-12">
                            <div class="newsletter footer-menu">
                                <h4>NewsLetter Updates</h4>
                                <img src="../img/others/strip-1.png" class="img-fluid" alt="strip-img" />
                                <form>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name*" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Email* " required>
                                    </div>
                                    <div class="form-group">
                                        <a href="#" class="btn">Submit Now</a>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="copyrights text-center">
                    <p>&copy; Copyrights 2023 <span>Driftwood Distillery </span> All Rights Reserved</p>
                </div>
            </div>
        </div>
    </footer>



    <!-- JavaScript Libraries -->
    <script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="../js/main.js"></script>
    <script src="https://kit.fontawesome.com/d547ebcf09.js" crossorigin="anonymous"></script>
    <!--- teleinput for country code js --->
    <script src="../../intlTelInput/js/intlTelInput.min.js"></script>
    <script src="../../intlTelInput/js/utils.js"></script>
    <script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=MQ8R-3CYE-EY32-ZRWE&load=InternationalTelephoneValidation,EmailValidation">
    </script>
    <script type="text/javascript">
        var validEmail = false;
        $("#btn-submit").click(function(e) {
            var email = $("#email").val();
        });

        $(document).ready(function() {
            // Validate email
            $("#email").change(function() {
                var email = $("#email").val();
                level = 'Address';
                var emailvalidation = new data8.emailvalidation();
                emailvalidation.isvalid(
                    email,
                    level,
                    [

                    ],
                    showIsEmailValid
                );
            });

            function showIsEmailValid(result) {
                console.log(result.Result);
                if (result.Result == 'Valid') {
                    validEmail = true;
                    $('#emailDiv p').remove('');
                    $("#btn-submit").attr('disabled', false);
                } else {
                    validEmail = false;
                    $('#emailDiv p').remove('');
                    $('#emailDiv').append(
                        '<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Email Address.</p>');
                    $("#btn-submit").attr('disabled', true);
                }
            }
        });
    </script>
</body>

</html>