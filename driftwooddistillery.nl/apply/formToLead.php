<?php

include('ZohoCrmClient.php');

$crmObj = new ZohoCrmClient();

if (!file_exists(__DIR__ . "/files")) {
    echo "insode idf";
    $file_name = mkdir(__DIR__ . "/files", 0777, true);
    $fname = "files/";
} else {
    echo "inside else";
    $fname = "files/";
}


$targetfolder = $fname;
$targetfolder1 = $fname;


if (!empty($_POST)) {

    $postData = [];
    $postData['Brand_Name'] = $_POST['brand_name'];
    $postData['Website'] = $_POST['website'];
    $postData['Social_Media_Please_provide_protocol_eg_https'] = $_POST['instagram'];
    $postData['Other_Social_Media'] = $_POST['social'];
    $postData['Year_Launched'] = $_POST['year_launched'];
    $postData['Please choose the relevant categories'] = json_encode($_POST['relevant_categories']);
    $postData['Other_Social_Media'] = $_POST['relevant_categories_other_description'];
    $postData['Where_is_your_business_located'] = $_POST['location'];
    $postData['How_many_founders_are_there'] = $_POST['founders_count'];
    $postData['Are_you_applying_for_the_Pre_Accelerator'] = $_POST['pre_accel'];
    $postData['independent_institutions'] = $_POST['is_certified'];
    $postData['more_underrepresented_communitie_s'] = $_POST['is_diverse'];
    $postData['First_Name'] = $_POST['first_name'];
    $postData['Last_Name'] = $_POST['last_name'];
    $postData['Country'] = $_POST['country'];
    $postData['Email'] = $_POST['email'];
    $postData['Please_provide_your_brief_bio'] = $_POST['bio'];
    $postData['Tell_us_a_bit_about_you_and_your_founding_team'] = $_POST['about_you'];
    $postData['Briefly_describe_your_liquid'] = $_POST['liquid_details'];
    $postData['change_or_solve_through_your_brand'] = $_POST['about_brand'];
    $postData['who_do_you_plan_to_focus_on_in_the_future'] = $_POST['audience'];
    $postData['Product_that_is_available_for_sale'] = $_POST['is_available_for_sale'];
    $postData['country_currency'] = $_POST['currency'];
    $postData['Do_you_have_any_indicators_of_interest_or_sales'] = $_POST['indicators_of_interest'];
    $postData['Price_point'] = $_POST['price_point'];
    $postData['know_anyone_at_Distill_Ventures'] = $_POST['has_referrals'];
    $postData['Finally_do_you_have_a_referral'] = $_POST['where_did_you_hear'];
    $postData['Other_Please_specify_below'] = $_POST['where_did_you_hear_other_description'];
    $postData['Lead_Source'] = $_POST['Lead_source'];
    $postData['ClickID'] = (isset($_POST['lid'])) ? $_POST['lid'] : "";
    $postData['Company'] = (isset($_POST['aid'])) ? $_POST['aid'] : "";
    $zohoPost = [];
    $zohoPost['data'] = array($postData);
    $log_message = date('d-m-Y H:i:s') . ' - request received - ' . json_encode($zohoPost);
    file_put_contents('thewhiskyinvestco.com_response_' . date('d-M-Y') . '.txt',  $log_message . "\n", FILE_APPEND);
    $create = $crmObj->createZohoCrmRecord('Leads', ($zohoPost));
    $res = json_decode($create, true);
    if (($res['data'][0]) && $res['data'][0]['code'] == "SUCCESS") {
        echo $lead_id =  $res['data'][0]['details']['id'];

        $brand_deck = true;
        $supporting_information  = true;
        if (isset($_FILES['supporting_information']) && $supporting_information == true) {
            $supporting_information = false;
            echo $targetfolder = $targetfolder . basename($_FILES['supporting_information']['name']);
            $file_type = $_FILES['supporting_information']['type'];
            if (isset($_FILES) && isset($_FILES['supporting_information']['name'])) {

                if (move_uploaded_file($_FILES['supporting_information']['tmp_name'], $targetfolder)) {

                    $document =  ($_FILES['supporting_information']['name']);
                    $url = "files/$document";
                    $responseAtt = $crmObj->attachment("Leads", $lead_id, $url);

                    $recordD = $_FILES['supporting_information']['name'];
                    $directory = "files";
                    $directoryHandle1 = opendir($directory);
                    while ($file1 = readdir($directoryHandle1)) {
                        if ($file1 == $recordD) {
                            unlink($directory . '/' . $file1);
                        }
                    }
                }
            }
        }



        if (isset($_FILES['brand_deck']) && $brand_deck == true) {
            $brand_deck = false;
            $targetfolder1 = $targetfolder1 . basename($_FILES['brand_deck']['name']);

            $file_tp = $_FILES['brand_deck']['type'];
            if (isset($_FILES) && isset($_FILES['brand_deck']['name'])) {
                if (move_uploaded_file($_FILES['brand_deck']['tmp_name'], $targetfolder1)) {
                    $doc =  ($_FILES['brand_deck']['name']);
                    $url1 = "files/" . $doc;
                    $responseAtt1 = $crmObj->attachment("Leads", $lead_id, $url1);

                    $data = $_FILES['brand_deck']['name'];
                    $dir = "files";
                    $dirHandle = opendir($dir);
                    while ($file = readdir($dirHandle)) {
                        if ($file == $data) {
                            unlink($dir . '/' . $file);
                        }
                    }
                }
            }
        }
        header('Location: ../index.php');
    }
}
