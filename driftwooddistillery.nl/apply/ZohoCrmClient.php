<?php
error_reporting(0);
class ZohoCrmClient
{

    public function getAccessToken()
    {
        $crm_config = array(
            'client_id' => '1000.G2VEJ8AMSAP98RB8KURI5ESDV66I7V',
            'client_secret' => '40eeeef6123d018a62198becc24d2acc412cd8a9e8',
            'refresh_token' => '1000.b060102d6faf81ddc4798983eb18a997.5ec3d939f8ce885448b28ecd905f9d66',
            'grant_type' => 'refresh_token',
            'api_domain' => 'https://www.zohoapis.eu/',
            'account_domain' => 'https://accounts.zoho.eu/',
        );

        $zoho_crm_session = file_get_contents('zoho/access_token.json');
        $zoho_crm_session = json_decode($zoho_crm_session);
        $current_time = date('Y-m-d H:i:s');

        if (isset($zoho_crm_session->access_token) && ($current_time < $zoho_crm_session->expiring_at)) {
            return $zoho_crm_session;
        } else {

            $url = $crm_config['account_domain'] . 'oauth/v2/token';
            $postData = [
                'client_id' => $crm_config['client_id'],
                'client_secret' => $crm_config['client_secret'],
                'refresh_token' => $crm_config['refresh_token'],
                'grant_type' => $crm_config['grant_type']
            ];
            $ch = curl_init(); // Create a curl handle
            curl_setopt($ch, CURLOPT_URL, $url); // Third party API URL
            curl_setopt($ch, CURLOPT_POST, FALSE);  // To set POST method true
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); // To send data to the API URL
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // To set SSL Verifier false
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // To set return response from the API
            $response = curl_exec($ch); // To execute the handle and get the response 
            $response = json_decode($response);

            if (isset($response->access_token)) {
                $now = time();
                $ten_minutes = $now + (30 * 60);
                $startDate = date('Y-m-d H:i:s', $now);
                $endDate = date('Y-m-d H:i:s', $ten_minutes);
                $current_time = date('Y-m-d H:i:s');
                $response->expiring_at = $endDate;
                file_put_contents('zoho/access_token.json', json_encode($response));

                return $response;
            } else {

                throw new Exception("Error Processing Request", 1);
            }
        }
    }


    public function getAllUsers()
    {
        $url = 'users?type=AllUsers';
        return $res = $this->sendGetRequest($url);
    }

    public function getZohoCrmRecordsList($module)
    {
        $url = $module;
        return $res = $this->sendGetRequest($url);
    }

    public function getZohoCrmSpecificRecords($module, $recordId)
    {
        $url = $module . '/' . $recordId;
        return $res = $this->sendGetRequest($url);
    }

    public function getZohoCrmActiveUsers()
    {
        $url = 'users?type=ActiveUsers';
        return $res = $this->sendGetRequest($url);
    }


    public function updateZohoCrmRecord($module, $record_id, $postData)
    {
        $url = $module . '/' . $record_id;
        return $res = $this->sendPutRequest($url, $postData);
    }
    public function createZohoCrmRecord($module, $postData)
    {
        $url = $module;
        return $res = $this->sendPostRequest($url, $postData);
    }

    public function searchZohoRecord($module, $lead_filter)
    {
        if ($module == 'Leads') {
            $url = $module . '/search?criteria=(Email:equals:' . $lead_filter . ')';
        }

        return $res = $this->sendGetRequest($url);
    }

    public function sendPostRequest($url, $postData)
    {
        $url = 'https://www.zohoapis.eu/crm/v2/' . $url;
        $credentials = $this->getAccessToken();
        if (isset($credentials->access_token)) {
            $postData = json_encode($postData);
            $headers = array('Authorization: Zoho-oauthtoken ' . $credentials->access_token, 'Content-type: application/json'); // return datatype
            $ch = curl_init(); // Create a curl handle
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Set curl handle header 
            curl_setopt($ch, CURLOPT_URL, $url); // Third party API URL
            curl_setopt($ch, CURLOPT_POST, FALSE);  // To set POST method true
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); // To send data to the API URL
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // To set SSL Verifier false
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // To set return response from the API
            return $response = curl_exec($ch); // To execute the handle and get the response 

            $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Get Http Status code
        }
    }
    public function sendPutRequest($url, $postData)
    {
        $url = 'https://www.zohoapis.eu/crm/v2/' . $url;
        $credentials = $this->getAccessToken();
        if (isset($credentials->access_token)) {
            $postData = json_encode($postData);
            $headers = array('Authorization: Zoho-oauthtoken ' . $credentials->access_token, 'Content-type: application/json'); // return datatype
            $ch = curl_init(); // Create a curl handle
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Set curl handle header 
            curl_setopt($ch, CURLOPT_URL, $url); // Third party API URL
            curl_setopt($ch, CURLOPT_POST, FALSE);  // To set POST method true
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); // To send data to the API URL
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // To set SSL Verifier false
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // To set return response from the API
            return $response = curl_exec($ch); // To execute the handle and get the response 

            $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Get Http Status code
        }
    }



    public function sendGetRequest($url)
    {
        $url = 'https://www.zohoapis.eu/crm/v2/' . $url;
        $credentials = $this->getAccessToken();
        if (isset($credentials->access_token)) {
            $headers = array('Authorization: Zoho-oauthtoken ' . $credentials->access_token, 'Content-type: application/json'); // return datatype
            $ch = curl_init(); // Create a curl handle
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Set curl handle header 
            curl_setopt($ch, CURLOPT_URL, $url); // Third party API URL
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // To set SSL Verifier false
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // To set return response from the API
            return $response = curl_exec($ch); // To execute the handle and get the response 

            $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Get Http Status code
        }
    }

    public function attachment($module, $record_id, $postData)
    {
        if (function_exists('curl_file_create')) {
            $cFile = curl_file_create($postData);
        } else {
            $cFile = '@' . realpath($postData);
        }
        $post = array(
            'file' => $cFile
        );

        $apiUrl = "https://www.zohoapis.eu/crm/v2/" . $module . "/" . $record_id . "/Attachments";
        $credentials = $this->getAccessToken();
        if (isset($credentials->access_token)) {
            $headers = array();
            $headers[] = 'Authorization: Zoho-oauthtoken ' . $credentials->access_token;
            $headers[] = 'Content-Type: multipart/form-data';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $apiUrl);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $r = curl_exec($ch);

            curl_close($ch);
            return $r;
        }
    }
}
