
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Compare Now | Compare ISA Rates Offering</title>
  <link rel="icon" href="assets/images/ISAchecker-01.jpg" type="image/jpg" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/stylesheet.css">
  <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/f435ef88a9.js" crossorigin="anonymous"></script>
  <!-- Conversion Pixel - SALE_CONV - DO NOT MODIFY -->
  <script src="https://secure.adnxs.com/px?id=999577&seg=13439766&t=1" type="text/javascript"></script>
  <!-- End of Conversion Pixel -->
</head>
<body>
  <header class="bg-white border-bottom">
    <div class="container">
      <div class="text-center">
        <img class="img-logo" src="assets/images/ISAchecker-01.png" 
        style="margin: 40px;">
      </div>
    </div>
  </header>

  <section class="form-section" style="background-color: #d9d9d9;">
    <br>
    <div class="container">
      <div class="row w-100 m-0 align-items-center mt-md-5 mb-md-4 my-3 ">
        <div class="col-md-9 mx-auto text-center">
          <form
          id="dealform"
          class="text-center"
          action="https://isachecker.com/mobile/zoho/form_data.php"
          method="post" data-parsley-validate="">
          
          <div class="form-group tab">
           <input type="hidden" name="lid" id="clickid" 
           value="<?php echo $_GET['lid'];?>">
           <input type="hidden" name="aid" id="aid" 
           value="<?php echo $_GET['aid'];?>">
           <div class="text-left">
            <label for="first">Full Name</label>
            <input 
            placeholder="Name"
            id="last_name" 
            name="last_name" 
            class="last_name pl-lg-5 pl-2 input-checker-div text-secondary form-control error-on-button"
            data-parsley-required="true"
            data-parsley-required-message="Please Enter Last Name"  
            data-parsley-group="block-4" 
            data-parsley-pattern-message="Last Name Must Be Alphabetic" 
            data-parsley-pattern="/^[a-zA-Z ]*$/" 
            type="text"
            required=""
            >
            <div class="error-box row w-100 mx-0 align-items-center justify-content-center text-danger">
            </div>
          </div>
        </div>

        <div class="form-group tab">
          <div class="text-left">
            <label for="first">Mobile</label>
                  <div class="">
                    <input 
                      placeholder="Phone number"
                      id="phone" 
                      name="phone" 
                      class="phone pl-lg-5 pl-2 input-checker-div text-secondary form-control error-on-button"
                      data-parsley-required="true"
                      type="tel" 
                      required>
                  </div>
                  <div id="Invalid_error"></div>
                 <div class="error-box row w-100 mx-0 align-items-center justify-content-center text-danger">
            </div>
          </div>
          <!-- data-parsley-validphone -->
        </div>

        <div class="tab mt-2  form-group text-left">
          <div class="text-left">
            <label for="first"><label class="">Do you currently have an ISA?</label>
          </label>
          <div class="col-12 text-left">
            <div class="input-checker-div">
              <input 
              type="radio" 
              name="haveIsa"
              class="haveIsa opacity-0 position-absolute form-control check-input error-on-button"
              id="haveIsa"
              data-parsley-required="true"
              value="Yes" 
              data-parsley-required-message="Please Select One"
              data-parsley-group="block-0">

              <label for="haveIsa" class="d-flex align-items-center" >
                <i class="fas fa-check check-icon"></i>
                <p class="m-0 pl-3">Yes</p>
              </label>
            </div>
          </div>
          <div class="col-12 text-left">
            <div class="input-checker-div">
              <input 
              type="radio" 
              name="haveIsa"
              class="haveIsa opacity-0 position-absolute form-control check-input error-on-button"
              id="haveIsano"
              value="No" 
              data-parsley-required="true"
              data-parsley-required-message="Please Select One"
              required=""
              data-parsley-group="block-0"
              >
              <label for="haveIsano" class="d-flex align-items-center">
                <i class="fas fa-check check-icon"></i>
                <p class="m-0 pl-3">No</p>
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="error-box row w-100 mx-0 align-items-center justify-content-center text-danger">
      </div>

      <div class="tab mt-2  form-group text-left">
        <div class="text-left">
          <label class="">How much would you be looking to invest?</label>
          <div class="col-12 text-left">
            <div class="input-checker-div">
              <input 
              type="radio" 
              name="invest-amount"
              class="invest-amount opacity-0 position-absolute form-control check-input error-on-button"
              id="invest-amount"
              value="£500 - £5,000" 
              required=""
              data-parsley-required="true"
              data-parsley-required-message="Please Select One"
              data-parsley-group="block-1"
              >
              <label for="invest-amount" class="d-flex align-items-center" >
                <i class="fas fa-check check-icon"></i>
                <p class="m-0 pl-3">£500 - £5,000</p>
              </label>
            </div>
          </div>
          <div class="col-12 text-left">
            <div class="input-checker-div">
              <input 
              type="radio" 
              name="invest-amount"
              class="invest-amount opacity-0 position-absolute form-control check-input error-on-button"
              id="invest-amount20"
              value="£5,000 - £20,000" 
              required=""
              data-parsley-required="true"
              data-parsley-required-message="Please Select One"
              data-parsley-group="block-1"
              >
              <label for="invest-amount20" class="d-flex align-items-center">
                <i class="fas fa-check check-icon"></i>
                <p class="m-0 pl-3">£5,000 - £20,000</p>
              </label>
            </div>
          </div>
          <div class="col-12 text-left">
            <div class="input-checker-div">
              <input 
              type="radio" 
              name="invest-amount"
              class="invest-amount opacity-0 position-absolute form-control check-input error-on-button"
              id="invest-amount50"
              value="£20,000 - £50,000" 
              required=""
              data-parsley-required="true"
              data-parsley-group="block-1"
              data-parsley-required-message="Please Select One">
              <label for="invest-amount50" class="d-flex align-items-center" >
                <i class="fas fa-check check-icon"></i>
                <p class="m-0 pl-3">£20,000 - £50,000</p>
              </label>
            </div>
          </div>
          <div class="col-12 text-left">
            <div class="input-checker-div">
              <input 
              type="radio" 
              name="invest-amount"
              class="invest-amount opacity-0 position-absolute form-control check-input error-on-button"
              id="invest-amountover"
              value="Over £50,000" 
              required=""
              data-parsley-required="true"
              data-parsley-group="block-0"
              data-parsley-required-message="Please Select One">
              <label for="invest-amountover" class="d-flex align-items-center">
                <i class="fas fa-check check-icon"></i>
                <p class="m-0 pl-3">Over £50,000</p>
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="error-box row w-100 mx-0 align-items-center justify-content-center text-danger"></div>
    <!--<div id="Invalid_error"></div>-->
      <div class="form-group submit">
        <div class="text-center">
          <input type="submit" value="Get Your Quotes" id="btn-submit" class="get_your_quotes font-weight-bold text-white">
        </div>
      </div>
    </div>
  </form>
</div>
</div>
</div>
<!-- <div class="contact-space">
  <p>&nbsp;</p>
</div> -->
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/parsley.js"></script>
<script src="assets/js/form.js?v=2.1"></script>
<script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=5NSH-UILN-E8IG-WMNJ&load=InternationalTelephoneValidation">
</script>
</body>
</html>