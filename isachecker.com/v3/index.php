<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
  <title>Compare ISA Rates Offering -The Best Returns</title>
  <link rel="stylesheet" type="text/css" href="assets/css/stylesheet.css">
  <link rel="stylesheet" type="text/css" href="assets/css/rangeslider.css">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
  <script src="https://kit.fontawesome.com/f435ef88a9.js" crossorigin="anonymous"></script>
    <!-- Segment Pixel - ALLPAGES_REM - DO NOT MODIFY -->
<script src="https://secure.adnxs.com/seg?add=13439767&t=1" type="text/javascript"></script>
<!-- End of Segment Pixel -->
</head>
<body>
  <header class="bg-white">
    <div class="container">
      <div class="row m-0 w-100 align-items-center justify-content-md-around justify-content-between py-3">
        <img class="img-logo" src="assets/images/Isachecker.png">
        <img class="img-tp" src="assets/images/trustpilot.png">
      </div>
    </div>
  </header>
  <div class="bg-img-top">
    <div class="row w-100 m-0 row-img position-relative">
      <div class="col-lg-6 mx-auto text-center py-3 z-indx">
        <div class="row tabs-row">
          <div class="col mx-auto">
            <div class="bg-white py-3 px-md-5 px-4 mb-4 form-div">
              <form method="POST"
                id="dealform"
                action="/v3/post.php"
                class=" mb-3 text-center"
                data-parsley-validate="">
                <div class="tab text-center">
                  <div class="mt-4 text-center form-group">
                    <label class="d-flex justify-content-center text-center font-weight-bold text-black f-25 mb-3">
                      Do you currently have an ISA?
                    </label>
                    <div class="row w-100 m-0 justify-content-around">
                      <div class="col-6 text-md-right">
                        <label for="haveIsa" class="mx-md-4 ">
                          <i class="fa fa-check-circle-o fs-75 yes-hover icon-haveIsa "></i>
                        </label>
                        <h4 class="mx-md-4 px-md-3">Yes</h4>
                        <input
                          id="haveIsa"
                          name="haveIsa"
                          class="haveIsa opacity-0 yes-hover px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control text-center position-absolute"
                          data-parsley-required="true"
                          data-parsley-group="block-0"
                          type="radio"
                          value="yes"
                          required=""
                        >
                      </div>
                      <div class="col-6 text-md-left">
                        <label for="haveIsano" class="mx-md-4 ">
                          <i class="fa fa-times-circle-o fs-75 no-hover icon-haveIsa"></i>
                        </label>
                        <h4 class="mx-md-4 px-md-4">No</h4>
                        <input
                          id="haveIsano"
                          name="haveIsa"
                          class="haveIsa opacity-0 no-hover px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control text-center position-absolute"
                          data-parsley-required="true"
                          data-parsley-group="block-0"
                          type="radio"
                          value="no"
                          required=""
                        >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab text-center disp-none">
                  <div class="mt-4 text-center form-group ">
                    <label class="d-flex justify-content-center text-center font-weight-bold text-black f-25 mb-3">
                     Do you want to get paid interest monthly, quarterly or annually?
                    </label>
                    <div class="row w-100 m-0 justify-content-around">
                      <div class="col-4 text-md-right">
                        <label for="interest-paid-type" class="mx-md-4 p-2">
                          <img class="img-m my-hover" src="assets/images/img31.png">
                        </label>
                        <h4 class="mx-md-3 fs-md-17">Monthly</h4>
                        <input
                          id="interest-paid-type"
                          name="interest-paid-type"
                          class="interest-paid-type opacity-0 px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control text-center position-absolute"
                          data-parsley-required="true"
                          data-parsley-group="block-1"
                          type="radio"
                          value="Monthly"
                          required=""
                        >
                      </div>
                      <div class="col-4 text-center">
                        <label for="interest-paid-type-quarter" class="mx-md-4 p-2">
                          <img class="img-y my-hover" src="assets/images/img-90.png">
                        </label>
                        <h4 class="mx-md-4 px-md-0 px-2 fs-md-17">Quarterly</h4>
                        <input
                          id="interest-paid-type-quarter"
                          name="interest-paid-type"
                          class="interest-paid-type opacity-0 px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control text-center position-absolute"
                          data-parsley-required="true"
                          data-parsley-group="block-1"
                          type="radio"
                          value="Quarterly"
                          required=""
                        >
                      </div>
                      <div class="col-4 text-md-left">
                        <label for="interest-paid-type-yearly" class="mx-md-4 p-2">
                          <img class="img-y my-hover" src="assets/images/img-365.png">
                        </label>
                        <h4 class="mx-md-2 px-2 fs-md-17">Annually</h4>
                        <input
                          id="interest-paid-type-yearly"
                          name="interest-paid-type"
                          class="interest-paid-type opacity-0 px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control text-center position-absolute"
                          data-parsley-required="true"
                          data-parsley-group="block-1"
                          type="radio"
                          value="Annually"
                          required=""
                        >
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab text-center disp-none">
                  <div class="text-center form-group">
                    <label class="d-flex justify-content-center py-2 h5 font-weight-bold f-25">How much would you be looking to invest?</label>
                    <div class="position-relative rangediv d-flex justify-content-center my-3 m-w mx-auto mt-5">
                      <input
                      class="invest-amount m-w"
                      id="invest-amount"
                      type="range"
                      min="1000" max="100000" step="1000"value="50000"
                      name="invest-amount"
                      data-parsley-group="block-2"
                      required="">
                    </div>
                    <div class="mt-4"></div>
                    <button id="btn-next" type="button" class="btn-lg btn-next text-white rounded-lg font-weight-bold w-100  m-w h-46 nextStep" onclick="nextStep(1)">
                      Next
                    </button>
                  </div>
                </div>
                <!-- New -->
                <div class="tab mt-2 disp-none form-group">
                  <label class="h5 font-weight-bold fs-md-17 text-blue">What type of investor are you?</label>
                  <div class="row align-items-center mt-5">
                    <div class="pointer invester-type col-md-3 col-6 text-center">
                      <div>
                        <input
                          type="radio"
                          name="investor-type"
                          class="investor-type opacity-0 position-absolute form-control cursor-point"
                          id="investor-type"
                          value="High net Worth"
                          required=""
                          data-parsley-required="true"
                          data-parsley-group="block-3"
                          >
                        <label for="investor-type" class="hover-shadow image-investor" >
                          <img class="rounded-lg cursor-point img-invest" src="assets/images/invest6.png">
                        </label>
                      </div>
                      <h6 class="mb-0 mt-3 font-weight-bold text-blue fs-md-13">High Net Worth</h6>
                    </div>
                    <div class="pointer invester-type col-md-3 col-6 text-center">
                      <div>
                        <input
                          type="radio"
                          name="investor-type"
                          class="investor-type opacity-0 position-absolute form-control cursor-point"
                          id="investor-type-sophis"
                          value="Sophisticated"
                          required=""
                          data-parsley-required="true"
                          data-parsley-group="block-3"
                          >
                          <label for="investor-type-sophis" class="hover-shadow image-investor">
                            <img class="rounded-lg cursor-point img-invest" src="assets/images/invest1.png">
                          </label>
                      </div>
                      <h6 class="mb-0 mt-3 font-weight-bold text-blue fs-md-13">Sophisticated</h6>
                    </div>
                    <div class="pointer invester-type col-md-3 col-6 text-center mt-2 mt-md-0">
                      <div>
                        <input
                          type="radio"
                          name="investor-type"
                          class="investor-type opacity-0 position-absolute form-control cursor-point"
                          id="investor-type-self-soph"
                          value="Self Certified Sophisticated"
                          required=""
                          data-parsley-required="true"
                          data-parsley-group="block-3"
                          >
                          <label for="investor-type-self-soph" class="hover-shadow image-investor">
                            <img class="rounded-lg cursor-point img-invest" src="assets/images/invest5.png">
                          </label>
                      </div>
                      <h6 class="mb-0 mt-3 font-weight-bold text-blue fs-md-13">Self Certified</h6>
                    </div>
                    <div class="pointer invester-type col-md-3 col-6 text-center mt-2 mt-md-0">
                      <div>
                        <input
                          type="radio"
                          name="investor-type"
                          class="investor-type opacity-0 position-absolute form-control cursor-point approve"
                          id="investor-type-everyday"
                          value="Everyday Investor"
                          required=""
                          data-parsley-required="true"
                          data-parsley-required-message="Please Select One Type"
                          data-parsley-group="block-3"
                          >
                          <label for="investor-type-everyday" class="hover-shadow image-investor">
                            <img class="rounded-lg cursor-point img-invest" src="assets/images/invest9.png">
                          </label>
                      </div>
                      <h6 class="mb-0 mt-3 font-weight-bold text-blue fs-md-13">Everyday</h6>
                    </div>
                  </div>
                  <div class="error-checkbox"></div>
                </div>

                <div class="tab text-center disp-none">
                  <div class="row m-0 w-100">
                    <div class="col-md-6 mt-4 pl-md-0 pr-md-2 px-0 text-center form-group position-relative">
                      <label class="d-flex justify-content-center text-center font-weight-bold text-black f-25 mb-3">
                        First name?
                      </label>
                      <input
                        placeholder="First Name"
                        id="first_name"
                        name="first_name"
                        class="first_name px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control"
                        data-parsley-required="true"
                        data-parsley-required-message="Please Enter First Name"
                        data-parsley-group="block-4"
                        data-parsley-pattern-message="First Name Must Be Alphabetic" data-parsley-pattern="/^[a-zA-Z ]*$/"
                        type="text"
                        required>
                        <i class="validate success fa fa-check-circle"></i>
                        <i class="validate error fa fa-times-circle"></i>
                    </div>
                    <div class="col-md-6 mt-4 pr-md-0 pl-md-2 px-0 text-center form-group position-relative">
                      <label class="d-flex justify-content-center text-center font-weight-bold text-black f-25 mb-3">
                        Last name?
                      </label>
                      <input
                        placeholder="Last Name"
                        id="last_name"
                        name="last_name"
                        class="last_name px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control"
                        data-parsley-required="true"
                        data-parsley-required-message="Please Enter Last Name"
                        data-parsley-group="block-4"
                        data-parsley-pattern-message="Last Name Must Be Alphabetic" data-parsley-pattern="/^[a-zA-Z ]*$/"
                        type="text"
                        required>
                        <i class="validate success fa fa-check-circle"></i>
                        <i class="validate error fa fa-times-circle"></i>
                    </div>
                  </div>
                  <button id="btn-next" type="button" class="btn-lg btn-next text-white rounded-lg font-weight-bold w-100 mt-4 m-w h-46 nextStep" onclick="nextStep(1)">
                    Next
                  </button>
                </div>
                <div class="tab text-center disp-none">
                  <div class="row w-100 m-0">
                    <div class="col-md-6 mt-4 pl-md-0 pr-md-2 px-0 text-center form-group position-relative">
                      <label class="d-flex justify-content-center text-center font-weight-bold text-black f-25 mb-3">
                        Email address?
                      </label>
                      <input
                        placeholder="Email"
                        id="email"
                        name="email"
                        class="email px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control"
                        data-parsley-required="true"
                        data-parsley-required-message="Please Enter Valid Email Address"
                        data-parsley-group="block-5"
                        data-parsley-type="email"
                        data-parsley-validemail
                        type="email"
                        required>
                        <i class="validate success fa fa-check-circle"></i>
                        <i class="validate error fa fa-times-circle"></i>
                    </div>
                    <div class="col-md-6 mt-4 pr-md-0 pl-md-2 px-0 text-center form-group position-relative">
                      <label class="d-flex justify-content-center text-center font-weight-bold text-black f-25 mb-3">
                        Phone number?
                      </label>
                      <input
                        placeholder="Phone number"
                        id="phone"
                        name="phone"
                        class="phone px-2 h-46 m-w mx-auto border border-secondary bg-white rounded text-secondary form-control"
                        data-parsley-required="true"
                        data-parsley-validphone
                        data-parsley-required-message="Please Enter Valid UK Phone"
                        data-parsley-group="block-5"
                        data-parsley-minlength-message="Phone Number Should Have Minimum 10 Digits"
                        data-parsley-minlength='10'
                        minlength='10'
                        data-parsley-maxlength='11'
                        data-parsley-maxlength-message="Phone Number Should Have Maximum 11 Digits"
                        type="tel"
                        required>
                        <i class="validate success fa fa-check-circle"></i>
                        <i class="validate error fa fa-times-circle"></i>
                    </div>
                  </div>
                  <div class="row align-items-center justify-content-center">
                    <div class="checkbox-col col-lg-8 text-center">
                      <div class="checkbox-area position-relative">
                        <div class="bg-opacity rounded-left border-right">
                        </div>
                        <div class="custom-control custom-checkbox border p-4 rounded shadow">
                          <input type="checkbox"
                          name="customCheckbox"
                          class="custom-control-input approve customCheckbox"
                          id="customCheckbox"
                          data-parsley-required="true"
                          data-parsley-group="block-5"
                          data-parsley-required-message="Please accept our terms and conditions">
                          <label class="custom-control-label text-blue fw-500 fs-16 pl-md-5 pl-5r" for="customCheckbox">Accept Terms & Conditions.</label>
                        </div>

                      </div>
                      <div class="error-checkbox newcheckbox"></div>
                      <div class="my-3">
                        <p>By checking this box you confirm that you understand the ISA Checker privacy policy and opt-in that you will be contacted.</p>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="lid" id="clickid" value="<?php echo $_GET['lid']; ?>" >
                 <input type="hidden" name="aid" id="aid" value="<?php echo $_GET['aid']; ?>">
                  <input type="submit" value="COMPARE NOW!" id="btn-submit" class="compare_now m-w" >
                </div>
                <div class="back-btn d-flex justify-content-center align-items-center" onclick="backStep(-1)">
                  <i class="fas fa-arrow-left"></i>
                </div>
                <div class="progress mt-4 m-w mx-auto mb-0">
                  <div id="progressBar" class="progress-bar progress-bar-striped bg-success" role="progressbar">
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <footer class="position-relative">
    <div class="bg-clip-path">
      <div class="row w-100 m-0 z-indx position-relative">
        <div class="col text-center mx-auto">
          <p class=" mt-md-5 mt-4 pt-5 text-white text-center">
            <a class="text-white font-weight-bold" href="assets/terms-and-conditions.html">Terms And Conditions</a> | 
            <a class="text-white font-weight-bold" href="assets/privacy-policy.html">Privacy Policy</a>
          </p>
          <div class="container">
            <div class="row w-100 m-0 justify-content-center align-items-center">
              <p class="m-0 text-center pt-4 pb-3 text-white">
                <strong class="text-center">ISA CHECKER</strong>  we do not provide advice to investors and the information on this website should not be construed as such. The information which appears on our website is for information purposes only and does not constitute specific advice. Neither does it constitute a solicitation, offer or recommendation to invest in or dispose of, any investment. If you are in any doubt as to the suitability of an investment, you should seek independent financial advice from a suitable financial advisor.
              </p>
            </div>
          </div>
          <p class="text-center text-white">© 2020 'ISAChecker.com' All Rights Reserved</p>
        </div>
      </div>
    </div>
  </footer>


  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/rangeslider.min.js"></script>
  <script src="assets/js/parsley.js"></script>
  <script src="assets/js/form.js"></script>
</body>
</html>

