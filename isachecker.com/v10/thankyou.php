
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Compare Now | Compare ISA Rates Offering</title>
	
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://isachecker.com/assets/css/stylesheet.css">
	<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/f435ef88a9.js" crossorigin="anonymous"></script>
	<!-- Conversion Pixel - SALE_CONV - DO NOT MODIFY -->
	<script src="https://secure.adnxs.com/px?id=999577&seg=13439766&t=1" type="text/javascript"></script>
	<!-- End of Conversion Pixel -->
</head>
<body>
	<header class="bg-white border-bottom">
		<div class="container">
			<div class="row m-0 w-100 align-items-center py-3">
				<div class="col-md-5 text-md-left text-center">
					<img class="img-logo" src="images/isachecker/logo.png">
				</div>
			</div>
		</div>
	</header>
	<section class="form-section bg-white ">
		<div class="container" style="margin-top: 150px!important;">
			<h2 style="text-align: center;">Thank You</h2>
			<p style="text-align: center;">IsaChecker Is Working Hard On Getting You The Best Quotes And We Will Be In Touch Shortly.</p>
		</div>
		<div class="contact-space">
			<p>&nbsp;</p>
		</div>
	</section>

	<footer class="footer">
			<div class="container">
				<div class="row justify-content-center">
                    <div class="col-xl-12 col-lg-11">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="left-box">
                                    <p>
                                        <a href="terms_conditions.php" class="_link">Terms And Conditions</a>  &nbsp;    
                                        <a href="privacy_policy.php" class="_link">Privacy Policy</a> <br />
                                        &copy; 2021 <a href="#">ISAChecker.com</a>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="right-box">
                                    <p>
                                        We do not provide advice to investors and the information on this website should not be construed as such. The information which appears on our website is for information purposes only and does not constitute specific advice. Neither does it constitute a solicitation, offer or recommendation to invest in or dispose of, any investment. If you are in any doubt as to the suitability of an investment, you should seek independent financial advice from a suitable financial advisor.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</footer>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <script src="https://isachecker.com/assets/js/parsley.js"></script>
  <script src="https://isachecker.com/assets/js/form.js?v=2.1"></script>
</body>
</html>