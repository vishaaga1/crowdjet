<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Interlets</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="style.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=PT+Sans+Narrow:wght@400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@200;300&family=Poppins:ital,wght@0,500;0,700;1,400&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;700&display=swap" rel="stylesheet">
    <!--- intlTelInput CSS for Country Code --->
    <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css">
    <link href="favicon.png" rel="apple-touch-icon" />
    <!-- FAVICONS -->
    <link rel="shortcut icon" href="img/favicon/favicon.png" type="image/png">
    <link rel="icon" href="img/favicon/favicon.png" type="image/png">
  </head>
  <body>
    <header>
      <div class="header-top">
        <div class="container">
          <div class="header-wrap">
            <div class="row">
              <div class="col-lg-3 col-md-3 d-none d-lg-block d-md-block d-none d-sm-none">
                <div class="header-logo">
                  <a href="index.php">
                    <img src="img/logo.png" class="img-fluid" alt="logo" />
                  </a>
                </div>
              </div>
              <div class="col-lg-9 col-md-9">
                <div class="mainmenu">
                  <nav class="navbar navbar-expand-md navbar-dark">
                    <!-- Brand -->
                    <a class="navbar-brand d-block d-md-none d-lg-none d-sm-block" href="index.php">
                      <img src="img/logo.png" class="img-fluid" alt="logo" />
                    </a>
                    <!-- Toggler/collapsibe Button -->
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <!-- Navbar links -->
                    <div class="collapse navbar-collapse" id="collapsibleNavbar">
                      <ul class="navbar-nav">
                        <li class="nav-item">
                          <a class="active" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                          <a href="#">About</a>
                        </li>
                        <li class="nav-item">
                          <a href="#">Properties</a>
                        </li>
                        <li class="nav-item">
                          <a href="#">How To Let</a>
                        </li>
                        <li class="nav-item">
                          <a href="#">Faqs</a>
                        </li>
                        <li class="nav-item">
                          <a href="#">Testimonials</a>
                        </li>
                        <li class="nav-item">
                          <a href="#">Contact Us</a>
                        <li class="nav-item">
                          <a href="#">Book Now</a>
                        </li>
                      </ul>
                    </div>
                  </nav>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="header-bottom">
        <img src="img/banner/banner.png" class="img-fluid" width="100%" alt="banner">
        <div class="banner-contents">
          <div class="container">
            <div class="banner-details">
              <div class="banner-info">
                <h3>
                  <span>Bespoke Design </span>
                  <img src="img/banner/ban-key.png" class="img-fluid" width="10%" alt="banner-key">
                  <br> Industry Leading Returns
                </h3>
                <p>Speak to a Host Advisor</p>
                <a class="btn" href="#">Contact Us</a>
              </div>
            </div>
          </div>
          </div>
        </div>
        <div class="banner-shape text-center">
          <img src="img/banner/ban-arrow.png" class="img-fluid" alt="banner-arrow" />
        </div>
      </div>
    </header>
    <div class="holder">
      <div class="about-section">
        <div class="container">
          <div class="about-heading heading text-center">
            <h3>About Interlets</h3>
            <p>Strive for simplicity. Deliver sophistication.</p>
            <p>Interlets is a <span class="fst">contemporary company</span> focused on a new type of property management. We seek to <span class="fst">maximize returns for any landlord through the adaptation of their property </span> and delivery to the correct market. Our wealth of experience and knowledge of Dubai's short-term market is then utilized to proactively market and manage properties as high yield serviced vacation rentals. <br> Short-Term rental platforms such as, Airbnb, Booking.com and HomeAway are used as vehicles to drive greater returns for our landlords. Property owners can make between <span class="fst">30% - 60%</span> more than traditional long term lets. <span class="sec">To get a 12-month forecast for your property BOOK A CONSULTATION </span>
            </p>
          </div>
          <div class="properties-heading heading text-center">
            <div class="properties-wrap">
              <h4>Our properties are featured on</h4>
              <div class="row">
                <div class="col-md-2 col-sm-6 col-6">
                  <img src="img/others/logo1.png" class="img-fluid" alt="logo-1" />
                </div>
                <div class="col-md-2 col-sm-6 col-6">
                  <img src="img/others/logo2.png" class="img-fluid" alt="logo-2" />
                </div>
                <div class="col-md-2 col-sm-6 col-6">
                  <img src="img/others/logo3.png" class="img-fluid" alt="logo-3" />
                </div>
                <div class="col-md-2 col-sm-6 col-6">
                  <img src="img/others/logo4.png" class="img-fluid" alt="logo-4" />
                </div>
                <div class="col-md-2 col-sm-6 col-6">
                  <img src="img/others/logo5.png" class="img-fluid" alt="logo-5" />
                </div>
                <div class="col-md-2 col-sm-6 col-6">
                  <img src="img/others/logo6.png" class="img-fluid" alt="logo-6" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="interlet-section">
        <div class="container">
          <div class="interlet-heading heading text-center">
            <h3>The interlets Process</h3>
            <p class="text">We Adapt & Manage</p>
          </div>
          <div class="interlet-wrap">
            <div class="row">
              <div class="col-md-4">
                <div class="interlet-img">
                  <div class="icon-cont icon-img1">
                    <a href="#">
                      <img src="img/icons/icon11.png" class="img-fluid hover-img" alt="icon1">
                      <img src="img/icons/icon1.png" class="img-fluid over-img" alt="icon11">
                    </a>
                  </div>
                  <h5>Assessment</h5>
                  <p>During a consultation, your allocated property manager will assess your space. Before sending over a comprehensive proposal.</p>
                  <a href="#">
                    <i class="fa fa-long-arrow-right text-right" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="interlet-img">
                  <div class="icon-cont icon-img1">
                    <a href="#">
                      <img src="img/icons/icon12.png" class="img-fluid hover-img" alt="icon12">
                      <img src="img/icons/icon2.png" class="img-fluid over-img" alt="icon2">
                    </a>
                  </div>
                  <h5>Adaptation</h5>
                  <p>This is where we transform or tweak the interiors to insure your property is a direct match with its respective target market.</p>
                  <a href="#">
                    <i class="fa fa-long-arrow-right text-right" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
              <div class="col-md-4">
                <div class="interlet-img">
                  <div class="icon-cont icon-img1">
                    <a href="#">
                      <img src="img/icons/icon13.png" class="img-fluid hover-img" alt="icon13">
                      <img src="img/icons/icon3.png" class="img-fluid over-img" alt="icon3">
                    </a>
                  </div>
                  <h5>Results</h5>
                  <p>Your property goes live. This marks the beginning of where we begin generating industry leading results for you in terms of occupancy rates and overall </p>
                  <a href="#">
                    <i class="fa fa-long-arrow-right text-right" aria-hidden="true"></i>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="video-section"><img src="img/others/background-img.png" class="img-fluid" width="100%" alt="banner"></div> -->
      <div class="video-overlay-section">
        <div class="container">
          <div class="video-overlay-heading heading text-center">
            <h3>Watch Our Video</h3>
            <p class="text">Take a look and enjoy!</p>
            <img src="img/others/video-overlay.png" class="img-fluid play-btn" width="100%" alt="banner">
          </div>
        </div>
      </div>
      <div class="gallery-section">
        <div class="container">
          <div class="gallery-heading heading text-center">
            <h3>INTERLETS Properties</h3>
            <p class="text">We pride ourselves in Attention to Detail</p>
          </div>
          <div class="gallery-wrap">
            <div class="row">
              <div class="col-md-3 col-sm-6 col-6">
                <div class="gallery-cont-wrap">
                  <div class="gallery-img text-center">
                    <img src="img/others/gallery-1.png" class="img-fluid" alt="gallery-img" />
                  </div>
                  <div class="gallery-contents text-center">
                    <img src="img/others/gallery-key.png" class="img-fluid" alt="gallery-key" />
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-6">
                <div class="gallery-cont-wrap">
                  <div class="gallery-img text-center">
                    <img src="img/others/gallery-2.png" class="img-fluid" alt="gallery2-img" />
                  </div>
                  <div class="gallery-contents text-center">
                    <img src="img/others/gallery-key.png" class="img-fluid" alt="gallery-key" />
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-6">
                <div class="gallery-cont-wrap">
                  <div class="gallery-img text-center">
                    <img src="img/others/gallery-3.png" class="img-fluid" alt="gallery3-img" />
                  </div>
                  <div class="gallery-contents text-center">
                    <img src="img/others/gallery-key.png" class="img-fluid" alt="gallery-key" />
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-6">
                <div class="gallery-cont-wrap">
                  <div class="gallery-img text-center">
                    <img src="img/others/gallery-4.png" class="img-fluid" alt="gallery4-img" />
                  </div>
                  <div class="gallery-contents text-center">
                    <img src="img/others/gallery-key.png" class="img-fluid" alt="gallery-key" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="gallery-wrap text-center">
            <div class="row">
              <div class="col-md-3 col-sm-6 col-6">
                <div class="gallery-cont-wrap">
                  <div class="gallery-img text-center">
                    <img src="img/others/gallery-5.png" class="img-fluid" alt="gallery5-img" />
                  </div>
                  <div class="gallery-contents text-center">
                    <img src="img/others/gallery-key.png" class="img-fluid" alt="gallery-key" />
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-6">
                <div class="gallery-cont-wrap">
                  <div class="gallery-img text-center">
                    <img src="img/others/gallery-6.png" class="img-fluid" alt="gallery6-img" />
                  </div>
                  <div class="gallery-contents text-center">
                    <img src="img/others/gallery-key.png" class="img-fluid" alt="gallery-key" />
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-6">
                <div class="gallery-cont-wrap">
                  <div class="gallery-img text-center">
                    <img src="img/others/gallery-7.png" class="img-fluid" alt="gallery7-img" />
                  </div>
                  <div class="gallery-contents text-center">
                    <img src="img/others/gallery-key.png" class="img-fluid" alt="gallery-key" />
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-sm-6 col-6">
                <div class="gallery-cont-wrap">
                  <div class="gallery-img text-center">
                    <img src="img/others/gallery-8.png" class="img-fluid" alt="gallery8-img" />
                  </div>
                  <div class="gallery-contents text-center">
                    <img src="img/others/gallery-key.png" class="img-fluid" alt="gallery-key" />
                  </div>
                </div>
              </div>
            </div>
            <a class="btn" href="#">View All Gallery</a>
          </div>
        </div>
      </div>
      <div class="testi-section">
        <div class="container">
          <div class="testi-heading heading text-center">
            <h3>Testimonials</h3>
            <p class="text">Our guests love the INTERLETS experience</p>
          </div>
          <div class="testi-wrap">
            <div class="row">
              <div class="col-md-4">
                <div class="contents-wrap">
                  <img src="img/others/quotes.png" class="img-fluid" alt="quotes" />
                  <img src="img/others/testimonial-1.png" class="img-fluid rounded-circle" alt="testmonial-img" />
                  <p>"This. was an amazing apartment. Everything was perfect. The location is 2mins from The Dubai Mall, the view from the apartment is breathtaking"</p>
                  <h5>Fatima <span>
                      <img src="img/others/stars.png" class="img-fluid" alt="quotes" />
                    </span>
                  </h5>
                  <h6>Luxury Burj Khalifa facing Suite in the Sky</h6>
                </div>
              </div>
              <div class="col-md-4">
                <div class="contents-wrap">
                  <img src="img/others/quotes.png" class="img-fluid" alt="quotes" />
                  <img src="img/others/testimonial-2.png" class="img-fluid rounded-circle" alt="testmonial2-img" />
                  <p>"Had an amazing and relaxing stay at Christopher's Airbnb! Would definitely book again."</p>
                  <h5>Daritza <span>
                      <img src="img/others/stars.png" class="img-fluid" alt="quotes" />
                    </span>
                  </h5>
                  <h6>Luxurious Contemporary Suite</h6>
                </div>
              </div>
              <div class="col-md-4">
                <div class="contents-wrap">
                  <img src="img/others/quotes.png" class="img-fluid" alt="quotes" />
                  <img src="img/others/testimonial-3.png" class="img-fluid rounded-circle" alt="testmonia3-img" />
                  <p>"Fantastic alternative to hotel of you plan on being out and about during your stay in Dubai. Complex is hotel like in its standard, apartment is like a large one bed room suite, pool and gym facilities excellent. </p>
                  <h5>James <span>
                      <img src="img/others/stars.png" class="img-fluid" alt="quotes" />
                    </span>
                  </h5>
                  <h6>Luxurious Contemporary Suite</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="services-section">
        <div class="container">
          <div class="services-heading heading text-center">
            <h3>Interlets</h3>
            <p class="text">Industry insights laid out for you by the INTERLETS Team</p>
          </div>
          <div class="services-wrap">
            <div class="row">
              <div class="col-md-6">
                <div class="services-wrap-cont">
                  <div class="row">
                    <div class="col-lg-3 col-md-3">
                      <div class="services-img rounded-circle">
                        <img src="img/icons/icons1.png" class="img-fluid" alt="icon1" />
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                      <div class="services-content">
                        <h5>Furnishings</h5>
                        <p>At the top of the list is furniture since this is what your guest will be coming into physical contact with, and in many ways distinguish the vibe and functionality of your space. To achieve high occupancy</p>
                        <a href="#" class="btn">Read More</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="services-wrap-cont">
                  <div class="row">
                    <div class="col-lg-3 col-md-3">
                      <div class="services-img rounded-circle">
                        <img src="img/icons/icons2.png" class="img-fluid" alt="icon2" />
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                      <div class="services-content">
                        <h5>Cleanliness</h5>
                        <p>It is no secret that cleanliness is important. But as a host welcoming guests into your home even more so. Everyone’s standards are different which is why the cleaning of your space </p>
                        <a href="#" class="btn">Read More</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="services-wrap-cont">
                  <div class="row">
                    <div class="col-lg-3 col-md-3">
                      <div class="services-img rounded-circle">
                        <img src="img/icons/icons3.png" class="img-fluid" alt="icon3" />
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                      <div class="services-content">
                        <h5>Lighting</h5>
                        <p>With the growth in popularity of short-term rental platforms like Airbnb, Booking.com, Home and Away and Expedia, homeowners are increasingly taking advantage of the opportunity to gain an extra income.</p>
                        <a href="#" class="btn">Read More</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="services-wrap-cont">
                  <div class="row">
                    <div class="col-lg-3 col-md-3">
                      <div class="services-img rounded-circle">
                        <img src="img/icons/icons4.png" class="img-fluid" alt="icon4" />
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                      <div class="services-content">
                        <h5>Photography</h5>
                        <p>Much like your furniture and lighting, photography is a one-time investment which will have a continuous impact on how your property performs once listed online.</p>
                        <a href="#" class="btn">Read More</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="services-wrap-cont">
                  <div class="row">
                    <div class="col-lg-3 col-md-3">
                      <div class="services-img rounded-circle">
                        <img src="img/icons/icons5.png" class="img-fluid" alt="icon5" />
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                      <div class="services-content">
                        <h5>Maintenance</h5>
                        <p>Before advertising your space make sure everything in the space works. Your guests will be paying a premium for a serviced space, it is paramount that everything advertised is fully functioning and hassle free.</p>
                        <a href="#" class="btn">Read More</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="services-wrap-cont">
                  <div class="row">
                    <div class="col-lg-3 col-md-3">
                      <div class="services-img rounded-circle">
                        <img src="img/icons/icons6.png" class="img-fluid" alt="icon6" />
                      </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                      <div class="services-content">
                        <h5>Conclusion</h5>
                        <p>The role of a successful Airbnb Host is fun and exceptionally rewarding. Once your space is up to spec and you systemize your check-in, responsiveness and turnovers into a smooth process</p>
                        <a href="#" class="btn">Read More</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="faq-section">
        <div class="container">
          <div class="faq-cont heading text-center">
            <h3>FAQS</h3>
            <p class="text">Commonly asked questions</p>
          </div>
          <div class="faq-wrap">
            <div id="accordion4">
              <div class="row">
                <div class="col-md-6">
                  <div class="card">
                    <div class="card-header" id="headingfifty">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefifty" aria-expanded="true" aria-controls="collapsefifty">
                          <i class="fa" aria-hidden="true"></i> What is a Short-Term Rental property? </button>
                      </h5>
                      <div id="collapsefifty" class="collapse" aria-labelledby="headingfifty" data-parent="#accordion4">
                        <div class="card-body">
                          <p>The term refers to an apartment, villa or house which is made available for lease for shorter periods of time than conventional 3-12-month minimum term rental commitments.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <div class="card-header" id="headingfifty1">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefifty1" aria-expanded="false" aria-controls="collapsefifty1">
                          <i class="fa" aria-hidden="true"></i> Who are the DTCM? </button>
                      </h5>
                    </div>
                    <div id="collapsefifty1" class="collapse" aria-labelledby="headingfifty1" data-parent="#accordion4">
                      <div class="card-body">
                        <p>The term refers to an apartment, villa or house which is made available for lease for shorter periods of time than conventional 3-12-month minimum term rental commitments.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <div class="card-header" id="headingfifty2">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefifty2" aria-expanded="false" aria-controls="collapsefifty2">
                          <i class="fa" aria-hidden="true"></i> Why do Short-Term Rentals see greater returns <br> than Long-Term Rentals? </button>
                      </h5>
                    </div>
                    <div id="collapsefifty2" class="collapse" aria-labelledby="headingfifty2" data-parent="#accordion4">
                      <div class="card-body">
                        <p>The term refers to an apartment, villa or house which is made available for lease for shorter periods of time than conventional 3-12-month minimum term rental commitments.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <div class="card-header" id="headingfifty3">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefifty3" aria-expanded="false" aria-controls="collapsefifty3">
                          <i class="fa" aria-hidden="true"></i> How much can I make? </button>
                      </h5>
                    </div>
                    <div id="collapsefifty3" class="collapse" aria-labelledby="headingfifty3" data-parent="#accordion4">
                      <div class="card-body">
                        <p>The term refers to an apartment, villa or house which is made available for lease for shorter periods of time than conventional 3-12-month minimum term rental commitments.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <div class="card-header" id="headingfifty4">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefifty4" aria-expanded="false" aria-controls="collapsefifty4">
                          <i class="fa" aria-hidden="true"></i> Why do people choose Short-Term Rentals <br> over Hotels? </button>
                      </h5>
                    </div>
                    <div id="collapsefifty4" class="collapse" aria-labelledby="headingfifty4" data-parent="#accordion4">
                      <div class="card-body">
                        <p>The term refers to an apartment, villa or house which is made available for lease for shorter periods of time than conventional 3-12-month minimum term rental commitments.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card">
                    <div class="card-header" id="headingfifty5">
                      <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefifty5" aria-expanded="false" aria-controls="collapsefifty5">
                          <i class="fa" aria-hidden="true"></i> What is required from me should I wish <br> to proceed? </button>
                      </h5>
                    </div>
                    <div id="collapsefifty5" class="collapse" aria-labelledby="headingfifty5" data-parent="#accordion4">
                      <div class="card-body">
                        <p>The term refers to an apartment, villa or house which is made available for lease for shorter periods of time than conventional 3-12-month minimum term rental commitments.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="contact1-section">
        <div class="container">
          <div class="contact-wrap">
            <div class="offer-cont heading text-center">
              <h3 class="removable-media">Contact us Today</h3>
              <p class="text removable-media">Speak to our team to learn more</p>
            </div>
            <form class="removable-media" action="" method="post" autocomplete="off">
              <div class="row">
                <div class="col-md-6">
                  <div id="firstNameDiv" class="form-group">
                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" required="" />
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="lastNameDiv" class="form-group">
                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" required="" />
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="phoneDiv" class="form-group">
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Phone Number" required="" />
                    <input type="hidden" name="country_code" id="country_code" value="44" />
                    <input type="hidden" name="country_name" id="country_name" value="United Kingdom" placeholder="Country">
                    <span id="valid-msg" class="hide">✓ Valid</span>
                    <span id="error-msg" class="hide"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="emailDiv" class="form-group">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Email Address" required="" />
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea class="col-md-12 cont" placeholder="Message"></textarea>
                  </div>
                </div>
                <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>" />
                <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>" />
              </div>
            </form>
            <div class="contact-btn equip text-center">
            <button type="button" class="btn removable-media" id="btn-submit">Submit Now</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <footer>
      <div class="footer-section text-center">
        <div class="container">
          <div class="footer-wrap">
            <div class="contact-content">
              <div class="row">
                <div class="col-md-4 col-sm-12 col-12">
                  <div class="contact-cont co-1">
                    <ul class="list-unstyled d-inline-flex">
                      <li>
                        <a href="mailto:info@interlets.com">
                          <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                      </li>
                      <li>
                        <a href="mailto:info@interlets.com">
                          <h4>EMAIL</h4>
                          <span>info@interlets.com </span>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-4 col-sm-12 col-12">
                  <div class="contact-cont">
                    <ul class="list-unstyled d-inline-flex">
                      <li>
                        <a href="tel:949-378-5738">
                          <i class="fa fa-map-marker" aria-hidden="true"></i>
                        </a>
                      </li>
                      <li>
                        <a href="tel:949-378-5738">
                          <h4>OUR LOCATION</h4>
                          <span>Dubai, UAE</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div class="col-md-4 col-sm-12 col-12">
                  <div class="contact-cont pho">
                    <ul class="list-unstyled d-inline-flex">
                      <li>
                        <a href="tel:+971 525 863 475"></a>
                        <i class="fa fa-phone" aria-hidden="true"></i>
                      </li>
                      <li>
                        <a href="tel:+971 525 863 475">
                          <h4>PHONE NUMBER</h4>
                          <span>+971 525 863 475</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="footer-logo">
              <a href="index.php">
                <img src="img/foot-logo.png" class="img-fluid" alt="footer-logo" />
              </a>
            </div>
            <div class="footer-menu text-center">
              <ul class="list-unstyled d-inline-flex">
                <li>
                  <a href="index.php">Home </a>
                </li>
                <li>
                  <a href="#">About</a>
                </li>
                <li>
                  <a href="#">Properties</a>
                </li>
                <li>
                  <a href="#">How To Let</a>
                </li>
                <li>
                  <a href="#">Faqs</a>
                </li>
                <li>
                  <a href="#">Testimonials</a>
                </li>
                <li>
                  <a href="#"> Contact Us</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="copyrights-sec text-center">
            <div class="social-icons">
              <ul class="list-unstyled d-inline-flex d-flex">
                <li>
                  <a href="https://www.facebook.com/" target="blank">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.twitter.com/" target="blank">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.google.com/" target="blank">
                    <i class="fa fa-google-plus" aria-hidden="true"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.pinterest.com/" target="blank">
                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                  </a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/" target="blank">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="copyrights">
            <p>&copy; Copyrights 2022 Interlets | All Rights Reserved</p>
          </div>
        </div>
      </div>
      </div>
    </footer>
    <div class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">X</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="top-container">
              <iframe width="100%" height="500" src="https://www.youtube.com/embed/5Peo-ivmupE" title="Lorem ipsum video - Dummy video for your website" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        <!--/.modal-content -->
      </div>
      <!--/.modal-dialog -->
    </div>
    <!-- JavaScript Libraries -->
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
    <!-- <script type="text/javascript">
      function PopUp(hideOrshow) {
        if (hideOrshow == 'hide') document.getElementById('ac-wrapper').style.display = "none";
        else document.getElementById('ac-wrapper').removeAttribute('style');
      }
      window.onload = function() {
        setTimeout(function() {
          PopUp('show');
        }, 2000);
      }
    </script> -->
    <!-- <script type="text/javascript">
      $('.play-btn').on('click', function(event) {
        $(".modal").modal('show');
      });
      $('.close').on('click', function(event) {
           $(".modal").modal('hide');
      });
    </script> -->
    <!-- teleinput for country code js -->
    <script src="intlTelInput/js/intlTelInput.min.js"></script>
    <script src="intlTelInput/js/utils.js"></script>
    <script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=KNRG-4LNQ-RLJU-FYAN&load=InternationalTelephoneValidation,EmailValidation"></script>
    <script type="text/javascript">
      var validEmail = false;
      var validPhone = false;
      $("#btn-submit").click(function(e) {
          var first_name = $("#first_name").val();
          var last_name = $("#last_name").val();
          var email = $("#email").val();
          var phoneNumber = $("#phone").val();
      
          if (first_name.length <= 1) {
              $('#firstNameDiv p').remove('');
              $('#firstNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >First Name is required!</p>');
              return false;
          } else {
              $('#firstNameDiv p').remove('');
          }
          if (last_name.length <= 1) {
              $('#lastNameDiv p').remove('');
              $('#lastNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >Last Name is required!</p>');
              return false;
          } else {
              $('#lastNameDiv p').remove('');
          }
          if (email.length <= 1) {
              $('#emailDiv p').remove('');
              $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Email is required!</p>');
              return false;
          } else {
              $('#emailDiv p').remove('');
          }
          if (phoneNumber.length <= 1) {
              $('#phoneDiv p').remove('');
              $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Phone Number is required!</p>');
              return false;
          } else {
              $('#phoneDiv p').remove('');
          }
        
          if (validEmail == true && validPhone == true) {
              $.ajax({
                  type: "POST",
                  url: "formToLead.php",
                  data: {
                      first_name: $("#first_name").val(),
                      last_name: $("#last_name").val(),
                      email: $("#email").val(),
                      phone: $("#phone").val(),
                      country: $("#country_name").val(),
                      lid: $("#lid").val(),
                      aid: $("#aid").val()
                  },
                  success: function(result) {
                      console.log(result);
                      if (result == "success") {
                        $("h3,p,form,button").remove(".removable-media");
                        $(".offer-cont").append('<h3>Thank you!</h3>');
                      }
                  },
                  error: function(result) {
                      console.log(result);
                      $("h3,p,form,button").remove(".removable-media");
                      $(".offer-cont").append('<h3>Thank you!</h3>');
                  }
              });
          }
      });
      
      $(document).ready(function() {
          //country code script
          var input = document.querySelector("#phone"),
              errorMsg = document.querySelector("#error-msg"),
              validMsg = document.querySelector("#valid-msg");
      
          // Here, the index maps to the error code returned from getValidationError
          var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
      
          // Get country code
          var countryData = window.intlTelInputGlobals.getCountryData(),
              country_code = document.querySelector("#country_code");
          // Initialise plugin
          var iti = window.intlTelInput(input, {
              utilsScript: "intlTelInput/js/utils.js",
              initialCountry: 'gb'
          });
          // Set it's initial value
          var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
          // console.log(all_countrycode);
        
          // Listen to the telephone input for changes
          input.addEventListener('countrychange', function(e) {
              var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
              // console.log(selected_countrycode);
              var selected_countryname = country_code.value = iti.getSelectedCountryData().name;
              // console.log(selected_countryname);
              $("#country_code").val(selected_countrycode);
              $("#country_name").val(selected_countryname);
          });
        
          var reset = function() {
              input.classList.remove("error");
              errorMsg.innerHTML = "";
              errorMsg.classList.add("hide");
              validMsg.classList.add("hide");
          };
        
          // On blur: validate
          input.addEventListener('blur', function() {
              reset();
          });
        
          // on keyup / change flag: reset
          input.addEventListener('change', reset);
          input.addEventListener('keyup', reset);
        
          // Validate email
          $("#email").change(function() {
              var email = $("#email").val();
              level = 'Address';
              // alert(email);
              var emailvalidation = new data8.emailvalidation();
              emailvalidation.isvalid(
                  email,
                  level,
                  [
                  
                  ],
                  showIsEmailValid
              );
          });
        
          function showIsEmailValid(result) {
              console.log(result.Result);
              if (result.Result == 'Valid') {
                  validEmail = true;
                  $('#emailDiv p').remove('');
                  $("#btn-submit").attr('disabled', false);
              } else {
                validEmail = false;
                $('#emailDiv p').remove('');
                $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Email Address</p>');
                $("#btn-submit").attr('disabled', true);
              }
          }
        
          $('select#country_code').on('change', function() {
              var country_code = $(this).children("option:selected").text();
              var code = country_code.replace('+', '');
              // console.log(code);
              $('#country_code').val(code);
          })
        
          $("#phone").bind("change", function(e) {
              var telephoneNumber = $("#phone").val();
              if ($("#country_code").val() == 'gb') {
                  defaultCountry = 'GB';
                  var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
                  internationaltelephonevalidation.isvalid(
                      telephoneNumber,
                      defaultCountry,
                      [
                          new data8.option('UseMobileValidation', 'false'),
                          new data8.option('UseLineValidation', 'false'),
                          new data8.option('RequiredCountry', ''),
                          new data8.option('AllowedPrefixes', ''),
                          new data8.option('BarredPrefixes', ''),
                          new data8.option('UseUnavailableStatus', 'true'),
                          new data8.option('UseAmbiguousStatus', 'true'),
                          new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                          new data8.option('ExcludeUnlikelyNumbers', 'false')
                      ],
                      showIsPhoneNumberValid
                  );
              } else {
                  defaultCountry = 'GB';
                  var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
                  internationaltelephonevalidation.isvalid(
                      telephoneNumber,
                      $("#country_code").val(),
                      [
                          new data8.option('UseMobileValidation', 'false'),
                          new data8.option('UseLineValidation', 'false'),
                          new data8.option('RequiredCountry', ''),
                          new data8.option('AllowedPrefixes', ''),
                          new data8.option('BarredPrefixes', ''),
                          new data8.option('UseUnavailableStatus', 'true'),
                          new data8.option('UseAmbiguousStatus', 'true'),
                          new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                          new data8.option('ExcludeUnlikelyNumbers', 'false')
                      ],
                      showIsPhoneNumberValid
                  );
              }
          });
        
          function showIsPhoneNumberValid(result) {
              console.log(result.Result.ValidationResult);
              if (result.Result.ValidationResult == 'Valid') {
                  validPhone = true;
                  $('#phoneDiv p').remove('');
                  $("#btn-submit").attr('disabled', false);
              } else {
                  validPhone = false;
                  $('#phoneDiv p').remove('');
                  $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Phone Number</p>');
                  $("#btn-submit").attr('disabled', true);
              }
          }
      });
    </script>
  </body>
</html>html