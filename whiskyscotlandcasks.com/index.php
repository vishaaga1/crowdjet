<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Whisky Scotland - Cask Guide</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:500,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,600&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/fontawesome-all.css" rel="stylesheet">
    <link href="css/swiper.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="css/styles.css" rel="stylesheet">
    <!--- intlTelInput CSS for Country Code --->
    <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css">
    <link href="images/favicon.png" rel="apple-touch-icon" />
    <link rel="icon" href="images/favicon.png">
  </head>
  <body data-spy="scroll" data-target=".fixed-top">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-md navbar-dark navbar-custom fixed-top">
      <a class="navbar-brand logo-image" href="#"><img src="images/logo.png" alt="alternative"></a>
      <!-- Mobile Menu Toggle Button -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-awesome fas fa-bars"></span>
        <span class="navbar-toggler-awesome fas fa-times"></span>
      </button>
      <!-- end of Mobile Menu Toggle Button -->
      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav ml-auto">
          <!-- <li class="nav-item">
            <a class="nav-link page-scroll" href="#header">Benefits <span class="sr-only">(current)</span></a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link page-scroll" href="#intro">Why invest in Whisky? <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link page-scroll" href="#services">How does it work?</a>
          </li>
        </ul>
        <span class="nav-item social-icons">
          <a href="#" class="navBtn" data-toggle="modal" data-target="#basicModal">Download your free guide </a>
        </span>
      </div>
    </nav>
    <!-- end of Navbar -->
    
    <!-- Intro -->
    <div id="intro" class="basic-1">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="text-center head1">
              <h2>WHISKY WAS THE BEST PERFORMING ASSET CLASS OF 2020.</h2>
            </div>
            <div class="first_screen_procent_tiles" data-da="first_screen_content,3,992">
              <div class="first_screen_procent_item">
                <div class="fsp_content">
                  <div class="fsp_title">Whisky Cask Market Returns</div>
                  <div class="fsp_number">8-12%</div>
                  <div class="fsp_description">Average return per annum over the last 10 years</div>
                </div>
              </div>
              <div class="first_screen_procent_item">
                <div class="fsp_content">
                  <div class="fsp_title">Capital Gains Tax Exempt</div>
                  <div class="fsp_number">0% TAX</div>
                  <div class="fsp_description">Unlike other investments, all returns are tax free</div>
                </div>
              </div>
            </div>
            <p class="textHead">We introduce investors to a billion-pound market with average returns of 8-12%* over recent decades.</p>
            <a href="#" class="navBtn" data-toggle="modal" data-target="#basicModal" style="width: 360px;height: 54px; font-size: 18px; margin: auto;">Download your free guide </a>
          </div>
        </div>
      </div>
    </div>
    <!-- end of Intro -->
    
    <div class="">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="number_items">
              <div class="number_item">
                <div class="number_items_content">
                  <div class="number_item_img">
                    <img src="images/Frame-52_3.svg" alt="logo">
                  </div>
                  <div class="number_item_num" style="color: #9c6137;font-size: 38px">4.5BN</div>
                  <div class="number_item_text" style="color: #9c6137;font-size: 17px">Annual Export Value Of Scotch</div>
                </div>
              </div>
              <div class="number_item">
                <div class="number_items_content">
                  <div class="number_item_img">
                    <img src="images/Frame-52_1.svg" alt="logo">
                  </div>
                  <div class="number_item_num" style="color: #9c6137;font-size: 38px">44</div>
                  <div class="number_item_text" style="color: #9c6137;font-size: 17px">Bottles exported every second</div>
                </div>
              </div>
              <div class="number_item">
                <div class="number_items_content">
                  <div class="number_item_img">
                    <img src="images/Frame-52_2.svg" alt="logo">
                  </div>
                  <div class="number_item_num" style="color: #9c6137;font-size: 38px">180</div>
                  <div class="number_item_text" style="color: #9c6137;font-size: 17px">Markets Scotch Whisky Is Exported to globally</div>
                </div>
              </div>
              <div class="number_item">
                <div class="number_items_content">
                  <div class="number_item_img">
                    <img src="images/Frame-52.svg" alt="logo">
                  </div>
                  <div class="number_item_num" style="color: #9c6137;font-size: 38px">0%</div>
                  <div class="number_item_text" style="color: #9c6137;font-size: 17px">Classed as a wasting asset, returns are tax free</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <section class="contImg">
      <div class="row">
        <div class="col-lg-6">
          <div class="clrSec">
            <div class="section_block_title">
              <div class="title">
                Whisky casks have achieved sizeable returns for decades. Our specialist team is with you every step of the way throughout your investment journey from sourcing the best casks for your portfolio to leveraging our industry expertise and contacts when it comes to time to sell.
              </div>
              <div class="text_after_tiles">
                We allow private investors to buy top-quality casks at wholesale prices. You even have the option of selling your cask back to us, making the selling process as easy as possible for our clients!
              </div>
            </div>
            <a href="#" class="navBtn" data-toggle="modal" data-target="#basicModal" style="width: 360px;height: 54px; font-size: 18px; margin: auto; margin-top: 15px;">Download your free guide </a>
          </div>
        </div>
        <div id="services" class="col-lg-6">
          <div class="contdiv">
            <div class="title">Why Invest In Whisky?</div>
            <div class="list_with_logo">
              <img src="images/diagram.svg" alt="">Effortless – Scotch Whisky naturally appreciates with time.
            </div>
            <div class="list_with_logo">
              <img src="images/house.svg" alt="">Safe – Unlike other investments, your physical item is insured and stored in a secure warehouse.
            </div>
            <div class="list_with_logo">
              <img src="images/pound1.svg" alt="">Resilient – A medium to long-term investment to protect against inflation.
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- <section class="contImg">
      <div class="row">
        <div class="col-lg-6">
          <div class="contdiv">
            <div class="title">
              A GROWING MARKET
            </div>
            <div class="text_after_tiles_my">
              The world whisky market is expanding rapidly with global sales estimated to climb. An incredible 41 bottles were shipped overseas every second in 2018 with Scotch being exported into 175 markets around the world. Looking ahead, there is an expectation that the global single malt whisky market revenue could reach US$3.43 billion in 2025 driven by the rapid growth in Scotch whisky exports combined with the burgeoning middle-classes in Asia Paciﬁc, which represents the biggest global market for single malt.<br><br>
              Historically, Scotch Whisky has delivered average returns of between 8-12% a year.<br><br>
              The age of whisky is a big indicator of quality, with older whiskies commanding higher prices due to their desirability.
            </div>
            <a href="#" class="navBtn" data-toggle="modal" data-target="#basicModal" style="width: 360px;height: 54px; font-size: 18px; margin: auto; margin-top: 15px;">Download your free guide </a>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="clrSec bgImg3"></div>
        </div>
      </div>
    </section> -->
    
    <!-- Copyright -->
    <div class="copyright">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <p class="p-small">Copyright © 2019 WHISKY SCOTLAND LTD - COMPANY REG NO: SC648236<br>
            </p>
            <div class="footer_links">
              <div class="footer_link">
                <a href="#">TERMS</a>
              </div>
              <div class="footer_link">
                <a href="#">PRIVACY</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end of copyright -->
    
    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <p type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </p>
          </div>
          <div class="modal-body text-center">
            <img class="popLogo" src="images/logo.png">
            <h3 class="popHead">Complete the simple form to download your FREE CASK Investment Guide</h3>
            <div class="form_holder">
              <form id="msform" action="" method="post" autocomplete="off">
                <ul id="progressbar">
                  <li class="active"></li>
                  <li></li>
                  <li></li>
                </ul>
                <fieldset>
                  <h2 class="fs-title">ARE YOU 18 OR OVER?</h2>
                  <input type="button" name="next" class="next action-button" value="Yes" />
                  <input type="button" name="next" class="next action-button btnBorder" value="No" />
                </fieldset>
                <fieldset>
                  <h2 class="fs-title">HAVE YOU EVER PURCHASED A<br>WHISKY CASK BEFORE?</h2>
                  <input type="button" name="next" class="next action-button" value="Yes" />
                  <input type="button" name="next" class="next action-button btnBorder" value="No" />
                </fieldset>
                <fieldset class="detailsForm">
                  <div class="row">
                    <div id="firstNameDiv" class="col-lg-6">
                      <label>FIRST NAME*</label>
                      <input class="inField" type="text" id="first_name" name="first_name" placeholder="First Name" required="" />
                    </div>
                    <div id="lastNameDiv" class="col-lg-6">
                      <label>LAST NAME*</label>
                      <input class="inField" type="text" name="last_name" id="last_name" placeholder="Last Name" />
                    </div>
                    <div id="emailDiv" class="col-lg-6">
                      <label>EMAIL*</label>
                      <input class="inField email" type="email" name="email" id="email" placeholder="Email Address" required="" />
                    </div>
                    <div id="phoneDiv" class="col-lg-6">
                      <label>PHONE*</label>
                      <input class="inField telephone" type="text" name="phone" id="phone" placeholder="Telephone Number" required="" style="padding-left: 50px !important;" />
                      <input type="hidden" name="country_code" id="country_code" value="44" />
                      <input type="hidden" name="country_name" id="country_name" value="United Kingdom" placeholder="Country">
                      <span id="valid-msg" class="hide">✓ Valid</span>
                      <span id="error-msg" class="hide"></span>
                    </div>
                    <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>" />
                    <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>" />
                  </div>
                  <input type="button" name="previous" class="previous action-button" value="Previous" />
                  <!-- <input type="submit" name="submit" class="submit action-button" value="GET YOUR GUIDE" /> -->
                  <button type="button" id="btn-submit" class="submit action-button">GET YOUR GUIDE</button>
                </fieldset>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Scripts -->
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/swiper.min.js"></script> 
    <script src="js/jquery.magnific-popup.js"></script> 
    <script src="js/morphext.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/validator.min.js"></script>
    <script src="js/scripts.js"></script>
    <!-- Custom scripts -->
    <script>
      $(function() {
        
        var current_fs, next_fs, previous_fs;
        var left, opacity, scale;
        var animating;
        $(".next").click(function() {
          if(animating) return false;
          animating = true;
          
          current_fs = $(this).parent();
          next_fs = $(this).parent().next();
          $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
          next_fs.show();
          current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
              scale = 1 - (1 - now) * 0.2;
              left = (now * 50)+"%";
              opacity = 1 - now;
              current_fs.css({'transform': 'scale('+scale+')'});
              next_fs.css({'left': left, 'opacity': opacity});
            },
            duration: 1000,
            complete: function() {
              current_fs.hide();
              animating = false;
            },
            easing: 'easeInOutBack'
          });
        });
        
        $(".previous").click(function() {
          if(animating) return false;
          animating = true;
          
          current_fs = $(this).parent();
          previous_fs = $(this).parent().prev();
          $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
          previous_fs.show();
          
          current_fs.animate({opacity: 0}, {
            step: function(now, mx) {
              scale = 0.8 + (1 - now) * 0.2;
              left = ((1-now) * 50)+"%";
              opacity = 1 - now;
              current_fs.css({'left': left});
              previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
            },
            duration: 1000,
            complete: function() {
              current_fs.hide();
              animating = false;
            },
            easing: 'easeInOutBack'
          });
        });
        
        $(".submit").click(function() {
          return false;
        })
      });
    </script>
    <!--- teleinput for country code js --->
    <script src="intlTelInput/js/intlTelInput.min.js"></script>
    <script src="intlTelInput/js/utils.js"></script>
    <script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=BUJ7-N7PQ-GFEA-7KSK&load=InternationalTelephoneValidation,EmailValidation"></script>
    <script type="text/javascript">
    var validEmail = false;
    var validPhone = false;
    $("#btn-submit").click(function(e) {
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email").val();
        var phoneNumber = $("#phone").val();
        
        if(first_name.length <= 1) {
            $('#firstNameDiv p').remove('');
            $('#firstNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >First Name is required!</p>');
            return false;
        }
        else {
            $('#firstNameDiv p').remove('');
        }
        if(last_name.length <= 1) {
            $('#lastNameDiv p').remove('');
            $('#lastNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >Last Name is required!</p>');
            return false;
        }
        else {
            $('#lastNameDiv p').remove('');
        }
        if(email.length <= 1) {
            $('#emailDiv p').remove('');
            $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Email is required!</p>');
            return false;
        }
        else {
            $('#emailDiv p').remove('');
        }
        if(phoneNumber.length <= 1) {
            $('#phoneDiv p').remove('');
            $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Phone Number is required!</p>');
            return false;
        }
        else {
            $('#phoneDiv p').remove('');
        }

        if(validEmail == true && validPhone == true) {
            $.ajax( {
                type: "POST",
                url: "formToLead.php",
                data: { 
                    first_name: $("#first_name").val(),
                    last_name: $("#last_name").val(),
                    email: $("#email").val(),
                    phone: $("#phone").val(),
                    country: $("#country_name").val(),
                    lid: $("#lid").val(),
                    aid: $("#aid").val()
                },
                success: function(result) {
                    console.log(result);
                    if(result=="success") {
                        location.href = "index.php";
                        location.href = "Scottish Whisky Investment Prospectus.pdf";

                    }
                },
                error: function(result) {
                    console.log(result);
                    location.href = "index.php";
                    location.href = "Scottish Whisky Investment Prospectus.pdf";
                }
            });
        }
    });

    $( document ).ready(function() {
        //country code script
        var input = document.querySelector("#phone"),
        errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg");
        
        // Here, the index maps to the error code returned from getValidationError
        var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

        // Get country code
        var countryData = window.intlTelInputGlobals.getCountryData(),
        country_code = document.querySelector("#country_code");
        // Initialise plugin
        var iti = window.intlTelInput(input, {
            utilsScript: "intlTelInput/js/utils.js",
            initialCountry: 'gb'
        });
        // Set it's initial value
        var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
        // console.log(all_countrycode);
        
        // Listen to the telephone input for changes
        input.addEventListener('countrychange', function(e) {
            var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
            // console.log(selected_countrycode);
            var selected_countryname = country_code.value = iti.getSelectedCountryData().name;
            // console.log(selected_countryname);
            $("#country_code").val(selected_countrycode);
            $("#country_name").val(selected_countryname);
        });
        
        var reset = function() {
            input.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };
        
        // On blur: validate
        input.addEventListener('blur', function() {
            reset();
        });
        
        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
        
        // Validate email
        $("#email").change(function() {
            var email = $("#email").val();
            level='Address';
            // alert(email);
            var emailvalidation = new data8.emailvalidation();
            emailvalidation.isvalid(
                email,
                level,
                [

                ],
                showIsEmailValid
            );
        });
        
        function showIsEmailValid(result) {
            console.log(result.Result);
            if(result.Result == 'Valid') {
                validEmail = true;
                $('#emailDiv p').remove('');
                $("#btn-submit").attr('disabled',false); 
            }
            else {
                validEmail = false;
                $('#emailDiv p').remove('');
                $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Email</p>');
                $("#btn-submit").attr('disabled',true);
            }
        }
        
        $('select#country_code').on('change',function() {
            var country_code =  $(this). children("option:selected").text();
            var code = country_code.replace('+','');
            // console.log(code);
            $('#country_code').val(code);
        })
        
        $("#phone").bind("change", function(e) {
            var telephoneNumber = $("#phone").val();
            if($("#country_code").val() == 'gb') {
                defaultCountry='GB';
                var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
                internationaltelephonevalidation.isvalid(
                    telephoneNumber,
                    defaultCountry,
                    [
                        new data8.option('UseMobileValidation', 'false'),
                        new data8.option('UseLineValidation', 'false'),
                        new data8.option('RequiredCountry', ''),
                        new data8.option('AllowedPrefixes', ''),
                        new data8.option('BarredPrefixes', ''),
                        new data8.option('UseUnavailableStatus', 'true'),
                        new data8.option('UseAmbiguousStatus', 'true'),
                        new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                        new data8.option('ExcludeUnlikelyNumbers', 'false')
                    ],
                    showIsPhoneNumberValid
                );
            }
            else {
                defaultCountry='GB';
                var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
                internationaltelephonevalidation.isvalid(
                    telephoneNumber,
                    $("#country_code").val(),
                    [
                        new data8.option('UseMobileValidation', 'false'),
                        new data8.option('UseLineValidation', 'false'),
                        new data8.option('RequiredCountry', ''),
                        new data8.option('AllowedPrefixes', ''),
                        new data8.option('BarredPrefixes', ''),
                        new data8.option('UseUnavailableStatus', 'true'),
                        new data8.option('UseAmbiguousStatus', 'true'),
                        new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                        new data8.option('ExcludeUnlikelyNumbers', 'false')
                    ],
                    showIsPhoneNumberValid
                );
            }
        });
        function showIsPhoneNumberValid(result) {
            console.log(result.Result.ValidationResult);
            if(result.Result.ValidationResult == 'Valid') {
                validPhone = true;
                $('#phoneDiv p').remove('');
                $("#btn-submit").attr('disabled',false);
            }
            else {
                validPhone = false;
                $('#phoneDiv p').remove('');
                $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Phone Number</p>');
                $("#btn-submit").attr('disabled',true);
            }
        }
    });
    </script>
  </body>
</html>