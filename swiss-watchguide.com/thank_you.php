<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Swiss Watch Guide | DOWNLOAD YOUR FREE WATCH INVESTMENT GUIDE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link href="favicon.png" rel="apple-touch-icon" />
	<link href="images/favicon.png" rel="icon" type="image/png" />
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
         fbq('init', '767634384464922'); 
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" 
        src="https://www.facebook.com/tr?id=767634384464922&ev=PageView
        &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>
<header>
  <div class="container">
  	<div class="row row_order">
    	<div class="col-sm-12 my-auto">
        	<div class="logo">
            	<a href="index.html"><img src="images/logo.png" class="img-fluid" /></a>
            </div>
        </div>
    	<div class="col-sm-12 my-auto"></div>
    </div>
  </div>
</header>
<section class="middle-content">
	<div class="container">
    	<div class="row">
        	<div class="col-lg-7">
            	<div class="row">
                	<div class="col-lg-10">
                        <div class="left-side">
                            <h2>INVESTORS SEE AN AVERAGE<br />
                                <span>TAX-FREE RETURN OF 20%+ PER ANNUM</span><br />
                                CAPITAL GAINS TAX EXEMPT<br />
                            </h2>            
                            <h1>According to the Knight Frank Luxury Investment Index Results, Watches had their strongest annual performance since 2018</h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
            	<div class="right-side">
                	<h2>DOWNLOAD YOUR FREE<br /> WATCH INVESTMENT GUIDE</h2>
                    <form action="#" method="post">
                        <div>
                            <div class="row send_sms">
                                <div class="col-md-12 alert success-message" style="margin-top: 20px;">
                                    <img src="images/checked_icon.png" class="checked_icon"><br>
                                    <p style="font-size: 20px;font-family: arial;">The guide you requested is now available.</p>
                                    <p>Thank you for registering your interest in our free guide. Please click the button below to start downloading your free guide.</p>
                                </div>
                            </div>
                           
                            <div class="col-md-12">
                                <button type="submit" id="submit" name="submit">
                                    <a href="SWG-Brochure.pdf" download style="color: #fff !important;"><img src="images/download-icon.png" class="img-fluid" />REQUEST BROCHURE</a>
                                </button>
                            </div>
                            <div class="col-md-12">
                            	<p></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- jQuery library --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/function.js"></script>
</body>
</html>
