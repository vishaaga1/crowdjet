<?php
include ('ZohoCrmClient.php');
$crmObj = new ZohoCrmClient();
$request = urldecode(file_get_contents('php://input'));
file_put_contents('input_file.json', file_get_contents('php://input'));
parse_str($request, $output);
// Get all the Form fields value
$firstname = isset($output['fields']['firstname']['value']) ? $output['fields']['firstname']['value'] : '';
$lastname = isset($output['fields']['lastname']['value']) ? $output['fields']['lastname']['value'] : '';
$email = isset($output['fields']['email']['value']) ? $output['fields']['email']['value'] : '';
$phone = isset($output['fields']['phonenumber']['value']) ? $output['fields']['phonenumber']['value'] : '';
// Remove the special characters form the phone number
$phoneNumber = preg_replace('/[^0-9]/', '', $phone);
$lid = isset($output['fields']['lid']['value']) ? $output['fields']['lid']['value'] : NULL;
$aid = isset($output['fields']['aid']['value']) ? $output['fields']['aid']['value'] : NULL;
$country_code = isset($output['fields']['country_code']['value']) ? $output['fields']['country_code']['value'] : '';
$country_name = isset($output['fields']['country_name']['value']) ? $output['fields']['country_name']['value'] : '';
$state = isset($output['fields']['state']['value']) ? $output['fields']['state']['value'] : '';
$content_array = [];
$array_format = [];
$content_array[] = array(
    'First_Name'=>$firstname,
    'Last_Name'=>$lastname,
    'Email'=>$email,
    'Phone'=>$phoneNumber,
    'ClickID'=>$lid,
    'Company'=>$aid,
    'Country'=>'Italy',
    'State'=>$state,
    'Lead_Source'=>'WS-ITALY'
);
$array_format['data']=$content_array;
// file_put_contents('formSubmit.json', json_encode($array_format));
$leadinfo = $crmObj->createZohoCrmRecord('Leads',$array_format);
file_put_contents('success.json', $leadinfo);
?>