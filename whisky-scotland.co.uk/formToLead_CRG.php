<?php
include ('ZohoCrmClient.php');
$crmObj = new ZohoCrmClient();
$request = urldecode(file_get_contents('php://input'));
file_put_contents('input_file.json', file_get_contents('php://input'));
parse_str($request, $output);
//echo '<pre>';print_r($output);die;
$firstname = isset($output['fields']['firstname']['value']) ? $output['fields']['firstname']['value'] : '';
$lastname = isset($output['fields']['lastname']['value']) ? $output['fields']['lastname']['value'] : '';
$email = isset($output['fields']['email']['value']) ? $output['fields']['email']['value'] : '';
$mobile = isset($output['fields']['phonenumber']['value']) ? $output['fields']['phonenumber']['value'] : '';
$lid = isset($output['fields']['lid']['value']) ? $output['fields']['lid']['value'] : NULL;
$aid = isset($output['fields']['aid']['value']) ? $output['fields']['aid']['value'] : NULL;
$country_code = isset($output['fields']['country_code']['value']) ? $output['fields']['country_code']['value'] : '';
$country_name = isset($output['fields']['country_name']['value']) ? $output['fields']['country_name']['value'] : '';
$content_array = [];
$array_format = [];
$content_array[] = array(
    'First_Name'=>$firstname,
    'Last_Name'=>$lastname,
    'Email'=>$email,
    'Phone'=>$mobile,
    'ClickID'=>$lid,
    'Company'=>$aid,
    'Country'=>$country_name,
    'Lead_Source'=>'WS CRG'
);
$array_format['data']=$content_array;
$leadinfo = $crmObj->createZohoCrmRecord('Leads',$array_format);
file_put_contents('success.json', $leadinfo);
?>