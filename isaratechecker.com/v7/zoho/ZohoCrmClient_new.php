<?php
// error_reporting(0);
Class ZohoCrmClient { 

    public function getAccessToken(){ 
    //     if(session_id() == '') {
    // // session_start();
    // }
        //session_start();
        $crm_config = array(
           'client_id' => '1000.WIV78CDF03LR7WU0ZR8Y5PI0UXAG0A',
            'client_secret' => '9a875c5a40df1b420216c063207f92f1e60599a9e7',
            'refresh_token' => '1000.5e8009c0ce3b9afc546a193888057e57.13525bf6852188149bf04b79d48dd23d',
            'grant_type' => 'refresh_token',
            'api_url' => 'https://www.zohoapis.eu/crm/v2/',
            'account_domain' => 'https://accounts.zoho.eu/',
        );

        $zoho_crm_session = file_get_contents('access_token.json');
        $zoho_crm_session = json_decode($zoho_crm_session);
        $current_time = date('Y-m-d H:i:s');
        // print_r($zoho_crm_session);


        if(isset($zoho_crm_session->access_token) && ($current_time<$zoho_crm_session->expiring_at)){
            return $zoho_crm_session;   
        }else{
         $url = $crm_config['account_domain'].'oauth/v2/token';
         $postData = [
            'client_id'=>$crm_config['client_id'],
            'client_secret'=>$crm_config['client_secret'],
            'refresh_token'=>$crm_config['refresh_token'],
            'grant_type'=>$crm_config['grant_type']
        ];
            $ch = curl_init(); // Create a curl handle
            curl_setopt($ch, CURLOPT_URL, $url); // Third party API URL
            curl_setopt($ch, CURLOPT_POST, FALSE);  // To set POST method true
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); // To send data to the API URL
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // To set SSL Verifier false
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // To set return response from the API
             $response = curl_exec($ch); // To execute the handle and get the response 
             $response = json_decode($response);
             if(isset($response->access_token)){
                $now = time();
                $ten_minutes = $now + (30 * 60);
                $startDate = date('Y-m-d H:i:s', $now);
                $endDate = date('Y-m-d H:i:s', $ten_minutes);
                $current_time = date('Y-m-d H:i:s');
                $response->expiring_at = $endDate;
                // $_SESSION['zoho_crm'] = $response;
        file_put_contents('access_token.json',json_encode($response));

                // $this->session->save();
                 return $response;
            }else{
                throw new Exception("Error Processing Request", 1);
            }
        }
    }


    public function getAllUsers(){
        $url = 'users?type=AllUsers';
        return $res = $this->sendGetRequest($url);
    }

    public function getZohoCrmRecordsList($module){
        $url = $module;
        return $res = $this->sendGetRequest($url);
    }

    public function getZohoCrmSpecificRecords($module,$recordId){
        $url = $module.'/'.$recordId;
        return $res = $this->sendGetRequest($url);
    }

    public function getZohoCrmActiveUsers(){
        $url = 'users?type=ActiveUsers';
        return $res = $this->sendGetRequest($url);
    }


     public function updateZohoCrmRecord($module,$record_id,$postData){
        $url = $module.'/'.$record_id;
        return $res = $this->sendPutRequest($url,$postData);
    }
    public function createZohoCrmRecord($module,$postData){
        $url = $module;
        return $res = $this->sendPostRequest($url,$postData);
    }

    public function searchZohoRecord($module,$lead_filter){
        if($module == 'Leads'){
            $url = $module.'/search?criteria=(Email:equals:'.$lead_filter.')';
           // echo 'Accoun_Billing' .$url;
        }
        
        return $res = $this->sendGetRequest($url);
    }   

    public function sendPostRequest($url,$postData){
        $url = 'https://www.zohoapis.eu/crm/v2/'.$url;
        $credentials = $this->getAccessToken();
        if(isset($credentials->access_token)){   
            $postData = json_encode($postData);
            $headers = array('Authorization: Zoho-oauthtoken '.$credentials->access_token, 'Content-type: application/json'); // return datatype
            $ch = curl_init(); // Create a curl handle
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Set curl handle header 
            curl_setopt($ch, CURLOPT_URL, $url); // Third party API URL
            curl_setopt($ch, CURLOPT_POST, FALSE);  // To set POST method true
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); // To send data to the API URL
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // To set SSL Verifier false
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // To set return response from the API
            return $response = curl_exec($ch); // To execute the handle and get the response 
            
            $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Get Http Status code
        }
    }
        public function sendPutRequest($url,$postData){
        $url = 'https://www.zohoapis.eu/crm/v2/'.$url;
        $credentials = $this->getAccessToken();
        if(isset($credentials->access_token)){   
            $postData = json_encode($postData);
            $headers = array('Authorization: Zoho-oauthtoken '.$credentials->access_token, 'Content-type: application/json'); // return datatype
            $ch = curl_init(); // Create a curl handle
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Set curl handle header 
            curl_setopt($ch, CURLOPT_URL, $url); // Third party API URL
            curl_setopt($ch, CURLOPT_POST, FALSE);  // To set POST method true
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData); // To send data to the API URL
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // To set SSL Verifier false
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // To set return response from the API
            return $response = curl_exec($ch); // To execute the handle and get the response 
            
            $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Get Http Status code
        }
    }



    public function sendGetRequest($url){
     $url = 'https://www.zohoapis.eu/crm/v2/'.$url;
     $credentials = $this->getAccessToken();
     if(isset($credentials->access_token)){ 
            $headers = array('Authorization: Zoho-oauthtoken '.$credentials->access_token, 'Content-type: application/json'); // return datatype
            $ch = curl_init(); // Create a curl handle
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); // Set curl handle header 
            curl_setopt($ch, CURLOPT_URL, $url); // Third party API URL
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // To set SSL Verifier false
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); // To set return response from the API
            return $response = curl_exec($ch); // To execute the handle and get the response 

            $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE); // Get Http Status code
        }
    }

}
