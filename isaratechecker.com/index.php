<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="icon" href="assets/images/ISAchecker-01.jpg" type="image/jpg" sizes="16x16">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ISA RATE CHECKER</title>
	<link media="all" rel="stylesheet" href="assets/css/main.css">
	<!--bootstrap v4.0.0-->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">

	<!--reset css-->
	<link rel="stylesheet" type="text/css" href="assets/css/normalize.css">

	<!--fontawesome cdn-->
	<link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
	<!--main style-->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">

	<!--modernizr-->
	<script src="assets/js/vendor/modernizr.js"></script>

	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>
	<div id="wrapper">
		<header class="header">
			<div class="container">
				<strong class="logo"><a href="index.php"><img src="assets/images/logo-isa.svg" alt="ISARATECHECKER.COM FINd THE BEST ISA FOR YOURSELF TODAY"></a></strong>
				<!-- <ul class="list-info">
					<li><a href="mailto:info@isaratechecker.com"><span class="img"><img src="assets/images/icon-envelope.png" alt="image description"></span>info@isaratechecker.com</a></li>
					<li><time datetime="2021-01-10">Monday - Friday: 9am - 8pm</time></li>
				</ul> -->
			</div>
		</header>
		<main class="main">
			<section class="visual-block" style="background-image: url(assets/images/img-banner.jpg);">
				<div class="container">
					<div class="text-holder">
						<h1>Find <strong>ISA <span class="text-orange">Rates</span></strong> Offering The <strong><span class="text-orange">Best</span> Returns</strong></h1>
						<ul class="list-bullets">
							<li><span class="img"><img src="assets/images/icon-tick-orange.png" alt="tick"></span>Check Rates From The Best Performing ISAs</li>
							<li><span class="img"><img src="assets/images/icon-tick-orange.png" alt="tick"></span>Quick and Simple 30 Second Enquiry Form</li>
							<li><span class="img"><img src="assets/images/icon-tick-orange.png" alt="tick"></span>See If Your Current ISA Is Still Offering You The Best Rate</li>
							<li><span class="img"><img src="assets/images/icon-tick-orange.png" alt="tick"></span>Transfer An Existing Or Start New ISA</li>
							<li><span class="img"><img src="assets/images/icon-tick-orange.png" alt="tick"></span>Get Better Returns On Your Tax-Free ISA</li>
							<li><span class="img"><img src="assets/images/icon-tick-orange.png" alt="tick"></span>Flexible and Fixed Terms</li>
						</ul>
						<a href="compare-now.php" class="btn btn-orange">COMPARE NOW</a>
					</div>
				</div>
			</section>
			<div class="text-block bg-white">
				<div class="container">
					<p>Your capital is at risk. The products compared may not be approved under section 21 of FSMA. Reliance on this promotion for the purpose of engaging in any investment activity may expose and individual to a significant risk of losing all of the property or other assets invested. Please consult your independent Financial Advisor before deciding to invest. Investors should be aware that there can be risk when investing in ISA's and Bonds. It is also important to note these investments may NOT be covered by the Financial Services Compensation Scheme(FSCS) and lenders will not have access to the Financial Ombudsman Service(FOS).</p>
					<p>We compare investments, but do not provide advice. All investors should consult with a registered financial advisor before making any investments decision. Despite security measures, which accompany some of the featured options, no investments are guaranteed and therefore you could receive less than you put in. The information on this website should not be taken as specific advice as it appears for information purposes only. Neither does this information constitute solicitation, offer or recommendation to invest in or dispose of, any investment.</p>
				</div>
			</div>
			<div class="text-block">
				<div class="container">
					<p>The information provided on this website is for information purposes only. The website and its content are not, and should not be deemed to be an offer of, or invitation to engage in any investment activity. The content of this promotion is not authorised under the Financial Services and Markets Act 2000 (FSMA). Reliance on the promotion for the purpose of engaging in any investment activity may expose an individual to a significant risk of losing all of the investment. UK residents wishing to participate in this promotion must fall into the category of sophisticated investor or high net worth individual as outlined by the Financial Conduct Authority</p>
					<p>You may lose some or all of your investment. Returns are NOT guaranteed. This investment may carry a high level of risk, and may not be suitable for all investors. Before proceeding you should carefully consider your investment objectives, level of experience, and risk appetite. You should be aware of all the risks associated with this investment and seek advice from an independent financial advisor if you have any doubts. The content of this promotion has not been approved by an authorised person (Financial Services and Markets Act 2000)</p>
				</div>
			</div>
		</main>
		<footer class="footer" style="background-color: #555;">
			<div class="container">
				<ul class="footer-links">
					<li><a href="terms_conditions.php">Terms And Conditions</a></li>
					<li><a href="privacy_policy.php">Privacy Policy</a></li>
				</ul><br>
				<p class="footer-text py-3 border-bottom">
              We do not provide advice to investors and the information on this website should not be construed as such. The information which appears on our website is for information purposes only and does not constitute specific advice. Neither does it constitute a solicitation, offer or recommendation to invest in or dispose of, any investment. If you are in any doubt as to the suitability of an investment, you should seek independent financial advice from a suitable financial advisor.</p>
               <center><p class="text-center py-2 footer-bottom" style="color: #fff;">© 2022 ISARATECHECKER.COM</p></center>
			</div>
		</footer>
	</div>
		<div class="modal fade" id="myModal">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
            <div class="modal-body">
            	<div class="se-pre-con"></div>

            	<!--disclaimer-->
            	<div class="disclaimer-wrapper-area">
            		<img src="assets/images/img-top.png" class="top-img"  alt=""/>
            		<div class="disc-inner">
            			<div class="area-wra">
            				<img src="assets/images/logo-isa.svg" alt="" style="width:50%;" />
            				<div class="single">
            					<h3>Legal And Regulatory Information</h3>
            					<p>The content of this promotion has not been approved by an authorised person within the meaning of the Financial Services and Markets Act 2000. Reliance on this promotion for the purpose of engaging in any investment activity may expose an individual to a significant risk of losing all of the property or other assets invested.</p>
            					<p>The information in this website sets out legal information and regulatory restrictions about investments, which you should read carefully. By continuing to access any page of this website, you agree to be bound by these restrictions. If you do not agree to be bound by them, you must leave the website. We remain not responsible for any misrepresentations you may make in order to gain access to this website.In order to access this site you must confirm that you fit into one of the 3 categories of investor as set out below.</p>
            					<h4>Certified Sophisticated Investor</h4>
            					<p>You have invested in more than one unlisted company, been a director of a company with an annual turnover of at least £1 million, or worked in private equity in the last two years, or you have been a member of a business angels network for at least the last six months.</p>
            					<h4>Self Certified Sophisticated Investor</h4>
            					<p>A self-certified sophisticated investor is a member of a network or syndicate of business angels and have been so for at least the last six months prior; has made more than one investment in an unlisted company, in the last two years prior; is working, or have worked in the two years prior, in a professional capacity in the private equity sector, or in the provision of finance for small and medium enterprises; is currently, or have been in the two years prior, a director of a company with an annual turnover o f at least £1 million.</p>
            					<h4>High Net Worth Investor</h4>
            					<p>You earn more than £100,000 per year or hold net assets of at least £250,000.</p> 
            					<p>you do not fall into one of these categories, then please exit the site. You will upon entering the site be requested to complete a full registration prior to any investment information being accessed. By proceeding to access any page of this website, you agree, so far as is permitted by law and regulation, to the exclusion of any liability whatsoever for any errors and/or omissions by it and/or any relevant third parties, in respect of its content. The site does not exclude any liability for, or remedy in respect of, fraudulent misrepresentation.The information set out in this website may be amended without notice to you. If you continue to access this website following any such changes, you will be deemed to have accepted them. This website does not constitute an offer or invitation to invest in any securities.</p>
            					<h4>Risk Factors</h4>
            					<p>Investors should be aware there are risks to investing in securities of any company, especially if they are private companies not listed on a Recognised Investment Exchange, and there may be little or no opportunity to sell the securities easily should you need to do so.The value of your Investments can go down as well as up and therefore you may not recover some, or in extreme cases, any of the value of your initial investment. If you are still unsure, seek the advice of a regulated financial adviser. Investments in these securities are NOT covered by the Financial Services Compensation Scheme (FSCS) nor would you have access to the Financial Ombudsman Service (FOS).</p>
            					<h4>Contents Of This Website</h4>
            					<p>This website is published solely for the purpose of receiving information.</p>
            					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
            						<a href="#" class="proceed-btn">proceed</a>
            					</button>
            				</div>
            			</div>
            			<img src="assets/images/img-bot.png" alt="" class="bot-img"/>
            		</div><!--/.disc-inner-->

            	</div>
            	<!--disclaimer-->

            </div>
        </div>
    </div>
</div>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="assets/js/jquery-1.12.4.min.js"></script>
<!--bootstrap v4 js-->
<script src="assets/js/vendor/bootstrap.min.js"></script>
<!--popper js-->
<script src="assets/js/vendor/popper.min.js"></script>
<!--easing js-->
<script src="assets/js/vendor/easing.js"></script>
<!--main script-->
<script src="assets/js/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.0/jquery.cookie.min.js">
</script>
<script type="text/javascript">
	$(document).ready(function() {
		if ($.cookie('pop') == null) {
				$('#myModal').modal('show');
			$.cookie('pop', '7');
		}
        // $('#myModal').modal('show');

    });
</script>
</body>
</html>
