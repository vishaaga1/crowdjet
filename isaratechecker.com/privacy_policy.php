<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<link rel="icon" href="assets/images/ISAchecker-01.jpg" type="image/jpg" 
	sizes="16x16">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ISA RATE CHECKER</title>
	<link media="all" rel="stylesheet" href="assets/css/main.css">
	<style type="text/css">
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 80%;
			margin-left: 50px;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}

		tr:nth-child(even) {
			background-color: #dddddd;
		}
		/* Extra small devices (phones, 600px and down) */
		@media only screen and (max-width: 600px) and (min-width: 300px) {
			table{
				width: 20% !important;
				height: 10% !important;
				margin-left: 0px!important;
				margin-right: 5px!important;
			}
		}

		/* Small devices (portrait tablets and large phones, 600px and up) */
		@media only screen and (min-width: 600px) {
			table{width: 30% !important;}
		}


		/* Medium devices (landscape tablets, 768px and up) */
		@media only screen and (min-width: 768px) {
			table{width: 30% !important;}
		}

		/* Large devices (laptops/desktops, 992px and up) */
		@media only screen and (min-width: 992px) {
			table{width: 30% !important;}
		}

		/* Extra large devices (large laptops and desktops, 1200px and up) */
		@media only screen and (min-width: 1200px) {
			table{width: 80% !important;}
		}
	</style>
</head>
<body>
	<div id="wrapper">
		<header class="header">
			<div class="container">
				<strong class="logo"><a href="index.php"><img src="assets/images/logo-isa.svg" alt="ISARATECHECKER.COM FINd THE BEST ISA FOR YOURSELF TODAY"></a></strong>
			</div>
		</header>
		<main class="main">
			<section >
				<div class="container">
					<center><h1 class="text-center">Privacy Policy</h1></center>
					<p>This privacy policy applies between you, the User of this Website and isaratechecker.com, the owner and provider of this Website. isaratechecker.com takes the privacy of your information very seriously. This privacy policy applies to our use of any and all Data collected by us or provided by you in relation to your use of the Website.<strong>Please read this privacy policy carefully.</strong></p>
					<ol type="1">
						<h2>Definitions and interpretation</h2>
						<li><h5>In this privacy policy, the following definitions are used:
						</h5>
						<table class="table">
							<tbody>
								<tr scope="row">
									<td>Data</td>
									<td>collectively all information that you submit to isaratechecker.com via the Website. This definition incorporates, where applicable, the definitions provided in the Data Protection Laws;</td>
								</tr>
								<tr scope="row">
									<td>Data Protection Laws</td>
									<td>any applicable law relating to the processing of personal Data, including but not limited to the Directive 96/46/EC (Data Protection Directive) or the GDPR, and any national implementing laws, regulations and secondary legislation, for as long as the GDPR is effective in the UK;</td>
								</tr>
								<tr scope="row">
									<td>GDPR</td>
									<td>the General Data Protection Regulation (EU) 2016/679;</td>
								</tr>
								<tr scope="row">
									<td>isaratechecker.com, we or you</td>
									<td>isaratechecker.com</td>
								</tr>
								<tr scope="row">
									<td>User or you</td>
									<td>any third party that accesses the Website and is not either (i) employed by isaratechecker.com and acting in the course of their employment or (ii) engaged as a consultant or otherwise providing services to isaratechecker.com and accessing the Website in connection with the provision of such services;</td>
								</tr>
								<tr scope="row">
									<td>Website</td>
									<td>The website that you are currently using, www.isaratechecker.com, and any sub-domains of this site unless expressly excluded by their own terms and conditions.</td>
								</tr>
							</tbody>
						</table>
					</li>
					<li><h5>In this privacy policy, unless the context requires a different interpretation:</h5>
						<ol type="a">
							<li>the singular includes the plural and vice versa;</li>
							<li>references to sub-clauses, clauses, schedules or appendices are to sub-clauses, clauses, schedules or appendices of this privacy policy;</li>
							<li>a reference to a person includes firms, companies, government entities, trusts and partnerships;</li>
							<li>"including" is understood to mean "including without limitation";</li>
							<li>reference to any statutory provision includes any modification or amendment of it;</li>
							<li>the singular includes the plural and vice versa;</li>
						</ol>
					</li>

					<h2>Scope of this privacy policy</h2>
					<li>This privacy policy applies only to the actions of isaratechecker.com and Users with respect to this Website. It does not extend to any websites that can be accessed from this Website including, but not limited to, any links we may provide to social media websites.
					</li>
					<li>For purposes of the applicable Data Protection Laws, isaratechecker.com is the "data controller". This means that isaratechecker.com determines the purposes for which, and the manner in which, your Data is processed.
					</li>

					<h2>Data collected</h2>
					<li>We may collect the following Data, which includes personal Data, from you:
						<ol type="a">
							<li>contact Information such as email addresses and telephone numbers;</li>
							<li>financial information such as credit / debit card numbers;</li>
							<li>IP address (automatically collected);
						in each case, in accordance with this privacy policy.</li>
						</ol> 
					</li>

					<h2>How we collect Data</h2>
					<li>We collect Data in the following ways:
						<ol type="a">
							<li>data is given to us by you; and</li>
							<li> data is collected automatically</li>
						</ol>
					 
					</li>

					<h2>Data that is given to us by you</h2>
					<li>isaratechecker.com will collect your Data in a number of ways, for example:
						<ol type="a">
						<li>when you contact us through the Website, by telephone, post, e-mail or through any other means;</li>
						<li> when you complete surveys that we use for research purposes (although you are not obliged to respond to them);in each case, in accordance with this privacy policy.</li>
						</ol>
					</li>

					<h2>Data that is collected automatically</h2>
					<li>To the extent that you access the Website, we will collect your Data automatically, for example:
						<ol type="a">
							<li>we automatically collect some information about your visit to the Website. This information helps us to make improvements to Website content and navigation, and includes your IP address, the date, times and frequency with which you access the Website and the way you use and interact with its content.</li>
						</ol>
					</li>

					<h2>Our use of Data</h2>
					<li>Any or all of the above Data may be required by us from time to time in order to provide you with the best possible service and experience when using our Website. Specifically, Data may be used by us for the following reasons:
						<ol type="a">
							<li>improvement of our products / services;</li>
							<li>transmission by email of marketing materials that may be of interest to you;</li>
						</ol>
					in each case, in accordance with this privacy policy.</li>
					<li> We may use your Data for the above purposes if we deem it necessary to do so for our legitimate interests. If you are not satisfied with this, you have the right to object in certain circumstances (see the section headed "Your rights" below).</li>
					<li> For the delivery of direct marketing to you via e-mail, we'll need your consent, whether via an opt-in or soft-opt-in:
						<ol type="a">
							<li> soft opt-in consent is a specific type of consent which applies when you have previously engaged with us (for example, you contact us to ask us for more details about a particular product/service, and we are marketing similar products/services). Under "soft opt-in" consent, we will take your consent as given unless you opt-out.</li>
							<li>for other types of e-marketing, we are required to obtain your explicit consent; that is, you need to take positive and affirmative action when consenting by, for example, checking a tick box that we'll provide.</li>
							<li>if you are not satisfied about our approach to marketing, you have the right to withdraw consent at any time. To find out how to withdraw your consent, see the section headed "Your rights" below.</li>
						</ol>
					</li>

					<h2>Who we share Data with</h2>
					<li>We may share your Data with the following groups of people for the following reasons:
						<ol type="a">
							<li>any of our group companies or affiliates - to ensure the proper administration of your website and business;</li>
							<li>our employees, agents and/or professional advisors - to obtain advice from experts;</li>
							<li>third party service providers who provide services to us which require the processing of personal data - to help third party service providers in receipt of any shared data to perform functions on our behalf to help ensure the website runs smoothly;
					in each case, in accordance with this privacy policy.</li>
							<li>ISA product providers and eligible investment providers whom we have verified and consider credible so that they may contact you in order to promote their products.
							</li>
						</ol>
					</li>

					<h2>Keeping Data secure</h2>
					<li>We will use technical and organisational measures to safeguard your Data, for example:
						<ol type="a">
							<li>access to your account is controlled by a password and a user name that is unique to you.</li>
							<li>we store your Data on secure servers.</li>
							<li>payment details are encrypted using SSL technology (typically you will see a lock icon or green address bar (or both) in your browser when we use this technology.</li>
						</ol>						
					</li>
					<li>Technical and organisational measures include measures to deal with any suspected data breach. If you suspect any misuse or loss or unauthorised access to your Data, please let us know immediately by contacting us via this e-mail address: info@isaratechecker.com.</li>
					<li>If you want detailed information from Get Safe Online on how to protect your information and your computers and devices against fraud, identity theft, viruses and many other online problems, please visit www.getsafeonline.org. Get Safe Online is supported by HM Government and leading businesses.</li>
					<h2>Data retention</h2>
					<li>Unless a longer retention period is required or permitted by law, we will only hold your Data on our systems for the period necessary to fulfil the purposes outlined in this privacy policy or until you request that the Data be deleted.</li>
					<li>Even if we delete your Data, it may persist on backup or archival media for legal, tax or regulatory purposes.</li>
					<h2>Your rights</h2>
					<li>You have the following rights in relation to your Data:
						<ol type="a">
							<li>Right to access - the right to request (i) copies of the information we hold about you at any time, or (ii) that we modify, update or delete such information. If we provide you with access to the information we hold about you, we will not charge you for this, unless your request is "manifestly unfounded or excessive." Where we are legally permitted to do so, we may refuse your request. If we refuse your request, we will tell you the reasons why.</li>
							<li>Right to correct - the right to have your Data rectified if it is inaccurate or incomplete.</li>
							<li>Right to erase - the right to request that we delete or remove your Data from our systems.</li>
							<li>Right to restrict our use of your Data - the right to "block" us from using your Data or limit the way in which we can use it.</li>
							<li>Right to data portability - the right to request that we move, copy or transfer your Data.</li>
							<li>Right to object - the right to object to our use of your Data including where we use it for our legitimate interests.</li>
						</ol>
					</li>
					<li>To make enquiries, exercise any of your rights set out above, or withdraw your consent to the processing of your Data (where consent is our legal basis for processing your Data), please contact us via this e-mail address: info@isaratechecker.com.</li>
					<li>If you are not satisfied with the way a complaint you make in relation to your Data is handled by us, you may be able to refer your complaint to the relevant data protection authority. For the UK, this is the Information Commissioner's Office (ICO). The ICO's contact details can be found on their website at https://ico.org.uk/.</li>
					<li>It is important that the Data we hold about you is accurate and current. Please keep us informed if your Data changes during the period for which we hold it.</li>
					<h2>Links to other websites</h2>
					<li>This Website may, from time to time, provide links to other websites. We have no control over such websites and are not responsible for the content of these websites. This privacy policy does not extend to your use of such websites. You are advised to read the privacy policy or statement of other websites prior to using them.</li>

					<h2>Changes of business ownership and control</h2>
					<li>isaratechecker.com may, from time to time, expand or reduce our business and this may involve the sale and/or the transfer of control of all or part of isaratechecker.com. Data provided by Users will, where it is relevant to any part of our business so transferred, be transferred along with that part and the new owner or newly controlling party will, under the terms of this privacy policy, be permitted to use the Data for the purposes for which it was originally supplied to us.</li>
					<li>We may also disclose Data to a prospective purchaser of our business or any part of it.</li>
					<li>In the above instances, we will take steps with the aim of ensuring your privacy is protected.</li>

					<h2>General</h2>
					<li>You may not transfer any of your rights under this privacy policy to any other person. We may transfer our rights under this privacy policy where we reasonably believe your rights will not be affected.</li>
					<li>If any court or competent authority finds that any provision of this privacy policy (or part of any provision) is invalid, illegal or unenforceable, that provision or part-provision will, to the extent required, be deemed to be deleted, and the validity and enforceability of the other provisions of this privacy policy will not be affected.</li>
					<li>Unless otherwise agreed, no delay, act or omission by a party in exercising any right or remedy will be deemed a waiver of that, or any other, right or remedy.</li>
					<li>This Agreement will be governed by and interpreted according to the law of England and Wales. All disputes arising under the Agreement will be subject to the exclusive jurisdiction of the English and Welsh courts.</li>

					<h2>Changes to this privacy policy</h2>
					<li>isaratechecker.com reserves the right to change this privacy policy as we may deem necessary from time to time or as may be required by law. Any changes will be immediately posted on the Website and you are deemed to have accepted the terms of the privacy policy on your first use of the Website following the alterations.<br>You may contact isaratechecker.com by email at info@isaratechecker.com.
				</li>
				</ol>

			</div>
		</section>
	</main>
	<footer class="footer" style="background-color: #555;">
		<div class="container">
			<ul class="footer-links">
				<li><a href="terms_conditions.php">Terms And Conditions</a></li>
				<li><a href="privacy_policy.php">Privacy Policy</a></li>
			</ul><br>
			<p class="footer-text py-3 border-bottom">
			We do not provide advice to investors and the information on this website should not be construed as such. The information which appears on our website is for information purposes only and does not constitute specific advice. Neither does it constitute a solicitation, offer or recommendation to invest in or dispose of, any investment. If you are in any doubt as to the suitability of an investment, you should seek independent financial advice from a suitable financial advisor.</p>
			<center><p class="text-center py-2 footer-bottom" style="color: #fff;">© 2022 ISARATECHECKER.COM</p></center>
		</div>
	</footer>
</div>
</body>
</html>
