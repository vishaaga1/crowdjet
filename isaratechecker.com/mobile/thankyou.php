
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ISA RATE CHECKER</title>
	<link rel="icon" href="assets/images/ISAchecker-01.jpg" type="image/jpg" sizes="16x16">
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/css/stylesheet.css">
	<link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/f435ef88a9.js" crossorigin="anonymous"></script>
	<!-- Conversion Pixel - SALE_CONV - DO NOT MODIFY -->
	<script src="https://secure.adnxs.com/px?id=999577&seg=13439766&t=1" type="text/javascript"></script>
	<!-- End of Conversion Pixel -->
</head>
<body>
	<header class="bg-white border-bottom">
		<div class="container">
			<div class="row m-0 w-100 align-items-center py-3">
				<div class="col-md-5 text-md-left text-center">
					<a href="https://isaratechecker.com/mobile/"><img class="img-logo" src="assets/images/ISAchecker-01.png"></a>
				</div>
			</div>
		</div>
	</header>
	<section class="form-section bg-white ">
		<div class="container" style="margin-top: 150px!important;">
			<h2 style="text-align: center;">Thank You</h2>
			<p style="text-align: center;">IsaRateChecker for Mobile Is Working Hard On Getting You The Best Quotes And We Will Be In Touch Shortly.</p>
		</div>
		<div class="contact-space">
			<p>&nbsp;</p>
		</div>
	</section>

	<footer class="position-relative">
		<div class="bg-clip-path">
			<div class="container">
				<div class="row w-100 m-0 z-indx position-relative">
					<div class="col">
						<p class="footer-text py-3 border-bottom">
							We do not provide advice to investors and the information on this website should not be construed as such. The information which appears on our website is for information purposes only and does not constitute specific advice. Neither does it constitute a solicitation, offer or recommendation to invest in or dispose of, any investment. If you are in any doubt as to the suitability of an investment, you should seek independent financial advice from a suitable financial advisor
							<br>
							<a href="assets/terms-and-conditions.html" class="text-white text-decoration-none">Terms And Conditions </a>|
							<a href="assets/privacy-policy.html" class="text-white text-decoration-none" > Privacy Policy</a>
						</p>

						<p class="text-center py-2 footer-bottom">© 2022 ISARATECHECKER.COM</p>
					</div>
				</div>
			</div>
		</div>
	</footer>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/parsley.js"></script>
	<script src="assets/js/form.js"></script>
</body>
</html>