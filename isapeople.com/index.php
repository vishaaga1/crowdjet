<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="icon" href="assets/images/theISApeople-01.jpg" type="image/jpg" sizes="16x16">
	<title>ISA PEOPLE</title>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link media="all" rel="stylesheet" href="assets/css/main.css">
</head>
<body>
	<div id="wrapper">
		<header class="header  has-bg">
			<div class="container">
				<strong class="logo"><a href="index.php"><img src="assets/images/logo.png" alt="ISAPEOPLE.COM"></a></strong>
				<!-- <ul class="list-info">
					<li><a href="mailto:info@isapeople.com"><span class="img"><img src="assets/images/icon-envelope.png" alt="image description"></span>info@isapeople.com</a></li>
					<li><time datetime="2020-07-30">Monday - Friday: 9am - 8pm</time></li>
				</ul> -->
			</div>
		</header>
		<main class="main">
			<section class="visual-block has-bg">
				<div class="container">
					<div class="visual-frame">
						<div class="text-holder">
							<h1>Find <strong>ISA <span class="text-orange">Rates</span></strong> Offering The <strong><span class="text-orange">Best</span> Returns</strong></h1>
							<ul class="list-bullets">
								<li><span class="img"><img src="assets/images/icon-tick.png" alt="tick"></span>Check Rates From The Best Performing ISAs</li>
								<li><span class="img"><img src="assets/images/icon-tick.png" alt="tick"></span>Quick and Simple 30 Second Enquiry Form</li>
								<li><span class="img"><img src="assets/images/icon-tick.png" alt="tick"></span>See If Your Current ISA Is Still Offering You The Best Rate</li>
								<li><span class="img"><img src="assets/images/icon-tick.png" alt="tick"></span>Transfer An Existing Or Start New ISA</li>
								<li><span class="img"><img src="assets/images/icon-tick.png" alt="tick"></span>Get Better Returns On Your Tax-Free ISA</li>
								<li><span class="img"><img src="assets/images/icon-tick.png" alt="tick"></span>Flexible and Fixed Terms</li>
							</ul>
							<a href="compare-now.php" class="btn btn-orange radius">COMPARE NOW</a>
						</div>
						<div class="image-holder">
								<img src="assets/images/img1.png" alt="image description">
						</div>
					</div>
				</div>
			</section>
			<div class="content-block">
				<div class="container">
					<div class="content-wrap">
						<div class="image-holder">
							<img src="assets/images/img2.png" alt="image description">
						</div>
						<div class="text-holder">
							<p>You capital is at risk. The products compared may not be approved under section 21 of FSMA. Reliance on this promotion for the purpose of engaging is any investment activity may expose and individual to a significant rist of losing all of the property or other assets invested. Please consult your independent Financila Advisor before deciding to invest. Investor should be aware that there can be rist to investing is ISA’s and Bonds. It is also important to note these investments may NOT be covered by the Financial Services Compensation Scheme(FSCS) and lenders will not have access to the Financial Ombudsman Service(FOS).</p>
							<p>We compare investment, but do not provide advice. All investors should consult register financial guidence before making any investments decision. Despite security measure, which accompany some of the featured option, no investments are guaranteed and therefore you could recieve less than you put in. The information on this website should not be taken as specific advice as it appears for information purposes only. Neither does this information constitute solucitation, offer or recommendation to invest in or dispose of, any investment.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="text-block">
				<div class="container">
					<p>The information provided on this website is for information purposes only. The website and its content are not, and should not be deemed to be an offer of, or invitation to engage in any investment activity. The content of this promotion is not authorised under the Financial Services and Markets Act 2000 (FSMA). Reliance on the promotion for the purpose of engaging in any investment activity may expose an individual to a significant risk of losing all of the investment. UK residents wishing to participate in this promotion must fall into the category of sophisticated investor or high net worth individual as outlined by the Financial Conduct Authority</p>
					<p>You may lose some or all of your investment. Returns are NOT guaranteed. This investment may carry a high level of risk, and may not be suitable for all investors. Before proceeding you should carefully consider your investment objectives, level of experience, and risk appetite. You should be aware of all the risks associated with this investment and seek advice from an independent financial advisor if you have any doubts. The content of this promotion has not been approved by an authorised person (Financial Services and Markets Act 2000)</p>
				</div>
			</div>
		</main>
		<footer class="footer" style="background-color: #555;">
			<div class="container">
				<ul class="footer-links">
					<li><a href="terms_conditions.php">Terms And Conditions</a></li>
					<li><a href="privacy_policy.php">Privacy Policy</a></li>
				</ul><br>
				<p class="footer-text py-3 border-bottom">
              We do not provide advice to investors and the information on this website should not be construed as such. The information which appears on our website is for information purposes only and does not constitute specific advice. Neither does it constitute a solicitation, offer or recommendation to invest in or dispose of, any investment. If you are in any doubt as to the suitability of an investment, you should seek independent financial advice from a suitable financial advisor.</p>
               <center><p class="text-center py-2 footer-bottom" style="color: #fff;">© 2022 ISAPeople.com</p></center>
			</div>
		</footer>
	</div>
</body>
</html>
