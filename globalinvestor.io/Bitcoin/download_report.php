<?php 
session_start();
?>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Global Investor | DOWNLOAD YOUR FREE HOW TO PROFIT FROM BITCOIN REPORT</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="css/stylesheet.css">
 <link href="favicon.png" rel="apple-touch-icon" />
 <link href="images/favicon.png" rel="icon" type="image/png" />
</head>
<body>
  <header>
   <div class="container">
    <div class="row">
     <div class="col-sm-6 my-auto">
      <div class="logo">
       <a href="index.php"><img src="images/logo.png" class="img-fluid" /></a>
     </div>
   </div>
   <div class="col-sm-6 my-auto">
    <div class="top-download">
     <a href="#">
       <img src="images/download-icon.png" class="img-fluid" />
       Download Your Report
     </a>
   </div>
 </div>
</div>
</div>
</header>
<section class="middle-content">
 <div class="container">
  <div class="row">
   <div class="col-lg-7">
    <div class="row">
     <div class="col-lg-10">
      <div class="left-side">
       <h1>We Are Providing A Complimentary Report<br />How To Profit Bitcoin</h1>        
        <h2>Download Your Free<br />Report On <span>How To Profit</span><br />From Bitcoin</h2>
     </div>
   </div>
 </div>
</div>
<div class="col-lg-5">
  <div class="right-side">
  <h2>DOWNLOAD YOUR FREE HOW TO<br /> PROFIT FROM BITCOIN REPORT</h2>
  <h3>How To Profit From Bitcoin Report Register Here &<br />Free Gain Access The Latest Information On Bitcoin</h3>
   <form action="" method="post">
      <div class="row send_sms">
        <div class="error"></div>
       <div class="col-md-12 alert alert-success" style="margin-top: 20px;">
         <img src="images/checked_icon.png" width="40px" height="30px" style="margin-left: 150px;"><br>
         <p style="font-size: 20px;font-family: arial;">The report you requested is now available.</p>
         <p>Thank you for registering your interest in our free investment report.
          Please click the button below to start downloading your free report.</p>
      </div>
  </div>
  <div class="col-md-12">
   <button type="submit" id="submit" name="submit">
     <a href="Global_Investor_Bitcoin.pdf" download style="color: #fff !important;"><img src="images/download-icon.png" class="img-fluid" />
     Open Free Report </a>
  </button>
  <p>
    By submitting your details, you agree to be contacted by one of our partners. You also acknowledge that you have read and understood our 
    <a href="terms_conditions.php">Terms & Conditions</a>.
  </p>
</div>
</form>
</div>
</div>
</div>
</div>
</section>
<!-- jQuery library --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/function.js"></script>
<script type="text/javascript">

 function verifyOTP() {
  $(".error").html("").hide();
  $(".success").html("").hide();
  var code = "";
   $("input[class *= 'code']").each(function(){
        code = code + $(this).val()
    });
  var otp = parseInt(code);
  var input = {
    "otp" : otp,
    "action" : "verify_otp",
  };
  if (otp.toString().length == 6 && otp != null) {
    console.log(otp.toString().length);
    $.ajax({
      url : 'verify_otp.php',
      type : 'POST',
      dataType : "json",
      data : input,
      success : function(response) {
        console.log(response);
        $("." + response.type).html(response.message)
        $("." + response.type).show();
        window.location.href = "/download_report.php";
      },
      error : function() {
        alert("You have entered wrong OTP");
      }
    });
  } else {
    $(".error").html('You have entered wrong OTP.')
    $(".error").show();
  }
}

</script>
</body>
</html>