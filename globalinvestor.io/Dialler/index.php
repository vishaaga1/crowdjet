<?php 
//echo '<pre>';print_r($_REQUEST);
?>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="content-type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	</head>
	<body>
		<div class="zcwf_lblLeft crmWebToEntityForm" id="crmWebToEntityForm" style="background-color: white;color: black;max-width: 600px;margin-left: 30% !important;">
   		    <h2>Contact form</h2>
           	<form class="" name="WebToLeads85431000001140077" action="form_data.php" method="POST" accept-charset="UTF-8">
				<div class="form-group">
					<label for="email">First Name:</label>
					<input required type="text" class="form-control" id="First_Name" placeholder="Enter_First_Name" name="First_Name" value="<?php echo $_REQUEST['entry_12345678']; ?>">
				</div>
				<div class="form-group">
					<label for="email">Last Name:</label>
					<input required type="text" class="form-control" id="Last_Name" placeholder="Enter_Last_Name" name="Last_Name" value="<?php echo $_REQUEST['entry_23456789']; ?>">
				</div>
			    <div class="form-group">
					<label for="email">Email:</label>
					<input required type="email" class="form-control" id="Email" placeholder="Enter_Email" name="Email" value="<?php echo $_REQUEST['entry_34567891']; ?>">
				</div>
				<div class="form-group">
					<label for="email">Phone:</label>
					<input required type="text" class="form-control" id="Phone" placeholder="Enter_Phone" name="Phone" value="<?php echo $_REQUEST['entry_45678912']; ?>">
				</div>
				<div class="form-group">
					<label for="email">Disposition :</label>
					<input type="text" class="form-control" id="Disposition" placeholder="Enter_Disposition" name="Disposition" value="<?php echo $_REQUEST['entry_56789123']; ?>">
				</div>
				<div class="form-group">
					<label for="email">How much do you want to invest :</label>
					<input type="text" class="form-control" id="How_much_do_you_want_to_invest1" placeholder="How_much_do_you_want_to_invest" name="How_much_do_you_want_to_invest1" value="">
				</div>
				<div class="form-group">
					<label for="email">Notes :</label>
					<textarea type="text" class="form-control" id="Notes" placeholder="Notes" name="Notes" value=""></textarea>
				</div>
                <button type="submit" class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" onclick="this.form.reset();">Reset</button>
			</form>
		</div>
	</body>
</html>