<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>EDELSTEIN | DOWNLOAD YOUR FREE DIAMONDS & EMERALDS GUIDE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link href="favicon.png" rel="apple-touch-icon" />
    <!--- teleinput for country code --->
    <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css">
    <link href="images/favicon.png" rel="icon" type="image/png" />
  </head>
  <body>
    <header>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 my-auto">
            <div class="logo">
              <a href="index.php"><img src="images/logo.png" class="img-fluid" /></a>
            </div>
          </div>
          <div class="col-sm-6 my-auto">
            <div class="top-download">
              <a href="#download_guide">
                <img src="images/download-icon.png" class="img-fluid" />Download Your Guide
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
    <section class="middle-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <div class="row">
              <div class="col-lg-10">
                <div class="left-side">
                  <h1>We Are Providing A Complimentary Guide On <b>Diamonds & Emeralds</b></h1>
                  <h2>Download Your <span>Free Guide</span> On Diamonds & Emeralds</h2>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5" id="download_guide">
            <div class="right-side">
              <h2>DOWNLOAD YOUR <br /> FREE GUIDE</h2>
              <h3><b>Register Here & Free Gain Access The Latest Information On Diamonds & Emeralds</b></h3>
              
              <form method="POST" id="edelsteinGold">
                <div class="row contatc-box">
                  <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>">
                  <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>">
                  <div class="col-md-6">
                    <input type="text" name="first_name" placeholder="First Name" id="first_name" value="" required="" />
                    <span id="firstName_error" style="color: #dc3545;font-size: 16px;"></span>
                  </div>
                  <div class="col-md-6">
                    <input type="text" name="last_name" placeholder="Last Name" id="last_name" value="" required="" />
                    <span id="lastName_error" style="color: #dc3545;font-size: 16px;"></span>
                  </div>
                  <div id="emaildiv" class="col-md-12">
                    <input type="text" name="email" placeholder="Email Address" class="email" id="email" value="" required=""/>
                    <span id="Invalid_error" style="color: #dc3545;font-size: 16px;"></span>
                  </div>
                  <div id="phonediv" class="col-md-12">
                    <input type="hidden" name="country_code" id="country_code" value="">
                    <input type="text" name="phone" placeholder="Telephone Number" class="telephone" value="" id="phone" required=""/>
                    <span id="valid-msg" class="hide">✓ Valid</span>
                    <span id="error-msg" style="color: #dc3545;font-size: 16px;"></span>
                  </div>
                  <!-- <div class="col-md-4">
                    <input type="number" name="age" placeholder="Age" class="age" value="" required=""/>
                    <span class="error"></span>
                  </div>
                  <div class="col-md-8">
                    <select name="investor_knowledge" required>
                      <option value="" disabled selected>Investor Knowledge</option>
                      <option value="Extensive Knowledge">Extensive Knowledge</option>
                      <option value="Limited Knowledge">Limited Knowledge</option>
                      <option value="Moderate Knowledge">Moderate Knowledge</option>
                      <option value="Seasoned Professional">Seasoned Professional</option>
                    </select>
                  </div> -->
                  <div class="col-md-12">
                    <!-- <div id="Invalid_error"></div> -->
                    <button type="button" id="btn-submit" name="submit">
                      <img src="images/download-icon.png" class="img-fluid" />Download Your Guide
                    </button>
                  </div>
                  <div class="col-md-12">
                    <p>By submitting your details, you agree to be contacted by one of our partners. You also acknowledge that you have read and understood our
                      <a href="terms_conditions.php">Terms & Conditions</a>.
                    </p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/function.js"></script>
    <script type="text/javascript">
      var validEmail = false;
      var validPhone = false;
      $( document ).ready(function() {
        //Country code script
        var input = document.querySelector("#phone"),
        errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg");

        //Here, the index maps to the error code returned from getValidationError - see readme
        var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

        //Get country code
        var countryData = window.intlTelInputGlobals.getCountryData(),
        country_code = document.querySelector("#country_code");
        //Initialise plugin
        var iti = window.intlTelInput(input, {
          utilsScript: "intlTelInput/js/utils.js",
          initialCountry: 'gb'
        }); 
        //Set it's initial value
        var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
        // console.log(all_countrycode);

        //Listen to the telephone input for changes
        input.addEventListener('countrychange', function(e) {
          var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
          console.log(selected_countrycode);
        });

        var reset = function() {
          input.classList.remove("error");
          errorMsg.innerHTML = "";
          errorMsg.classList.add("hide");
          validMsg.classList.add("hide");
        };

        //On blur: validate
        input.addEventListener('blur', function() {
          reset();
        });

        //On keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);

        //Validate email
        $("#email").change(function(){ 
          var email = $("#email").val();
          level='Address';
          //alert(email);
          var emailvalidation = new data8.emailvalidation();

          emailvalidation.isvalid(
            email,
            level,
            [
            
            ],
            showIsValidResult
          );
        });

        function showIsValidResult(result) {
          console.log(result.Result);
          if(result.Result == 'Valid') {
            validEmail = true;
            $('#Invalid_error').hide();
            $('#btn-submit').prop('disabled', false);
          }
          else
          {
            validEmail = false;
            $('#Invalid_error').show();
            $('#Invalid_error').html('Please Enter Valid Email.');
            $('#btn-submit').prop('disabled', true);
          }
        }

        // //Validate age not less than 18
        // $('.age').on('keyup',function(){
          //   $age = $(this).val();
          //   if($age<18){
            //    $('.error').show();
            //    $('.error').html('<p style="color: #dc3545;padding:0px !important;" >Minimum Age Required Should be 18.</p>');
          //  }
          //  else
          //  {
            //   $('.error').hide();
          // }
        // });

        $('select#country_code').on('change',function(){
          var country_code =  $(this). children("option:selected").text();
          var code = country_code.replace('+','');
          console.log(code);
          $('#code_of_country').val(code);
        })

        $("#phone").bind("change", function(e) {
          var telephoneNumber = $("#phone").val();
          if($("#country_code").val() == 'gb') {
            console.log(e);
            defaultCountry='GB';
            var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
            internationaltelephonevalidation.isvalid(
              telephoneNumber,
              defaultCountry,
              [
                new data8.option('UseMobileValidation', 'false'),
                new data8.option('UseLineValidation', 'false'),
                new data8.option('RequiredCountry', ''),
                new data8.option('AllowedPrefixes', ''),
                new data8.option('BarredPrefixes', ''),
                new data8.option('UseUnavailableStatus', 'true'),
                new data8.option('UseAmbiguousStatus', 'true'),
                new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                new data8.option('ExcludeUnlikelyNumbers', 'false')
              ],
              showIsValidResults
            );
          }
          else{
            console.log(e);
            defaultCountry='GB';
            var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
            internationaltelephonevalidation.isvalid(
              telephoneNumber,
              $("#country_code").val(),
              [
                new data8.option('UseMobileValidation', 'false'),
                new data8.option('UseLineValidation', 'false'),
                new data8.option('RequiredCountry', ''),
                new data8.option('AllowedPrefixes', ''),
                new data8.option('BarredPrefixes', ''),
                new data8.option('UseUnavailableStatus', 'true'),
                new data8.option('UseAmbiguousStatus', 'true'),
                new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                new data8.option('ExcludeUnlikelyNumbers', 'false')
              ],
              showIsValidResults
            );
          }
        });
        
        function showIsValidResults(result) {
          console.log(result.Result.ValidationResult);
          if(result.Result.ValidationResult == 'Valid') {
            validPhone = true;
            $('#error-msg').hide();
            $('#btn-submit').prop('disabled', false);
          }
          else {
            validPhone = false;
            $('#error-msg').show();
            $('#error-msg').html('Please Enter Valid Phone Number');
            $('#btn-submit').prop('disabled', true);
          }
        }
      });
      
      $("#btn-submit").click(function(e) {
      var first_name = $("#first_name").val();
      var last_name = $("#last_name").val();
      var email = $("#email").val();
      var phoneNumber = $("#phone").val();
      
      if(first_name.length <= 1) {
        $('#firstName_error').html('First Name is required!');
        return false;
      }
      else{
        $('#firstName_error').html('');
      }
      if(last_name.length <= 1) {
        $('#lastName_error').html('Last Name is required!');
        return false;
      }
      else{
        $('#lastName_error').html('');
      }
      if(email.length <= 1) {
        $('#Invalid_error').html('Email is required!');
        return false;
      }
      else{
        $('#Invalid_error').html('');
      }
      if(phoneNumber.length <= 1) {
        $('#error-msg').html('Phone Number is required!');
        return false;
      }
      else{
        $('#error-msg').html('');
      }
      
      if(validEmail == true && validPhone == true){
        $.ajax({
            type: "POST",
            url: "lead_form.php",
            data: { 
                first_name: $("#first_name").val(),
                last_name: $("#last_name").val(),
                email: $("#email").val(),
                phone: $("#phone").val(),
                lid: $("#lid").val(),
                aid: $("#aid").val()
            },
            success: function(result) {
                if(result=="success"){
                    location.href = "download_guide.php";
                }
            },
            error: function(result) {
                location.href = "download_guide.php";
            }
        });
      }
    });
    </script>
    <!--- teleinput for country code js --->
    <script src="intlTelInput/js/intlTelInput.min.js"></script>
    <script src="intlTelInput/js/utils.js"></script>
    <script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=SF8D-5CBP-L8KA-36PP&load=InternationalTelephoneValidation,EmailValidation"></script>
  </body>
</html>