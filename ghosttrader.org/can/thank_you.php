<?php
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/style_bg.css" />
        <link rel="apple-touch-icon" href="favicon.png">
        <link rel="icon" type="image/png" href="assets/images/favicon.png">
        
        <title>Ghost Trader | THANK YOU </title>
        <!-- Conversion Pixel - SALE_CONV - DO NOT MODIFY -->
        <script src="https://secure.adnxs.com/px?id=976298&seg=12005196&t=1" type="text/javascript"></script>
        <!-- End of Conversion Pixel -->
    </head>
    <body class="trade1">
        <div class="h-100">
            <div class="my-auto">
                <header>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="logo">
                                    <a href="#">
                                        <img src="assets/images/logo.png" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="main-content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <div class="donwload-btn">
                                                    <p style="height: 5vw;">
                                                        Find Out If You’ve Won By Joining Our Telegram Group<br />
                                                        @ <a href="https://t.me/EnterCryptoCompz">https://t.me/EnterCryptoCompz</a><br />
                                                        Mention Code "CRYPTO COMPZ"
                                                    </p>
                                                    <!-- <button type="submit">CLICK HERE TO DOWNLOAD YOUR GUIDE</button> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-6">
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <div class="social-description">
                                                    <p>
                                                        Follow Us @ <a href="https://twitter.com/GhostTraderbsc">https://twitter.com/GhostTraderbsc</a><br /> 
                                                        Website: <a href="https://ghosttrader.org">https://ghosttrader.org</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row justify-content-center">
                                    <div class="col-md-8">
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <div class="left-box">
                                                    <h3>
                                                        <span class="name">THE WORLD’S FIRST</span><br />  
                                                        Tokenized Crypto Hedge Fund
                                                    </h3>
                                                    <div class="text-box mb-3">
                                                        <p>
                                                            <span class="name">What Is Ghost Trader?</span><br /> 
                                                            Ghost Trader is the world’s first tokenized crypto “<b>hedge fund</b>”. <br /><br />
                                                            It utilizes the unique characteristics of blockchain and smart contract technologies to 
                                                            pool together money from buyers for our professional traders to use, and dishes the profits 
                                                            right back out to buyers on a monthly basis in the form of BUSD dividends
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/popper.js"></script>
        <script src="assets/js/bootstrap.js"></script> 	
    </body>
</html>