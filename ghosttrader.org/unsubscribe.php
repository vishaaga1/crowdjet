<?php
?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/bootstrap.css" />
        <link rel="stylesheet" href="assets/css/style.css" />
        <!--- intlTelInput CSS for Country Code --->
        <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css">
        <link rel="apple-touch-icon" href="favicon.png">
        <link rel="icon" type="image/png" href="assets/images/favicon.png">

        <title>Ghost Trader | Unsubscribe </title>
        <!-- Segment Pixel - ALLPAGES_REM - DO NOT MODIFY -->
        <script src="https://secure.adnxs.com/seg?add=12005195&t=1" type="text/javascript"></script>
        <!-- End of Segment Pixel -->
    </head>
    <body class="trade1">
        <div class="h-100">
            <div class="my-auto">
                <header>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="logo">
                                    <a href="#">
                                        <img src="assets/images/logo.png" class="img-fluid" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
                <section class="main-content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-md-10">
                                <div class="row justify-content-center">
                                    
                                    <div class="col-md-6">
                                        <div class="row justify-content-center">
                                            <div class="col-md-11">
                                                <div class="right-box">
                                                    <h2>
                                                      <?php
                                                      
if(isset($_POST['UNSUBSCRIBE']))
{
   // echo "hi this for is get submited";
    

$email=$_POST['email'];
$phone=$_POST['phone'];

//echo "https://monetise.leadbyte.co.uk/api/submit.php?returnjson=yes&campid=GHOSTTRADER-UNSUB&sid=98878923922&email=$email&phone1=$phone";die;
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://monetise.leadbyte.co.uk/api/submit.php?returnjson=yes&campid=GHOSTTRADER-UNSUB&sid=98878923922&email=$email&phone1=$phone",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
));

$response = curl_exec($curl);

curl_close($curl);
//echo $response;
echo"<br>";
echo "Successfull Submited";

}
?>
                                                    </h2>
                                                    <form action="" method="post" autocomplete="off">
                                                        <div class="row">
                                                           
                                                            
                                                            <div id="emailDiv" class="col-md-12">
                                                                <input type="text" name="email" id="email" placeholder="Email Address" />
                                                            </div>
                                                            <div id="phoneDiv" class="col-md-12">
                                                                <input type="text" name="phone" id="phone" placeholder="Phone Number" />
                                                                <input type="hidden" name="country_code" id="country_code" value="44" placeholder="Code">
                                                                <input type="hidden" name="country_name" id="country_name" value="United Kingdom" placeholder="Country">
                                                                <span id="valid-msg" class="hide">✓ Valid</span>
                                                                <span id="error-msg" class="hide"></span>
                                                            </div>
                                                            <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>" />
                                                            <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>" />
                                                            
                                                            <div class="col-md-12" style="margin-top: 39px;">
                                                                
                                                                <input type="submit" name="UNSUBSCRIBE" value="UNSUBSCRIBE" id="btn-submit"/>
                                                            </div>
                                                            <div id="atcDiv" class="col-md-12">
                                                                
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="text-box hiddenContentDesktop">
                                                    <p>
                                                        <span class="name">What Is Ghost Trader?</span><br /> 
                                                        Ghost Trader is the world’s first tokenized crypto “<b>hedge fund</b>”. <br /><br />
                                                        It utilizes the unique characteristics of blockchain and smart contract technologies to 
                                                        pool together money from buyers for our professional traders to use, and dishes the profits 
                                                        right back out to buyers on a monthly basis in the form of BUSD dividends
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <footer>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    Find Out If You’ve Won By Joining Our Telegram<br />
                                    @ <a href="https://t.me/ghosttraderbsc">https://t.me/ghosttraderbsc</a><br />
                                    Mention Code "GTR COMP"
                                </p>
                                <p>
                                    Follow Us @ <a href="https://twitter.com/GhostTraderbsc">https://twitter.com/GhostTraderbsc</a><br /> 
                                    Website: <a href="https://ghosttrader.org">https://ghosttrader.org</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/popper.js"></script>
        <script src="assets/js/bootstrap.js"></script>
        <!--- intlTelInput JS for Country Code --->
        <script src="intlTelInput/js/intlTelInput.min.js"></script>
        <script src="intlTelInput/js/utils.js"></script>
        <script type="text/javascript" src="https://webservices.data-8.co.uk/javascript/loader.ashx?key=NDZR-SKMC-HSHK-S6RL&load=InternationalTelephoneValidation,EmailValidation"></script>
        <!-- <script type="text/javascript" src="https://webservices.data-8.co.uk/javascript/jqueryvalidation_min.js"></script> -->
        <script type="text/javascript">
            var validEmail = false;
            var validPhone = false;
            $("button").click(function(e) {
                var first_name = $("#first_name").val();
                var last_name = $("#last_name").val();
                var email = $("#email").val();
                var phoneNumber = $("#phone").val();
                
                if(first_name.length <= 1) {
                    $('#firstNameDiv p').remove('');
                    $('#firstNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >First Name is required!</p>');
                    return false;
                }
                else {
                    $('#firstNameDiv p').remove('');
                }
                if(last_name.length <= 1) {
                    $('#lastNameDiv p').remove('');
                    $('#lastNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >Last Name is required!</p>');
                    return false;
                }
                else {
                    $('#lastNameDiv p').remove('');
                }
                if(email.length <= 1) {
                    $('#emailDiv p').remove('');
                    $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Email is required!</p>');
                    return false;
                }
                else {
                    $('#emailDiv p').remove('');
                }
                if(phoneNumber.length <= 1) {
                    $('#phoneDiv p').remove('');
                    $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Phone Number is required!</p>');
                    return false;
                }
                else {
                    $('#phoneDiv p').remove('');
                }
                if($("#termsConditions").prop('checked') == false) {
                    $('#atcDiv p').remove('');
                    $('#atcDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please accept Terms & Conditions!</p>');
                    return false;
                }
                else {
                    $('#atcDiv p').remove('');
                }

                if(validEmail == true && validPhone == true) {
                    $.ajax( {
                        type: "POST",
                        url: "formToLead.php",
                        data: { 
                            first_name: $("#first_name").val(),
                            last_name: $("#last_name").val(),
                            email: $("#email").val(),
                            phone: $("#phone").val(),
                            country: $("#country_name").val(),
                            lid: $("#lid").val(),
                            aid: $("#aid").val()
                        },
                        success: function(result) {
                            if(result=="success") {
                                location.href = "thank_you.php";
                            }
                        },
                        error: function(result) {
                            location.href = "thank_you.php";
                        }
                    });
                }
            });

            $( document ).ready(function() {
                //country code script
                var input = document.querySelector("#phone"),
		        errorMsg = document.querySelector("#error-msg"),
		        validMsg = document.querySelector("#valid-msg");
                
                // Here, the index maps to the error code returned from getValidationError
		        var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

		        // Get country code
		        var countryData = window.intlTelInputGlobals.getCountryData(),
		        country_code = document.querySelector("#country_code");
		        // Initialise plugin
		        var iti = window.intlTelInput(input, {
		        	utilsScript: "intlTelInput/js/utils.js",
		        	initialCountry: 'gb'
		        });
		        // Set it's initial value
		        var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
		        // console.log(all_countrycode);
                
                // Listen to the telephone input for changes
                input.addEventListener('countrychange', function(e) {
                    var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
			        // console.log(selected_countrycode);
			        var selected_countryname = country_code.value = iti.getSelectedCountryData().name;
			        // console.log(selected_countryname);
			        $("#country_code").val(selected_countrycode);
			        $("#country_name").val(selected_countryname);
                });
                
                var reset = function() {
		        	input.classList.remove("error");
		        	errorMsg.innerHTML = "";
		        	errorMsg.classList.add("hide");
		        	validMsg.classList.add("hide");
		        };
                
                // On blur: validate
		        input.addEventListener('blur', function() {
		        	reset();
		        });
                
                // on keyup / change flag: reset
                input.addEventListener('change', reset);
                input.addEventListener('keyup', reset);
                
                // Validate email
                $("#email").change(function() {
                    var email = $("#email").val();
                    level='Address';
                    // alert(email);
                    var emailvalidation = new data8.emailvalidation();
                    emailvalidation.isvalid(
                        email,
                        level,
                        [

                        ],
                        showIsEmailValid
                    );
                });
                
                function showIsEmailValid(result) {
                    console.log(result.Result);
                    if(result.Result == 'Valid') {
                        validEmail = true;
                        $('#emailDiv p').remove('');
                        $("#btn-submit").attr('disabled',false); 
                    }
                    else {
                        validEmail = false;
                        $('#emailDiv p').remove('');
                        $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Email</p>');
                        $("#btn-submit").attr('disabled',true);
                    }
                }
                
                $('select#country_code').on('change',function() {
                    var country_code =  $(this). children("option:selected").text();
                    var code = country_code.replace('+','');
                    // console.log(code);
                    $('#country_code').val(code);
                })
                
                $("#phone").bind("change", function(e) {
                    var telephoneNumber = $("#phone").val();
                    if($("#country_code").val() == 'gb') {
                        defaultCountry='GB';
                        var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
                        internationaltelephonevalidation.isvalid(
                            telephoneNumber,
                            defaultCountry,
                            [
                                new data8.option('UseMobileValidation', 'false'),
                                new data8.option('UseLineValidation', 'false'),
                                new data8.option('RequiredCountry', ''),
                                new data8.option('AllowedPrefixes', ''),
                                new data8.option('BarredPrefixes', ''),
                                new data8.option('UseUnavailableStatus', 'true'),
                                new data8.option('UseAmbiguousStatus', 'true'),
                                new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                                new data8.option('ExcludeUnlikelyNumbers', 'false')
                            ],
                            showIsPhoneNumberValid
                        );
                    }
                    else {
                        defaultCountry='GB';
                        var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
                        internationaltelephonevalidation.isvalid(
                            telephoneNumber,
                            $("#country_code").val(),
                            [
                                new data8.option('UseMobileValidation', 'false'),
                                new data8.option('UseLineValidation', 'false'),
                                new data8.option('RequiredCountry', ''),
                                new data8.option('AllowedPrefixes', ''),
                                new data8.option('BarredPrefixes', ''),
                                new data8.option('UseUnavailableStatus', 'true'),
                                new data8.option('UseAmbiguousStatus', 'true'),
                                new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                                new data8.option('ExcludeUnlikelyNumbers', 'false')
                            ],
                            showIsPhoneNumberValid
                        );
                    }
                });
                function showIsPhoneNumberValid(result) {
                    console.log(result.Result.ValidationResult);
                    if(result.Result.ValidationResult == 'Valid') {
                        validPhone = true;
                        $('#phoneDiv p').remove('');
                        $("#btn-submit").attr('disabled',false);
                    }
                    else {
                        validPhone = false;
                        $('#phoneDiv p').remove('');
                        $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Phone Number</p>');
                        $("#btn-submit").attr('disabled',true);
                    }
                }
            });
        </script>
    </body>
</html>