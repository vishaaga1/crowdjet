<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>KCS Metals</title>
<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<!-- Stylesheet --->
<link rel="stylesheet" href="css/intlTelInput.css" />
<link rel="stylesheet" href="css/style.css">
<link rel="apple-touch-icon" href="favicon.png">
<link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>
<header>
  <div class="container-fluid">
    <div class="row">
    	<div class="col-lg-4 col-sm-12 my-auto">
        	<div class="row">
                <div class="col-12 my-auto">
                    <div class="logo-site"><a href="#"><img src="images/kcs-logo.png" class="img-fluid" /></a></div>
                    <div class="navbar-toggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 my-auto navigation">
              <nav>
                <ul>
                  <li>
                      <a href="#">HOME</a>
                  </li>
                  <li>
                      <a href="#service">SERVICES</a>
                  </li>
                  <li>
                      <a href="#contact">CONTACT</a>
                  </li>
                  <li class="top-email">
                      <a href="mailto:info@kcsmetals.com">
                          <img src="images/top-email.png" class="img-fluid" /> 
                          info@kcsmetals.com
                    </a>
                  </li>                 
                </ul>
              </nav>
        </div>
    </div>
  </div>
</header>
<section class="hero-section">
    <div class="container-fluid h-100">
        <div class="row h-100">
            <div class="col-lg-12 my-auto">
                <h1>METAL CLEARANCE & COLLECTION</h1>
            </div>
        </div>
    </div>
</section>
<section class="section-two">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="iner">
                    <div class="row flex-row-reverse">
                        <div class="col-lg-6 left-box">
                            <div class="">
                                <h2>WAREHOUSE & SITE CLEARANCE</h2>
                                <h3>Covering Surrey and the South EAST</h3>
                                <p>
                                    Based in Surrey, we cover the whole of the South East region. 
                                </p>
                                <p>
                                    We operate as fully independent, scrap metal merchants, offering a personal service tailored to suit your exact requirements. 
                                <p>
                                <p>
                                    As a fully licensed Upper tier Waste Carrier, Broker & Dealer (License No: CBDU418537) and a 
                                    environmentally conscious company, we are a trusted buyer for all non-ferrous metals in the South 
                                    East region at fair prices.
                                </p>
                                <p>
                                    KCS Metals offer the best marketplace prices for any unwanted scrap metal.
                                </p>
                                <p>
                                    We provide a collection service throughout the South East completely free.
                                </p> 
                                <p>
                                    Our professional team guide you through the process from start to finish.
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="right-box">
                                <img src="images/metal-merchants.png"  class="img-fluid" class="img" alt="">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-three">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2>SOME OF THE METALS WE DEAL WITH INCLUDE</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="main-box">
                    <div class="boxes">
                        <div class="images-box">
                            <img src="images/brass.png" class="img-fluid" alt="">
                        </div>
                        <h3>Brass</h3>
                    </div>
                    <div class="boxes">
                        <div class="images-box">
                            <img src="images/bronze.png" class="img-fluid" alt="">
                        </div>
                        <h3>Bronze</h3>
                    </div>
                    <div class="boxes">
                        <div class="images-box">
                            <img src="images/zinc.png" class="img-fluid" alt="">
                        </div>
                        <h3>Zinc</h3>
                    </div>
                    <div class="boxes">
                        <div class="images-box">
                            <img src="images/copper.png" class="img-fluid" alt="">
                        </div>
                        <h3>Copper</h3>
                    </div>
                    <div class="boxes">
                        <div class="images-box">
                            <img src="images/aluminium.png" class="img-fluid" alt="">
                        </div>
                        <h3>Aluminium</h3>
                    </div>
                    <div class="boxes">
                        <div class="images-box">
                            <img src="images/alloys.png" class="img-fluid" alt="">
                        </div>
                        <h3>Aluminium<br /> Alloys</h3>
                    </div>
                    <div class="boxes">
                        <div class="images-box">
                            <img src="images/stainless-steel.png" class="img-fluid" alt="">
                        </div>
                        <h3>
                            Stainless <br /> Steel
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-five">
<div id="service"></div>
    <div class="container-fluid">
        <div class="row flex-row-reverse">
            <div class="col-lg-6 my-auto">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="left-box">
                            <h2>WAREHOUSE & SITE CLEARANCE</h2>
                            <p>
                                We are very experienced contractors and are fully equipped to clear or strip any 
                                site ready for redevelopment.
                            </p>
                            <p>
                                We pride ourselves on a fast turn around and can clear any site leaving it safe and secure. 
                            </p>
                            <p>
                                We can quote to dismantle and recycling all shelving, pallet racking and mezzanine flooring.
                            </p>
                            <p>
                                KCS Metals have been buyers and sellers of industrial/restaurant kitchen and catering equipment, were we aim to buy and collect on the same day.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 right-box">
                <img src="images/warehouse-site.png"  class="img-fluid" class="img" alt="">
            </div>
        </div>
    </div>
</section>
<section class="section-foure">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 my-auto">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="row justify-content-end">
                            <div class="col-lg-10">
                                <div class="left-box">
                                    <h2>STRIP OUT</h2>
                                    <p>
                                        KCS Metals is your ideal company with all your strip out needs with maximum 
                                        efficiency and minimal fuss.
                                    </p>
                                    <p>
                                        When stripping a building back to bare shell, work needs to be done safely 
                                        within regulations and cause little disruption.
                                    </p>
                                    <p>
                                        We work on getting back to your quote within 24 hours from your application.
                                    </p>
                                    <p>
                                        Were all items were properly removed from site and completed with no complications with maximum satisfaction.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 right-box">
                <img src="images/strip-out.png" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>
<section class="section-five">
    <div class="container-fluid">
        <div class="row flex-row-reverse">
            <div class="col-lg-6 my-auto">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="left-box">
                            <h2>AC UNIT DISPOSAL</h2>
                            <p>
                                Got a air conditioning/ chiller unit you need collecting and disposed off?
                            </p>
                            <p>
                                We can arrange for them to be lifted of roof tops and can be dismantled on and off site depending on your discretion. Call today to speak to a member of our team and arrange a free quote.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 right-box">
                <img src="images/unit-disposal.png" class="img-fluid" class="img" alt="">
            </div>
        </div>
    </div>
</section>
<section class="section-six">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <h2>ENVIRONMENTALLY CONSCIOUS<br /> SCRAP METAL MERCHANTS</h2>
                        <p>
                            With responsible waste disposal and recycling more important than ever, people choose their scrap metal merchants carefully.
                        </p>
                        <p>
                            We prioritise conscientious and responsible practices at all times, ensuring environmental care remains at the forefront of our <a href="#">services.</a> 
                        </p>
                        <p>
                            We take every step possible to preserve our local environment while lowering the impact of our carbon footprint.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact-us">
<div id="contact"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 my-auto">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="row justify-content-end">
                            <div class="col-lg-10">
                                <form method="POST" id="kcsmetals">
                                    <div class="left-box">
                                        <h2>Contact Us</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-box">
                                                    <input type="text" id="first_name" placeholder="First Name" value="" required="" />
                                                    <span id="firstName_error" style="color: #dc3545;font-size: 16px;"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-box">
                                                    <input type="text" id="last_name" placeholder="Last Name" />
                                                    <span id="lastName_error" style="color: #dc3545;font-size: 16px;"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-box">
                                                    <input type="text" id="email" placeholder="Email" />
                                                    <span id="Invalid_error" style="color: #dc3545;font-size: 16px;"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-box">
                                                    <input id="phone" name="phone" id="phone" type="tel" placeholder="Phone" />
                                                    <input type="hidden" name="country_code" id="country_code" value="44">
                                                    <input type="hidden" name="country_name" id="country_name" value="United Kingdom" placeholder="Country">
                                                    <span id="valid-msg" class="hide">✓ Valid</span>
                                                    <span id="error-msg" style="color: #dc3545;font-size: 16px;"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-box">
                                                    <textarea placeholder="Message"></textarea>
                                                </div>
                                            </div>
                                            <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>" />
                                            <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>" />
                                            <div class="col-md-12">
                                                <!-- <button type="submit">Send</button> -->
                                                <button type="button" id="btn-submit" name="submit">Send</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 p-0 my-auto">
                <div class="map-box">
                    <div class="google-maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2487.36497288784!2d-0.4644059842327457!3d51.433090879622995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487673f91ba2f60b%3A0x13baec9fe6053ce1!2s54a%20Church%20Rd%2C%20Ashford%20TW15%202TS%2C%20UK!5e0!3m2!1sen!2sin!4v1642409224033!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
                <div class="contact-details">
                    <div class="row">
                        <div class="col-sm-3">
                            <ul>
                                <li>
                                    <a href="#">
                                        <div class="icons">
                                            <img src="images/phone.png" class="img-fluid" alt="">
                                        </div>
                                        XXXXX
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="icons">
                                            <img src="images/phone.png" class="img-fluid" alt="">
                                        </div>
                                        XXXXX
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-5">
                            <ul>
                                <li>
                                    <a href="mailto:info@kcsmetals.com">
                                        <div class="icons">
                                            <img src="images/email.png" class="img-fluid" alt="">
                                        </div>
                                        info@kcsmetals.com
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-4">
                            <ul>
                                <li>
                                    <a href="#">
                                        <div class="icons">
                                            <img src="images/phone.png" class="img-fluid" alt="">
                                        </div>
                                        54a Church Rd<br /> Ashford<br /> Middlesex <br /> TW15 2TS
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
	<div class="container-fluid">
    	<div class="row justify-content-center">
        	<div class="col-lg-12">
            	<div class="iner">
                    <div class="row flex-row-reverse justify-content-center">
                        <div class="col-lg-6 my-auto">
                        	<div class="row">
                                <div class="col-lg-12 my-auto">
                                    <div class="footer-nav">
                                        <ul>
                                            <li><a href="#">Privacy Policy</a></li> 
                                            <li><a href="#">Cookie Policy</a></li>
                                            <li><a href="#">Terms & Conditions</a></li>
                                        </ul>
                                    </div>       
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 my-auto">
                        	<div class="copy-right">
                                <p>
                                	&copy; 2022, KCS Metals
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- jQuery library --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
<script src="js/function.js"></script>
<script src="js/intlTelInput.js"></script>
<script type="text/javascript" src="https://webservices.data-8.co.uk/javascript/loader.ashx?key=ZGA9-H4YC-6J84-D6XI&load=InternationalTelephoneValidation,EmailValidation"></script>
<script type="text/javascript">
    var validEmail = false;
    var validPhone = false;
    $( document ).ready(function() {
      //Country code script
      var input = document.querySelector("#phone"),
      errorMsg = document.querySelector("#error-msg"),
      validMsg = document.querySelector("#valid-msg");

      //Here, the index maps to the error code returned from getValidationError - see readme
      var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

      //Get country code
      var countryData = window.intlTelInputGlobals.getCountryData(),
      country_code = document.querySelector("#country_code");
      //Initialise plugin
      var iti = window.intlTelInput(input, {
        utilsScript: "intlTelInput/js/utils.js",
        initialCountry: 'gb'
      }); 
      //Set it's initial value
      var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
      // console.log(all_countrycode);

      //Listen to the telephone input for changes
      input.addEventListener('countrychange', function(e) {
        var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
        console.log(selected_countrycode);
      });

      var reset = function() {
        input.classList.remove("error");
        errorMsg.innerHTML = "";
        errorMsg.classList.add("hide");
        validMsg.classList.add("hide");
      };

      //On blur: validate
      input.addEventListener('blur', function() {
        reset();
      });

      //On keyup / change flag: reset
      input.addEventListener('change', reset);
      input.addEventListener('keyup', reset);

      //Validate email
      $("#email").change(function(){ 
        var email = $("#email").val();
        level='Address';
        //alert(email);
        var emailvalidation = new data8.emailvalidation();

        emailvalidation.isvalid(
          email,
          level,
          [
          
          ],
          showIsValidResult
        );
      });

      function showIsValidResult(result) {
        console.log(result.Result);
        if(result.Result == 'Valid') {
          validEmail = true;
          $('#Invalid_error').hide();
          $('#btn-submit').prop('disabled', false);
        }
        else
        {
          validEmail = false;
          $('#Invalid_error').show();
          $('#Invalid_error').html('Please Enter Valid Email.');
          $('#btn-submit').prop('disabled', true);
        }
      }

      $('select#country_code').on('change',function(){
        var country_code =  $(this). children("option:selected").text();
        var code = country_code.replace('+','');
        console.log(code);
        $('#code_of_country').val(code);
      })

      $("#phone").bind("change", function(e) {
        var telephoneNumber = $("#phone").val();
        if($("#country_code").val() == 'gb') {
          console.log(e);
          defaultCountry='GB';
          var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
          internationaltelephonevalidation.isvalid(
            telephoneNumber,
            defaultCountry,
            [
              new data8.option('UseMobileValidation', 'false'),
              new data8.option('UseLineValidation', 'false'),
              new data8.option('RequiredCountry', ''),
              new data8.option('AllowedPrefixes', ''),
              new data8.option('BarredPrefixes', ''),
              new data8.option('UseUnavailableStatus', 'true'),
              new data8.option('UseAmbiguousStatus', 'true'),
              new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
              new data8.option('ExcludeUnlikelyNumbers', 'false')
            ],
            showIsValidResults
          );
        }
        else{
          console.log(e);
          defaultCountry='GB';
          var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
          internationaltelephonevalidation.isvalid(
            telephoneNumber,
            $("#country_code").val(),
            [
              new data8.option('UseMobileValidation', 'false'),
              new data8.option('UseLineValidation', 'false'),
              new data8.option('RequiredCountry', ''),
              new data8.option('AllowedPrefixes', ''),
              new data8.option('BarredPrefixes', ''),
              new data8.option('UseUnavailableStatus', 'true'),
              new data8.option('UseAmbiguousStatus', 'true'),
              new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
              new data8.option('ExcludeUnlikelyNumbers', 'false')
            ],
            showIsValidResults
          );
        }
      });
      
      function showIsValidResults(result) {
        console.log(result.Result.ValidationResult);
        if(result.Result.ValidationResult == 'Valid') {
          validPhone = true;
          $('#error-msg').hide();
          $('#btn-submit').prop('disabled', false);
        }
        else {
          validPhone = false;
          $('#error-msg').show();
          $('#error-msg').html('Please Enter Valid Phone Number');
          $('#btn-submit').prop('disabled', true);
        }
      }
    });
    
    $("#btn-submit").click(function(e) {
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var email = $("#email").val();
    var phoneNumber = $("#phone").val();
    
    if(first_name.length <= 1) {
      $('#firstName_error').html('First Name is required!');
      return false;
    }
    else{
      $('#firstName_error').html('');
    }
    if(last_name.length <= 1) {
      $('#lastName_error').html('Last Name is required!');
      return false;
    }
    else{
      $('#lastName_error').html('');
    }
    if(email.length <= 1) {
      $('#Invalid_error').html('Email is required!');
      return false;
    }
    else{
      $('#Invalid_error').html('');
    }
    if(phoneNumber.length <= 1) {
      $('#error-msg').html('Phone Number is required!');
      return false;
    }
    else{
      $('#error-msg').html('');
    }
    
    if(validEmail == true && validPhone == true){
      $.ajax({
          type: "POST",
          url: "zoho/form_data.php",
          data: { 
              first_name: $("#first_name").val(),
              last_name: $("#last_name").val(),
              email: $("#email").val(),
              phone: $("#phone").val(),
              lid: $("#lid").val(),
              aid: $("#aid").val()
          },
          success: function(result) {
              if(result=="success"){
                  location.href = "thankyou.html";
              }
          },
          error: function(result) {
              location.href = "thankyou.html";
          }
      });
    }
  });
  </script>
  <!--- teleinput for country code js --->
  <script src="js/utils.js"></script>
<!-- <script>	
$(document).ready(function(e){
    var input = document.querySelector("#phone");
        window.intlTelInput(input, {
    });
});
</script> -->
</body>
</html>
