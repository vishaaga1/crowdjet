<form method="post" action="options.php">
<?php settings_fields( 'd8cf7-settings-group' ); ?>
<table class="wp-list-table widefat fixed bookmarks">
    <thead>
        <tr>
            <th><strong>Settings</strong></th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <table class="form-table d8cf7_form">
				<tr>
					<td colspan="2">
						This plugin uses the Data8 Email Validation, International Telephone Validation and PredictiveAddress&trade;
						services to ensure you are capturing valid contact details in Woocommerce, Gravity Forms and Contact Form 7
						and improve the user experience. This requires credits for the relevant services. If you already have an accounts
						with Data8, enter your Data8 login details below. Otherwise, 
						<a href="https://www.data-8.co.uk/ValidationAdmin/FreeTrial/ContactForm7" target="_blank">sign up for a free trial</a>.
					</td>
				</tr>
                <tr valign="top">
                    <td scope="row">Data8 Username</td>
                    <td>
                        <input type="text" name="d8cf7_username" style="width:150px;" value="<?php echo esc_attr(get_option('d8cf7_username')); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <td scope="row">Data8 Password</td>
                    <td>
                        <input type="password" name="d8cf7_password" style="width:150px;" value="" />
						
						<?php
						if (get_option('d8cf7_password')) {
						?>
						<br />
						<small>Your existing password is saved but is not displayed here for security reasons.</small>
						<?php } ?>
                    </td>
                </tr>
				
				<?php
				if (get_option('d8cf7_error')) {
				?>
				<tr>
					<td colspan="2" class="notice notice-error">
						<?php echo esc_attr(get_option('d8cf7_error')); ?>
					</td>
				</tr>
				<?php } ?>
				
				<?php
				if (get_option('d8cf7_email_validation')) {
				?>
				<tr>
					<td colspan="2">
						Data8 enhanced <a href="https://www.data-8.co.uk/services/data-validation/validation-services/email-validation" target="_blank">Email Validation</a> will automatically be applied to all email fields in Gravity Forms and Contact Form 7.
					</td>
				</tr>
				<?php } else { ?>
				<tr>
					<td colspan="2" style="color:brown;">
						Data8 enhanced <a href="https://www.data-8.co.uk/services/data-validation/validation-services/email-validation" target="_blank">Email Validation</a> will NOT be applied. Ensure you have credits for this service, enter your Data8 username &amp; password above and click Save Changes to enable this option.
					</td>
				</tr>
				<?php } ?>
				
				<?php
				if (get_option('d8cf7_telephone_validation')) {
				?>
				<tr>
					<td colspan="2">
						Data8 enhanced <a href="https://www.data-8.co.uk/services/data-validation/validation-services/phone-validation" target="_blank">International Telephone Number Validation</a> will automatically be applied to all telephone fields in Gravity Forms and Contact Form 7.
					</td>
				</tr>
				<?php } else { ?>
				<tr>
					<td colspan="2" style="color:brown;">
						Data8 enhanced <a href="https://www.data-8.co.uk/services/data-validation/validation-services/phone-validation" target="_blank">International Telephone Number Validation</a> will NOT be applied. Ensure you have credits for this service, enter your Data8 username &amp; password above and click Save Changes to enable this option.
					</td>
				</tr>
				<?php } ?>
				
				<?php
				if (get_option('d8cf7_predictiveaddress')) {
				?>
				<tr>
					<td colspan="2">
						Data8 <a href="https://www.data-8.co.uk/services/data-validation/validation-services/address-validation" target="_blank">PredictiveAddress&trade;</a> auto-complete will automatically be applied to address fields in Woocommerce and Gravity Forms, and fields in Contact Form 7 with appropriate classes (see below)
					</td>
				</tr>
				<?php } else { ?>
				<tr>
					<td colspan="2" style="color:brown;">
						Data8 <a href="https://www.data-8.co.uk/services/data-validation/validation-services/address-validation" target="_blank">PredictiveAddress&trade;</a> auto-complete will NOT be applied. Ensure you have credits for this service, enter your Data8 username &amp; password above and click Save Changes to enable this option.
					</td>
				</tr>
				<?php } ?>
				
				<?php
				if (get_option('d8cf7_salaciousName')) {
				?>
				<tr>
					<td colspan="2">
					Data8 SalaciousName&trade; validation will automatically be applied to address fields in Gravity Forms and Contact Form 7
					</td>
				</tr>
				<?php } else { ?>
				<tr>
					<td colspan="2" style="color:brown;">
						Data8 SalaciousName&trade; validation will NOT be applied. Ensure you have credits for this service, enter your Data8 username &amp; password above and click Save Changes to enable this option.
					</td>
				</tr>
				<?php } ?>
            </table>
            
            <p class="submit">
				<input type="submit" name="submit-d8cf7" class="button-primary" value="<?php _e('Save Changes') ?>" />
            </p>
            
        </td>
        
    </tr>
    </tbody>
</table>
<br/>
</form>