=== Data8 Validation ===
Contributors: data8
Tags: woocommerce, gravity, gravity forms, gravityforms, contact form 7, contactform7, cf7, data validation, email, telephone, address, addresses, autocomplete, autofill, cell, checkout, data, form, international, mobile, paf, phone, usps, royal mail, validation, verification
Requires at least: 4.6.1
Tested up to: 4.9.8
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Applies Data8 Email, International Telephone Validation and PredictiveAddress services to WooCommerce checkout, Gravity Forms and Contact Form 7 input

== Description ==

Verify international addresses, email addresses and telephone numbers as they are entered into your WooCommerce shopping cart and Gravity Forms and Contact Form 7 forms.

International coverage for all services, advanced fuzzy matching and backed by official data from sources such as USPS and Royal Mail ensures you can rely on the data being entered.

Improve your user experience with fast, natural address entry.

Ensure you capture accurate contact details and eliminate spam contacts by verifying email addresses and telephone numbers in real time.

Quick to set up with a free trial. This extension is free of charge, just pay for the credits you use for our validation services.

= WooCommerce =

The Data8 PredictiveAddress service is automatically applied to all address entry forms in WooCommerce, including billing and shipping addresses on checkout and in the "My Account" section.

= Gravity Forms =

The Data8 PredictiveAddress service is automatically applied to all Address fields
The Data8 Email Validation service is automatically applied to all Email fields
The Data8 International Telephone Validation service is automatically applied to all Phone fields
The SalaciousName service is automatically applied to all name fields

= Contact Form 7 =

The Data8 PredictiveAddress service is automatically applied to all correctly tagged Address fields
The Data8 Email Validation service is automatically applied to all correctly tagged Email fields
The Data8 International Telephone Validation service is automatically applied to all correctly tagged Phone fields
The Salacious Name service is automatically applied to all correctly tagged name fields

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/data8-validation-for-contact-form-7` directory
1. Activate the plugin through the 'Plugins' screen in Wordpress
1. Use the Settings link on the 'Plugins' screen to configure the plugin
1. Enter your Data8 username and password and click Save
1. Email and International Telephone Validation will be automatically enabled on all relevant fields
1. Salacious Name service is automatically applied to all relevant fields
1. Add class names to address entry fields as described on the settings screen to enable PredictiveAddress
 
== Frequently Asked Questions ==
 
= How do I change the email validation level? =

Any email field in Gravity Forms and Contact Form 7 will be automatically validated. By default, MX level email validation is used.

In Contact Form 7, the email validation level can be changed using the level option, e.g.
[email* your-email level:Address]

In Gravity Forms, the email validation level can be changed by adding a "d8level_Address" CSS class to the Custom CSS Class setting.
 
The following are valid email validation levels:

* Syntax - validates the syntax of the email address (quickest)
* MX - validates the domain name (right hand part) the email address (default)
* Server - validates the mail servers for the domain are alive
* Address - validates the full email address (slowest)

= How do I use the full Data8 Mobile and Landline validation services? =

Any telephone field will be automatically validated using the Data8 International Telephone Validation service. If you have credits for the Data8 Mobile Validation service or Live Number Testing service, you can enable the use of these as follows:

In Gravity Forms, add the "d8mobile" and "d8landline" CSS classes to the Custom CSS Class setting.

In Contact Form 7, use the "mobile" and "landline" tags:
[tel* your-tel mobile:true landline:true]

This will provide full number validation for international mobile numbers and UK landline numbers.

= How do I enter international telephone numbers? =

The International Telephone Validation service will validate the entered number using the rules appropriate for the country indicated in the telephone number using standard international dialling rules. For example, if a number is entered with a "+1" or "001" prefix, it will be validated as a US number, or if "+44" or "0044" is used it will be validated as a UK number.

If no international prefix is specified, it will be validated according to the rules for a default country. This will normally be UK, but can be changed as follows:

In Gravity Forms, add the "d8country_XX" CSS class to the Custom CSS Class setting, e.g. d8country_US

In Contact Form 7, use the "country" tag, e.g.:
[tel* your-tel country:US]

== Changelog ==

= 1.0 =
* Initial release!

= 1.1 =
* Fixed "Fatal error: Cannot use object of type stdClass as array" errors

= 1.2 =
* Fixed use of telephone validation without email validation

= 1.3 =
* Fixed syntax error on PHP 5.3

= 1.4 =
* Added support for WooCommerce and Gravity Forms

= 1.5 =
* Added support for SalaciousName to Gravity Forms & Contact Forms 7
* Moved the setting page to it's own page

= 1.6 =
* Added AllowedPrefixes & BarredPrefixes parameters to Tel Val

== Upgrade Notice ==

= 1.0 =
* First version, nothing to upgrade!

= 1.1 =
* Bug fixes for when your Data8 account only has access to a single web service

= 1.2 =
* Bug fixes for when your Data8 account has access to telephone validation but not email validation

= 1.3 =
* Bug fix for installing on PHP 5.3

= 1.4 =
* Added support for WooCommerce and Gravity Forms

= 1.5 =
* Added support for SalaciousName to Gravity Forms & Contact Forms 7
* Moved the setting page to it's own page

= 1.6 =
* Added AllowedPrefixes & BarredPrefixes parameters to Tel Val

== Screenshots ==

1. Configuration screen
2. PredictiveAddress in action