<?php 
/* Template Name: Contact Page Template */ 
get_header();

?>
<div class="bd-contentlayout-7 bd-page-width   bd-sheetstyles-2   bd-no-margins bd-margins">
<div class="main_container">
<div class="contact-container">
	<div class="row">
		<?php echo do_shortcode('[contact-form-7 id="86" title="Lead Capture Form"]'); ?>
	</div>
</div>
</div>
</div>
<div class="contact-space">
<p>&nbsp;</p>
</div>

<script type="text/javascript">
	jQuery(document).ready(function ($) {
        $('form').submit(function(e) {
        var new_data = $('form').serializeArray().reduce(function(obj, item) {
            obj[item.name] = item.value;
            return obj;
        }, {});
        console.log(new_data);
        var click_id=new_data['clkid'];
        var acceptance =new_data['acceptance-594'];
    
        if (click_id != '' && acceptance == "1" ){
            // console.log(acceptance);
        var data = {};
        data['first_name'] =new_data['first-name'];
        data['last_name'] =new_data['last-name'];
        data['Phone'] =new_data['tel-783'];
        data['Email'] =new_data['email-623'];
        data['source'] =new_data['lead_source'];
        data['clickid'] =new_data['clkid'];
        data['radio_q1'] =new_data['radio-q1'];
        data['radio_q2'] =new_data['radio-q2'];
        data['radio_q3'] =new_data['radio-q3'];
        data['radio_q4'] =new_data['radio-q4'];
        console.log(data);
        $.ajax({
        url : "https://isafinder.co.uk/zoho/form_data.php",
        type: "POST",
        data : JSON.stringify(data),
        success: function(d)
        {
            console.log(d);
        },
        error: function (errorThrown)
        {
            console.log('error mess');
            console.log(errorThrown);
        }
    });
    }else{
        console.log('else');
    }
});
    });
	

</script>



<?php
get_footer();
?>
