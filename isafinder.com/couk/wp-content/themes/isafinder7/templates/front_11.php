<?php
/*
Template Name: Home 2
*/
$GLOBALS['theme_current_template_info'] = array('name' => 'home-2');
?>
<?php if (!defined('ABSPATH')) exit; // Exit if accessed directly
?>
<!DOCTYPE html>
<html <?php echo !is_rtl() ? 'dir="ltr" ' : ''; language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>" />
    
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <script>
    var themeHasJQuery = !!window.jQuery;
</script>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/jquery.js?ver=' . wp_get_theme()->get('Version'); ?>"></script>
<script>
    window._$ = jQuery.noConflict(themeHasJQuery);
</script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.css' ?>" />
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.js' ?>"></script>
<![endif]-->
<link class="" href='//fonts.googleapis.com/css?family=Raleway:100,200,300,regular,500,600,700,800,900&subset=latin' rel='stylesheet' type='text/css'>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.core.js' ?>"></script>
<script src="<?php echo get_bloginfo('template_url', 'display'); ?>/CloudZoom.js?ver=<?php echo wp_get_theme()->get('Version'); ?>" type="text/javascript"></script>
    
    <?php wp_head(); ?>
    
</head>
<?php do_action('theme_after_head'); ?>
<?php ob_start(); // body start ?>
<body <?php body_class(' hfeed bootstrap bd-body-11 
 bd-homepage bd-pagebackground bd-margins'); /*   */ ?>>
<header class=" bd-headerarea-1 bd-margins">
        <section class=" bd-section-3 hidden-md hidden-sm hidden-xs hidden-lg bd-tagstyles" id="section3" data-section-title="Menu">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutbox-3 bd-no-margins clearfix">
    <div class="bd-container-inner">
        <?php
    if (theme_get_option('theme_use_default_menu')) {
        wp_nav_menu( array('theme_location' => 'primary-menu-1') );
    } else {
        theme_hmenu_1();
    }
?>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-5 bd-tagstyles" id="section5" data-section-title="Header">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-8 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-7 
 col-lg-2
 col-xs-12">
    <div class="bd-layoutcolumn-7 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-2', '16_13');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-2', ' bd-sidebar-13 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-18 
 col-lg-10
 col-xs-12">
    <div class="bd-layoutcolumn-18 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-4', '9_15');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-4', ' bd-sidebar-15 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
</header>
	
		<section class=" bd-section-32 hidden-md hidden-sm hidden-xs bd-page-width bd-tagstyles  bd-textureoverlay bd-textureoverlay-1" id="section9" data-section-title="Header BG Image Desktop">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-45 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-121 
 col-lg-6
 col-xs-12">
    <div class="bd-layoutcolumn-121 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-6', '13_64');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-6', ' bd-sidebar-64 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-6 hidden-lg bd-page-width bd-tagstyles  bd-textureoverlay bd-textureoverlay-2" id="section9" data-section-title="Header BG Image Mobile">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-11 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-21 
 col-lg-6
 col-xs-12">
    <div class="bd-layoutcolumn-21 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-6', '15_81');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-6', ' bd-sidebar-81 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<div class=" bd-stretchtobottom-7 bd-stretch-to-bottom" data-control-selector=".bd-contentlayout-11">
<div class="bd-contentlayout-11 bd-page-width   bd-sheetstyles   bd-no-margins bd-margins" >
    <div class="bd-container-inner">

        <div class="bd-flex-vertical bd-stretch-inner bd-no-margins">
            
            <div class="bd-flex-horizontal bd-flex-wide bd-no-margins">
                
                <div class="bd-flex-vertical bd-flex-wide bd-no-margins">
                    

                    <div class=" bd-layoutitemsbox-4 bd-flex-wide bd-margins">
    <div class=" bd-content-6">
    
        <?php theme_blog_4(); ?>
</div>
</div>

                    
                </div>
                
            </div>
            
        </div>

    </div>
</div></div>
	
		<section class=" bd-section-31 hidden-md hidden-sm hidden-xs hidden-lg bd-tagstyles" id="section12" data-section-title="ISA Examples">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-43 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-119 
 col-sm-12">
    <div class="bd-layoutcolumn-119 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-8', '1_62');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-8', ' bd-sidebar-62 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-34 hidden-md hidden-sm hidden-xs bd-tagstyles" id="section4" data-section-title="ISA Example 2 - Desktop">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-51 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-183 
 col-sm-12">
    <div class="bd-layoutcolumn-183 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-67', '17_68');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-67', ' bd-sidebar-68 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		<div class=" bd-layoutcontainer-47 bd-no-margins bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-151 
 col-lg-2">
    <div class="bd-layoutcolumn-151 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-72 animated bd-animation-30 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">£5,000</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-71 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Initial Amount Invested
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-150 
 col-lg-1">
    <div class="bd-layoutcolumn-150 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-37 animated bd-animation-29 bd-own-margins bd-icon-94 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-22 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-149 
 col-lg-2">
    <div class="bd-layoutcolumn-149 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-70 animated bd-animation-28 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    8%</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-69 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Annual Interest Rate
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-148 
 col-lg-1">
    <div class="bd-layoutcolumn-148 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-36 animated bd-animation-27 bd-own-margins bd-icon-93 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-21 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-147 
 col-lg-2">
    <div class="bd-layoutcolumn-147 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-68 animated bd-animation-26 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    £2,346.64</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-67 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Your Tax-free Interest
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-146 
 col-lg-1">
    <div class="bd-layoutcolumn-146 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-35 animated bd-animation-25 bd-own-margins bd-icon-92 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-20 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-145 
 col-lg-2">
    <div class="bd-layoutcolumn-145 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-66 animated bd-animation-24 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    £7346.64</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-65 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Total Amount After 5 Years
CUSTOM_CODE;
?>
</h2></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-39 hidden-sm hidden-xs hidden-lg bd-tagstyles" id="section4" data-section-title="ISA Example 2 - Laptop">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-63 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-257 
 col-sm-12">
    <div class="bd-layoutcolumn-257 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-67', '20_84');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-67', ' bd-sidebar-84 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		<div class=" bd-layoutcontainer-62 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-255 
 col-md-4">
    <div class="bd-layoutcolumn-255 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-125 animated bd-animation-60 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    £5,000</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-124 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Initial Amount Invested
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-254 
 col-md-4">
    <div class="bd-layoutcolumn-254 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-60 animated bd-animation-59 bd-own-margins bd-icon-116 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-48 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-253 
 col-md-4">
    <div class="bd-layoutcolumn-253 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-123 animated bd-animation-58 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    8%</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-122 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Annual Interest Rate
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-251 
 col-md-4">
    <div class="bd-layoutcolumn-251 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-121 animated bd-animation-56 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
£2,346.64
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-120 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Your Tax-free Interest
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-250 
 col-md-4">
    <div class="bd-layoutcolumn-250 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-58 animated bd-animation-55 bd-own-margins bd-icon-114 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-46 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-249 
 col-md-4">
    <div class="bd-layoutcolumn-249 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-119 animated bd-animation-54 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    £7346.64</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-118 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Total Amount After 5 Years
CUSTOM_CODE;
?>
</h2></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-41 hidden-md hidden-lg bd-background-width bd-tagstyles " id="section4" data-section-title="ISA Example 2 - Mobile">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-67 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-289 
 col-sm-12">
    <div class="bd-layoutcolumn-289 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-67', '21_88');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-67', ' bd-sidebar-88 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		<div class=" bd-layoutcontainer-66 bd-background-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-287 
 col-xs-12">
    <div class="bd-layoutcolumn-287 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-141 animated bd-animation-74 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    £5,000</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-140 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Initial Amount Invested
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-286 
 col-xs-12">
    <div class="bd-layoutcolumn-286 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-66 animated bd-animation-73 bd-own-margins bd-icon-122 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-54 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-285 
 col-xs-12">
    <div class="bd-layoutcolumn-285 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-139 animated bd-animation-72 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    8%</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-138 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Annual Interest Rate
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-284 
 col-xs-12">
    <div class="bd-layoutcolumn-284 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-65 animated bd-animation-71 bd-own-margins bd-icon-121 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-53 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-283 
 col-xs-12">
    <div class="bd-layoutcolumn-283 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-137 animated bd-animation-70 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
£2,346.64
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-136 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Your Tax-free Interest
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-282 
 col-xs-12">
    <div class="bd-layoutcolumn-282 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-64 animated bd-animation-69 bd-own-margins bd-icon-120 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-52 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-281 
 col-xs-12">
    <div class="bd-layoutcolumn-281 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-135 animated bd-animation-68 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
<a href="https://isafinder.co.uk/compare-now">
    £7346.64</a>
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-134 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Total Amount After 5 Years
CUSTOM_CODE;
?>
</h2></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-30 hidden-md hidden-sm hidden-xs hidden-lg bd-tagstyles" id="section15" data-section-title="ISA Comparison">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-42 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-117 
 col-sm-12">
    <div class="bd-layoutcolumn-117 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-10', '1_60');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-10', ' bd-sidebar-60 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-36 bd-tagstyles" id="section4" data-section-title="ISA Comparison 2">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-53 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-191 
 col-sm-12">
    <div class="bd-layoutcolumn-191 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-71', '22_72');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-71', ' bd-sidebar-72 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		<div class=" bd-layoutcontainer-49 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-175 
 col-md-3
 col-xs-12">
    <div class="bd-layoutcolumn-175 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-45 animated bd-animation-38 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-102 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-30 clearfix"></div>
	
		<h2 class=" bd-textblock-80 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Cash<br>
ISA
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-166 
 col-md-3
 col-xs-12">
    <div class="bd-layoutcolumn-166 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-44 animated bd-animation-37 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-101 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-29 clearfix"></div>
	
		<h2 class=" bd-textblock-79 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Easy Access&nbsp;<br>ISA
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-165 
 col-md-3
 col-xs-12">
    <div class="bd-layoutcolumn-165 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-43 animated bd-animation-36 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-100 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-28 clearfix"></div>
	
		<h2 class=" bd-textblock-78 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Stocks &amp; Shares&nbsp;<br>ISA
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-164 
 col-md-3
 col-xs-12">
    <div class="bd-layoutcolumn-164 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-42 animated bd-animation-35 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-99 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-27 clearfix"></div>
	
		<h2 class=" bd-textblock-77 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Innovative Finance&nbsp;
ISA
CUSTOM_CODE;
?>
</h2></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		<div class=" bd-layoutcontainer-55 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-199 
 col-sm-12">
    <div class="bd-layoutcolumn-199 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-75', '23_76');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-75', ' bd-sidebar-76 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-37 bd-tagstyles" id="section4" data-section-title="Section 4">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-57 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-203 
 col-md-4
 col-sm-6">
    <div class="bd-layoutcolumn-203 bd-background-width  bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-47 animated bd-animation-40 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-104 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-32 clearfix"></div>
	
		<p class=" bd-textblock-88 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Invest In Different Sectors And Industries
CUSTOM_CODE;
?>
</p></div></div>
</div>
	
		<div class=" bd-columnwrapper-205 
 col-md-4
 col-sm-6">
    <div class="bd-layoutcolumn-205 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-49 animated bd-animation-42 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-106 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-34 clearfix"></div>
	
		<p class=" bd-textblock-92 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
ISAs With 8% Yearly Returns
CUSTOM_CODE;
?>
</p></div></div>
</div>
	
		<div class=" bd-columnwrapper-207 
 col-md-4
 col-sm-6">
    <div class="bd-layoutcolumn-207 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-51 animated bd-animation-43 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-107 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-36 clearfix"></div>
	
		<p class=" bd-textblock-96 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Flexible And Fixed Term Options
CUSTOM_CODE;
?>
</p></div></div>
</div>
	
		<div class=" bd-columnwrapper-209 
 col-md-4
 col-sm-6">
    <div class="bd-layoutcolumn-209 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-52 animated bd-animation-44 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-108 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-38 clearfix"></div>
	
		<p class=" bd-textblock-100 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Options To Transfer Your Existing
CUSTOM_CODE;
?>
</p></div></div>
</div>
	
		<div class=" bd-columnwrapper-211 
 col-md-4
 col-sm-6">
    <div class="bd-layoutcolumn-211 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-53 animated bd-animation-45 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-109 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-40 clearfix"></div>
	
		<p class=" bd-textblock-104 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
ISAs Tailored To You Personally
CUSTOM_CODE;
?>
</p></div></div>
</div>
	
		<div class=" bd-columnwrapper-213 
 col-md-4
 col-sm-6">
    <div class="bd-layoutcolumn-213 bd-column" ><div class="bd-vertical-align-wrapper"><a class="bd-iconlink-54 animated bd-animation-46 bd-own-margins bd-iconlink " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false" href="https://isafinder.co.uk/compare-now">
    <span class="bd-icon-110 bd-icon "></span>
</a>
	
		<div class=" bd-spacer-42 clearfix"></div>
	
		<p class=" bd-textblock-108 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Get A Better Return On Your ISA
CUSTOM_CODE;
?>
</p></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		<div class=" bd-layoutcontainer-59 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-226 
 col-sm-12">
    <div class="bd-layoutcolumn-226 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-79', '11_80');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-79', ' bd-sidebar-80 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-29 bd-tagstyles" id="section18" data-section-title="Bank Logos">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-41 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-115 
 col-md-2
 col-xs-12">
    <div class="bd-layoutcolumn-115 bd-no-margins bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-23', '10_58');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-23', ' bd-sidebar-58 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-114 
 col-md-2
 col-xs-12">
    <div class="bd-layoutcolumn-114 bd-no-margins bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-27', '25_56');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-27', ' bd-sidebar-56 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-113 
 col-md-2
 col-xs-12">
    <div class="bd-layoutcolumn-113 bd-no-margins bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-31', '26_54');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-31', ' bd-sidebar-54 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-30 
 col-md-2
 col-xs-12">
    <div class="bd-layoutcolumn-30 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-35', '27_52');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-35', ' bd-sidebar-52 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-28 
 col-md-2
 col-xs-12">
    <div class="bd-layoutcolumn-28 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-39', '28_50');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-39', ' bd-sidebar-50 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-25 
 col-md-2
 col-xs-12">
    <div class="bd-layoutcolumn-25 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-43', '29_46');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-43', ' bd-sidebar-46 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-7 bd-tagstyles" id="section7" data-section-title="2 Columns">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-10 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-22 
 col-sm-12">
    <div class="bd-layoutcolumn-22 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-3', '1_85');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-3', ' bd-sidebar-85 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<footer class=" bd-footerarea-1">
    <?php if (theme_get_option('theme_override_default_footer_content')): ?>
        <?php echo do_shortcode(theme_get_option('theme_footer_content')); ?>
    <?php else: ?>
        <section class=" bd-section-2 bd-page-width bd-tagstyles " id="section2" data-section-title="">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-28 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-62 
 col-xs-12">
    <div class="bd-layoutcolumn-62 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer1", 'footer_2_3');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer1', ' bd-footerwidgetarea-3 clearfix', '');
?>
	
		<div class=" bd-customcmscode-2 bd-tagstyles">
<?php include get_template_directory() . "/fragments/code-2.php"; ?>
</div></div></div>
</div>
	
		<div class=" bd-columnwrapper-63 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-63 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer2", 'footer_8_4');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer2', ' bd-footerwidgetarea-4 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-64 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-64 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer3", 'footer_6_6');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer3', ' bd-footerwidgetarea-6 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-65 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-65 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer4", 'footer_7_8');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer4', ' bd-footerwidgetarea-8 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		
    </div>
</section>
    <?php endif; ?>
</footer>
	
		<div data-smooth-scroll data-animation-time="250" class=" bd-smoothscroll-4"><a href="#" class=" bd-backtotop-2 ">
    <span class="bd-icon-87 bd-icon "></span>
</a></div>
<div id="wp-footer">
    <?php wp_footer(); ?>
    <!-- <?php printf(__('%d queries. %s seconds.', 'default'), get_num_queries(), timer_stop(0, 3)); ?> -->
</div>
</body>
<?php echo apply_filters('theme_body', ob_get_clean()); // body end ?>
</html>