<?php
theme_register_sidebar('Area-67',  __('Home - ISA Examples Text Widget Area', 'default'));

function theme_block_20_84($title = '', $content = '', $class = '', $id = ''){
    ob_start();
?>
    <div class=" bd-block-20 bd-own-margins <?php echo $class; ?>" id="<?php echo $id; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class=" bd-blockheader bd-tagstyles">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class=" bd-blockcontent bd-tagstyles <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
    return ob_get_clean();
}
?>