<?php
/*
Template Name: Home 2
*/
$GLOBALS['theme_current_template_info'] = array('name' => 'home-2');
?>
<?php if (!defined('ABSPATH')) exit; // Exit if accessed directly
?>
<!DOCTYPE html>
<html <?php echo !is_rtl() ? 'dir="ltr" ' : ''; language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>" />
    
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <script>
    var themeHasJQuery = !!window.jQuery;
</script>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/jquery.js?ver=' . wp_get_theme()->get('Version'); ?>"></script>
<script>
    window._$ = jQuery.noConflict(themeHasJQuery);
</script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.css' ?>" />
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.js' ?>"></script>
<![endif]-->
<link class="" href='//fonts.googleapis.com/css?family=Raleway:100,200,300,regular,500,600,700,800,900&subset=latin' rel='stylesheet' type='text/css'>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.core.js' ?>"></script>
<script src="<?php echo get_bloginfo('template_url', 'display'); ?>/CloudZoom.js?ver=<?php echo wp_get_theme()->get('Version'); ?>" type="text/javascript"></script>
    
    <?php wp_head(); ?>
    
</head>
<?php do_action('theme_after_head'); ?>
<?php ob_start(); // body start ?>
<body <?php body_class(' hfeed bootstrap bd-body-11 
 bd-homepage bd-pagebackground bd-margins'); /*   */ ?>>
<header class=" bd-headerarea-1 bd-margins">
        <section class=" bd-section-3 hidden-md hidden-sm hidden-xs hidden-lg bd-tagstyles" id="section3" data-section-title="Menu">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutbox-3 bd-no-margins clearfix">
    <div class="bd-container-inner">
        <?php
    if (theme_get_option('theme_use_default_menu')) {
        wp_nav_menu( array('theme_location' => 'primary-menu-1') );
    } else {
        theme_hmenu_1();
    }
?>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-5 bd-tagstyles" id="section5" data-section-title="Header">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-8 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-7 
 col-lg-2
 col-xs-12">
    <div class="bd-layoutcolumn-7 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-2', '16_13');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-2', ' bd-sidebar-13 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-18 
 col-lg-10
 col-xs-12">
    <div class="bd-layoutcolumn-18 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-4', '9_15');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-4', ' bd-sidebar-15 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
</header>

<div class=" bd-stretchtobottom-2 bd-stretch-to-bottom" data-control-selector=".bd-contentlayout-7">
    <div class="bd-contentlayout-7 bd-page-width   bd-sheetstyles-2   bd-no-margins bd-margins">
        <div class="bd-container-inner">
            <div class="bd-flex-vertical bd-stretch-inner bd-no-margins">
                <div class="bd-flex-horizontal bd-flex-wide bd-no-margins">
                   <div class="bd-flex-vertical bd-flex-wide bd-no-margins">
                          <div class=" bd-layoutitemsbox-21 bd-flex-wide bd-margins">
                            <div class=" bd-content-12">
                                <div class=" bd-blog-3">
                                    <div class="bd-container-inner">
                                        <div class=" bd-grid-6 bd-margins">
                                        <div class="container-fluid">
                                    <div class="separated-grid row">
                        
                        <div class="separated-item-21 col-md-12 first-col last-row last-col" style="overflow: visible; height: auto; min-height: 942px;">
                        
                            <div class="bd-griditem-21">
                                <article id="post-2" class=" bd-article-4 clearfix hentry post-2 page type-page status-publish hentry" style="position: relative;">
        
            
<div class=" bd-postcontent-3 bd-tagstyles  entry-content bd-contentlayout-offset">
 <div class="contact-container">
    <?php echo do_shortcode('[contact-form-7 id="86" title="Lead Capture Form"]'); ?>

        </div>
</div>                                                            </div>
                        </div>
                </div>
            </div>
        </div>
        
        </div>
    </div>
</div>
</div>

                    
                </div>
                
            </div>
            
        </div>

    </div>
</div>
</div>

	
		<footer class=" bd-footerarea-1">
    <?php if (theme_get_option('theme_override_default_footer_content')): ?>
        <?php echo do_shortcode(theme_get_option('theme_footer_content')); ?>
    <?php else: ?>
        <section class=" bd-section-2 bd-page-width bd-tagstyles " id="section2" data-section-title="">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-28 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-62 
 col-xs-12">
    <div class="bd-layoutcolumn-62 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer1", 'footer_2_3');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer1', ' bd-footerwidgetarea-3 clearfix', '');
?>
	
		<div class=" bd-customcmscode-2 bd-tagstyles">
<?php include get_template_directory() . "/fragments/code-2.php"; ?>
</div></div></div>
</div>
	
		<div class=" bd-columnwrapper-63 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-63 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer2", 'footer_8_4');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer2', ' bd-footerwidgetarea-4 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-64 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-64 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer3", 'footer_6_6');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer3', ' bd-footerwidgetarea-6 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-65 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-65 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer4", 'footer_7_8');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer4', ' bd-footerwidgetarea-8 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		
    </div>
</section>
    <?php endif; ?>
</footer>
	
		<div data-smooth-scroll data-animation-time="250" class=" bd-smoothscroll-4"><a href="#" class=" bd-backtotop-2 ">
    <span class="bd-icon-87 bd-icon "></span>
</a></div>
<div id="wp-footer">
    <?php wp_footer(); ?>
    <!-- <?php printf(__('%d queries. %s seconds.', 'default'), get_num_queries(), timer_stop(0, 3)); ?> -->
</div>
</body>
<?php echo apply_filters('theme_body', ob_get_clean()); // body end ?>
</html>