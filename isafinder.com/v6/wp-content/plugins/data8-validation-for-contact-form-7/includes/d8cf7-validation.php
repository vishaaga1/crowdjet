<?php

if (get_option('d8cf7_telephone_validation')) {
	add_filter( 'wpcf7_validate_tel', 'd8_validate_tel', 10, 2 );
	add_filter( 'wpcf7_validate_tel*', 'd8_validate_tel', 10, 2 );
}

function d8_validate_tel($result, $tag) {
	$tag = new WPCF7_FormTag( $tag );
	$name = $tag->name;

	$value = isset( $_POST[$name] )
		? trim( strtr( (string) $_POST[$name], "\n", " " ) )
		: '';

	$defaultCountry = $tag->get_option('country', '', true);

	if(!empty($defaultCountry))
		$defaultCountry = $defaultCountry.trim();

	if ($defaultCountry == ''){
		if(get_option('d8cf7_telephone_default_country'))
			$defaultCountry = get_option('d8cf7_telephone_default_country');
		else
			$defaultCountry = "44";
	}

	$line = $tag->get_option( 'landline', '(true|false)', true);
	if ($line == ''){
		if(get_option('d8cf7_telephone_landline_validation'))
			$line = get_option('d8cf7_telephone_landline_validation');
		else
			$line = 'false';
	}

	$mobile = $tag->get_option( 'mobile', '(true|false)', true);
	if ($mobile == ''){
		if(get_option('d8cf7_telephone_mobile_validation'))
			$mobile = get_option('d8cf7_telephone_mobile_validation');
		else
			$mobile = 'false';
	}

	// Get the allowed prefixes
	$allowedPrefixesInput = $tag->get_option( 'allowedPrefixes', '', true );
	$allowedPrefixArray = [];
	if ($allowedPrefixesInput != '') 
		$allowedPrefixArray = explode("_", $allowedPrefixesInput);
	
	$allowedPrefixes = '';
	foreach ($allowedPrefixArray as &$prefix) {
		$allowedPrefixes = ($allowedPrefixes == '' ? $prefix : $allowedPrefixes.','.$prefix);
	}

	// Get the barred prefixes
	$barredPrefixesInput = $tag->get_option( 'barredPrefixes', '', true );
	$barredPrefixArray = [];
	if ($barredPrefixesInput != '') 
		$barredPrefixArray = explode("_", $barredPrefixesInput);
	
	$barredPrefixes = '';
	foreach ($barredPrefixArray as &$prefix) {
		$barredPrefixes = ($barredPrefixes == '' ? $prefix : $barredPrefixes.','.$prefix);
	}

	if ( $tag->is_required() && '' == $value ) 
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	elseif ( '' != $value) {
		$d8cf7_ajax_key = get_option('d8cf7_ajax_key');

		if(get_option('d8cf7_ajax_key') == "")
			return $result;

		// Set up the parameters for the web service call:
		// https://www.data-8.co.uk/support/service-documentation/international-telephone-validation-part-of-the-phone-validation-suite/reference/isvalid
		$params = array(
			'telephoneNumber' => $value,
			'defaultCountry' => $defaultCountry,
			'options' => array (
				'UseMobileValidation' => $mobile,
				'UseLineValidation' => $line,
				'AllowedPrefixes' => $allowedPrefixes,
				'BarredPrefixes' => $barredPrefixes,
				'ApplicationName' => 'WordPress'
			)
		);

		$url = "https://webservices.data-8.co.uk/InternationalTelephoneValidation/IsValid.json?key=" . $d8cf7_ajax_key;
		$wsresult = ajaxAsyncRequest($url, $params);

		if ($wsresult['Status']['Success'] && ($wsresult['Result']['ValidationResult'] == 'Invalid' || $wsresult['Result']['ValidationResult'] == 'Blank'))
			$result->invalidate( $tag, wpcf7_get_message( 'invalid_tel' ) );	
	}
	return $result;
}

if (get_option('d8cf7_email_validation_level') && get_option('d8cf7_email_validation_level') !== "None") {
	add_filter( 'wpcf7_validate_email', 'd8_validate_email', 10, 2 );
	add_filter( 'wpcf7_validate_email*', 'd8_validate_email', 10, 2 );
}

function d8_validate_email($result, $tag) {
	$tag = new WPCF7_FormTag( $tag );
	$name = $tag->name;

	$value = isset( $_POST[$name] )
		? trim( strtr( (string) $_POST[$name], "\n", " " ) )
		: '';

	$level = $tag->get_option( 'level', '(Syntax|MX|Server|Address)', true);

	if($level == "None"){ return $result; }
	if ($level == ''){
		if(get_option('d8cf7_email_validation_level'))
			$level = get_option('d8cf7_email_validation_level');
		else
			$level = "MX";
	}

	if ( $tag->is_required() && '' == $value ) 
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	elseif ( '' != $value) {
		$d8cf7_ajax_key = get_option('d8cf7_ajax_key');
		
		if(isset($d8cf7_ajax_key))
		{
			// Set up the parameters for the web service call:
			// https://www.data-8.co.uk/support/service-documentation/email-validation/reference/isvalid
			$params = array(
				'email' => $value,
				'level' => $level,
				'options' => array (
					'ApplicationName' => 'WordPress'
				)
			);
			$url = "https://webservices.data-8.co.uk/EmailValidation/IsValid.json?key=" . $d8cf7_ajax_key;
			$wsresult = ajaxAsyncRequest($url, $params);

			if ($wsresult['Status']['Success'] && $wsresult['Result'] == 'Invalid')
				$result->invalidate( $tag, wpcf7_get_message( 'invalid_email' ) );
		}
	}
	return $result;
}

if (get_option('d8cf7_salaciousName')) {
	add_filter( 'wpcf7_validate_text', 'd8_validate_name', 10, 2 );
	add_filter( 'wpcf7_validate_text*', 'd8_validate_name', 10, 2 );
}

function d8_validate_name ($result, $tag) {
	$tag = new WPCF7_FormTag( $tag );
	$nameType = $tag->get_option( 'name_type', '', true );

	if($nameType == 'FullName'){
		$name = $tag->name;
	
		$value = isset( $_POST[$name] )
			? trim( strtr( (string) $_POST[$name], "\n", " " ) )
			: '';
			
		
		if ( $tag->is_required() && '' == $value ) 
			$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
		elseif ( '' != $value) {
			$d8cf7_ajax_key = get_option('d8cf7_ajax_key');
	
			if(get_option('d8cf7_ajax_key') == "")
				return $result;
	
			// Set up the parameters for the web service call:
			// https://www.data-8.co.uk/support/service-documentation/email-validation/reference/isvalid
			$params = array(
				'name' => array(
					'title' => null,
					'forename' => $value,
					'middlename' => null,
					'surname' => null,
				),
				'options' => array (
					'ApplicationName'=> 'WordPress'
				)
			);
			$url = "https://webservices.data-8.co.uk/SalaciousName/IsUnusableName.json?key=" . $d8cf7_ajax_key;
			$wsresult = ajaxAsyncRequest($url, $params);
	
			if ($wsresult['Status']['Success'] && $wsresult['Result'] != '')
				$result->invalidate( $tag, "Check your name" );	
		}
	}
    return $result;
}
?>