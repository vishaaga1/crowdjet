<!-- 
title blue = #005b94 
heading blue = #4b8db4
logo orange = #f1511b
website orange = #ff5416
 -->

<form method="post" action="options.php">
<?php settings_fields( 'd8cf7-settings-group' );?>
<style>
	.switch {
		position: relative;
		display: inline-block;
		width: 60px;
		height: 34px;
	}

	.switch input { 
		opacity: 0;
		width: 0;
		height: 0;
	}

	.slider {
		position: absolute;
		cursor: pointer;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-color: #ccc;
		-webkit-transition: .4s;
		transition: .4s;
	}

	.slider:before {
		position: absolute;
		content: "";
		height: 26px;
		width: 26px;
		left: 4px;
		bottom: 4px;
		background-color: white;
		-webkit-transition: .4s;
		transition: .4s;
	}

	input:checked + .slider {
		background-color: #4b8db4;
	}

	input:focus + .slider {
		box-shadow: 0 0 1px #4b8db4;
	}

	input:checked + .slider:before {
		-webkit-transform: translateX(26px);
		-ms-transform: translateX(26px);
		transform: translateX(26px);
		.phoneOption{
			visibility: visible;
		}
	}

	/* Rounded sliders */
	.slider.round {
		border-radius: 34px;
	}

	.slider.round:before {
		border-radius: 50%;
	}
</style>
<table class="wp-list-table widefat fixed bookmarks">
    <thead>
        <tr style="background-color:#005b94; padding:10px;">
            <th style="font-size:18px; color:white;"><strong>Settings</strong></th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <table>
				<tr>
					<td>
						This plugin uses the Data8 Email Validation, International Telephone Validation, Unusable Name Validation and PredictiveAddress&trade;
						services to ensure you are capturing valid contact details in Woocommerce, Gravity Forms and Contact Form 7
						and improve the user experience. This requires credits for the relevant services. 
						</td>
				</tr>
				<tr>
					<td>
						Please enter your Data8 API key below. <strong>Haven't got a Data8 account yet? <a href="https://www.data-8.co.uk/ValidationAdmin/FreeTrial/ContactForm7" target="_blank">Get a free trial now</a></strong>
					</td>
				</tr>
			</table>
			<table class="form-table d8cf7_form" style="width:100%;">
                <tr valign="top">
                    <td style="padding:10px;"><strong><span style="color:red;">*</span> Data8 AJAX API Key</strong></td>
                    <td style="text-align:center; padding:10px;">
                        <input type="text" name="d8cf7_ajax_key" style="width:150px; height:34px;" value="<?php echo esc_attr(get_option('d8cf7_ajax_key')); ?>" />
                    </td>
                </tr>	
		
				<?php
				if (get_option('d8cf7_error')) {
				?>
				<tr>
					<td class="notice notice-error">
						<?php echo esc_attr(get_option('d8cf7_error')); ?>
					</td>
				</tr>
				<?php } ?>
				<tr><td></td></tr>
				<tr style="background-color:#4b8db4; height:40px;">
					<td style="font-size:16px; color:white;"><strong>Address Capture Services</strong></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><strong>Use Data8 <a href="https://www.data-8.co.uk/services/data-validation/validation-services/address-validation" target="_blank">PredictiveAddress&trade;</a></strong></td>
					<td style="text-align:center;">
						<label class="switch">
							<?php if(get_option('d8cf7_predictiveaddress')){ ?>
								<input type="checkbox" name="d8cf7_predictiveaddress" value="true" checked="checked"/> 
							<?php }else { ?>
								<input type="checkbox" name="d8cf7_predictiveaddress" value="true"/> 
							<?php } ?>
							<span class="slider round"></span>
						</label>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr style="background-color:#4b8db4;">
					<td style="font-size:16px; color:white;"><strong>Validation Services</strong></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td style="width:35%; padding:10px;"><strong>Data8 <a href="https://www.data-8.co.uk/services/data-validation/validation-services/email-validation" target="_blank">Email Validation</a> level</strong></td>
					<td style="width:15%; padding:10px; text-align:center;">
						<select name="d8cf7_email_validation_level" style="height:34px;">
						<?php if(get_option('d8cf7_email_validation_level') === "None" || null === (get_option('d8cf7_email_validation_level'))){ ?>
							<option name="None" value="None" selected>None</option>
						<?php }else { ?>
							<option name="None" value="None">None</option>
						<?php }
						if(get_option('d8cf7_email_validation_level') === "Syntax"){ ?>
							<option name="Syntax" value="Syntax" selected>Syntax (Lowest)</option>
						<?php }else { ?>
							<option name="Syntax" value="Syntax">Syntax (Lowest)</option>
						<?php }
						 if(get_option('d8cf7_email_validation_level') === "MX"){ ?>
							<option name="MX" value="MX" selected>Domain</option>
						<?php }else { ?>
							<option name="MX" value="MX">Domain</option>
						<?php }
						 if(get_option('d8cf7_email_validation_level') === "Server"){ ?>
							<option name="Server" value="Server" selected>Server</option>
						<?php }else { ?>
							<option name="Server" value="Server">Server</option>
						<?php }
						 if(get_option('d8cf7_email_validation_level') === "Address"){ ?>
							<option name="Address" value="Address" selected>Address (Highest)</option>
						<?php }else { ?>
							<option name="Address" value="Address">Address (Highest)</option>
						<?php } ?>
						</select>
					</td>	
					<?php if(get_option('d8cf7_email_validation_level') === "Syntax"){ ?>
						<td style="text-align:left;">The supplied email is checked to ensure that it meets the standard email address format.</td>
					<?php }else if(get_option('d8cf7_email_validation_level') === "MX"){ ?>
						<td style="text-align:left;">The supplied email is checked to ensure that the domain name (the part to the right of the @ sign) exists and is set up to receive email.</td>
					<?php }else if(get_option('d8cf7_email_validation_level') === "Server"){ ?>
						<td style="text-align:left;">In addition to the Domain level checks, Server level validation ensures that at least one of the mail servers advertised for the domain is actually live.</td>
					<?php }else if(get_option('d8cf7_email_validation_level') === "Address"){ ?>
						<td style="text-align:left;">In addition to the Server level checks, Address level validation ensures that the mail server accepts mail for the full email address.</td>
					<?php } else { ?>
						<td style="text-align:left;">No Data8 email validation will occur on your site</td>
					<?php } ?>
				</tr>
				<tr>
					<td><strong>Use Data8 <a href="https://www.data-8.co.uk/services/data-validation/validation-services/phone-validation" target="_blank">International Telephone Number Validation</a></strong></td>
					<td style="text-align:center;">
						<label class="switch">
							<?php if(get_option('d8cf7_telephone_validation')){ ?>
								<input type="checkbox" name="d8cf7_telephone_validation" id="d8cf7_telephone_validation" value="true" checked="checked"/> 
							<?php }else { ?>
								<input type="checkbox" name="d8cf7_telephone_validation" id="d8cf7_telephone_validation" value="true"/> 
							<?php } ?>
							<span class="slider round"></span>
						</label>
					</td>
				</tr>
				<tr>
					<td colspan="3" id="d8cf7_telephone_information">If enabled, UK Landline or Mobile validation will be used instead of International Telephone Validation if a landline or mobile number is detected.</td>
				</tr>
				<tr>
					<td style="padding-left:40px;"><strong>Use Data8 UK Landline Validation</strong></td>
					<td style="text-align:center;">
						<label class="switch">
							<?php if(get_option('d8cf7_telephone_landline_validation')){ ?>
								<input type="checkbox" name="d8cf7_telephone_landline_validation" id="d8cf7_telephone_landline_validation" value="true" checked="checked"/> 
							<?php }else { ?>
								<input type="checkbox" name="d8cf7_telephone_landline_validation" id="d8cf7_telephone_landline_validation" value="true"/> 
							<?php } ?>
							<span class="slider round"></span>
						</label>
					</td>
				</tr>
				<tr>				
				 	<td style="padding-left:40px;"><strong>Use Data8 Mobile Validation</strong></td>
					<td style="text-align:center;">
						<label class="switch">
							<?php if(get_option('d8cf7_telephone_mobile_validation')){ ?>
								<input type="checkbox" name="d8cf7_telephone_mobile_validation" id="d8cf7_telephone_mobile_validation" value="true" checked="checked"/> 
							<?php }else { ?>
								<input type="checkbox" name="d8cf7_telephone_mobile_validation" id="d8cf7_telephone_mobile_validation" value="true"/> 
							<?php } ?>
							<span class="slider round"></span>
						</label>
					</td>
				</tr>	
				<tr>				
				 	<td style="padding-left:40px;"><strong>Default Country Code</strong></td>
					<td style="text-align:center; padding:10px;">
                        <input type="text" name="d8cf7_telephone_default_country" id="d8cf7_telephone_default_country" style="width:150px; height:34px;" value="<?php echo esc_attr(get_option('d8cf7_telephone_default_country')); ?>" placeholder="44" />
                    </td>
				</tr>
				<tr>
					<td><strong>Use Data8 Unusable Name Validation</strong></td>
					<td style="text-align:center;">
						<label class="switch">
							<?php if(get_option('d8cf7_salaciousName')){ ?>
								<input type="checkbox" name="d8cf7_salaciousName" value="true" checked="checked"/> 
							<?php }else { ?>
								<input type="checkbox" name="d8cf7_salaciousName" value="true"/> 
							<?php } ?>
							<span class="slider round"></span>
						</label>
					</td>
				</tr>	
			</table>
            <br/>
			<br/>
            <p class="submit">
				<input type="submit" name="submit-d8cf7" class="button-primary" value="<?php _e('Save Changes') ?>" />
            </p>
            
        </td>
        
    </tr>
    </tbody>
</table>
<br/>
</form>
<script type="text/javascript">
if(window.jQuery){
    var $ = window.jQuery;

    $(document).ready(function() {
        data8SetupAnimations();
    });
}
else{
    if (document.readyState === "complete")
    data8SetupAnimations();
    else
        document.addEventListener("readystatechange", data8SetupAnimations);
}

function data8SetupAnimations(){
    var _telephoneValidation = document.getElementById("d8cf7_telephone_validation");
    
    _telephoneValidation.addEventListener('click', function (){
        showHidePhoneOptions();
    });

    function showHidePhoneOptions (){
        if($){
			var closestGroupLandline = $('#d8cf7_telephone_landline_validation').closest("tr");
        	var closestGroupMobile = $('#d8cf7_telephone_mobile_validation').closest("tr");
			var closestGroupCountry = $('#d8cf7_telephone_default_country').closest("tr");
			var closestGroupInformation = $("#d8cf7_telephone_information").closest("tr");
		
            if(_telephoneValidation.checked){
                closestGroupLandline.fadeIn("200");
                closestGroupMobile.fadeIn("200");
				closestGroupCountry.fadeIn("200");
				closestGroupInformation.fadeIn("200");
            }
            else {
                closestGroupLandline.fadeOut("200");
                closestGroupMobile.fadeOut("200");
				closestGroupCountry.fadeOut("200");
				closestGroupInformation.fadeOut("200");
            }
        }
        else{
			var closestGroupLandline = document.getElementById('d8cf7_telephone_landline_validation').closest("tr");
        	var closestGroupMobile = document.getElementById('d8cf7_telephone_mobile_validation').closest("tr");
			var closestGroupCountry = document.getElementById('d8cf7_telephone_default_country').closest("tr");
			var closestGroupInformation = document.getElementById("d8cf7_telephone_information").closest("tr");

            if(_telephoneValidation.checked){
                closestGroupLandline.setAttribute('style', 'display:table-row');
                closestGroupMobile.setAttribute('style', 'display:table-row');
				closestGroupCountry.setAttribute('style', 'display:table-row');
				closestGroupInformation.setAttribute('style', 'display:table-row');
            }
            else{
                closestGroupLandline.setAttribute('style', 'display:none;');
                closestGroupMobile.setAttribute('style', 'display:none;');
				closestGroupCountry.setAttribute('style', 'display:none;');
				closestGroupInformation.setAttribute('style', 'display:none;');
            }
        }
    }

    showHidePhoneOptions();
}
</script>