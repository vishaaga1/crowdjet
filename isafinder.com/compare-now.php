
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Compare Now | Compare ISA Rates Offering</title>
  <link rel="icon" href="assets/images/ISAfinder-01.jpg" type="image/jpg" sizes="16x16">
  <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="assets/css/stylesheet.css">
  <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/f435ef88a9.js" crossorigin="anonymous"></script>
    <!-- Conversion Pixel - SALE_CONV - DO NOT MODIFY -->
  <script src="https://secure.adnxs.com/px?id=999577&seg=13439766&t=1" type="text/javascript"></script>
  <!-- End of Conversion Pixel -->
</head>
<body>
  <header class="bg-white border-bottom">
    <div class="container">
      <div class="row m-0 w-100 align-items-center py-3">
        <div class="col-md-5 text-md-left text-center">
          <a href="index.php"><img class="img-logo" src="assets/images/logo-isa-blue.png"></a>
        </div>
        <!-- <div class="col-md-7 align-self-end">
          <p class="text-font mb-0 text-md-right text-center">
            <a class="text-header font-weight-bold text-decoration-none" href="mailto:info@isachecker.com">info@isachecker.com
            </a> | Monday - Friday: 9am - 8pm
          </p>
        </div> -->
        
      </div>
    </div>
  </header>

  <section class="form-section bg-white ">
    <div class="container">
      <div class="row w-100 m-0 align-items-center mt-md-5 mb-md-4 my-3 ">
        <div class="col-md-9 mx-auto text-center">
          <form
            id="dealform"
            class="text-center"
            action="/zoho/form_data.php"
            method="post" data-parsley-validate="">
            <!-- first tab -->
            <div class="tab mt-2  form-group text-left">
              <h4 class="my-3 f-30">Step 1 of 4</h4>
              <label class="">Do you currently have an ISA?</label>
              <div class="row align-items-center justify-content-between mt-5">
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="haveIsa"
                      class="haveIsa opacity-0 position-absolute form-control check-input error-on-button"
                      id="haveIsa"
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      value="Yes" 
                      data-parsley-group="block-0"

                      >
                    <label for="haveIsa" class="d-flex align-items-center" >
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">Yes</p>
                    </label>
                  </div>
                </div>
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="haveIsa"
                      class="haveIsa opacity-0 position-absolute form-control check-input error-on-button"
                      id="haveIsano"
                      value="No" 
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      required=""
                      data-parsley-group="block-0"
                      >
                    <label for="haveIsano" class="d-flex align-items-center">
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">No</p>
                    </label>
                  </div>
                  
                </div>
                <div class="error-box row w-100 mx-0 align-items-center justify-content-center text-danger">
                </div>
                <div class="row w-100 m-0 align-items-center justify-content-md-end d-md-block d-none">
                  <button class="btn nextStep font-weight-bold text-white" type="button" onclick="nextStep(1)">Next</button>
                </div>
                <div class="row w-100 m-0 align-items-center justify-content-center d-block d-md-none">
                  <div class="col text-center mx-auto">
                    <button class="btn nextStep font-weight-bold text-white w-100 mx-0" type="button" onclick="nextStep(1)">Next</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- second tab -->
            <div class="tab mt-2 disp-none form-group text-left">
              <h4 class="my-3 f-30">Step 2 of 4</h4>
              <label class="">How much would you be looking to invest?</label>
              <div class="row align-items-center justify-content-between mt-5">
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="invest-amount"
                      class="invest-amount opacity-0 position-absolute form-control check-input error-on-button"
                      id="invest-amount"
                      value="£500 - £5,000" 
                      required=""
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      data-parsley-group="block-1"
                      >
                    <label for="invest-amount" class="d-flex align-items-center" >
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">£500 - £5,000</p>
                    </label>
                  </div>
                </div>
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="invest-amount"
                      class="invest-amount opacity-0 position-absolute form-control check-input error-on-button"
                      id="invest-amount20"
                      value="£5,000 - £20,000" 
                      required=""
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      data-parsley-group="block-1"
                      >
                    <label for="invest-amount20" class="d-flex align-items-center">
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">£5,000 - £20,000</p>
                    </label>
                  </div>
                </div>
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="invest-amount"
                      class="invest-amount opacity-0 position-absolute form-control check-input error-on-button"
                      id="invest-amount50"
                      value="£20,000 - £50,000" 
                      required=""
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      data-parsley-group="block-1"
                      >
                    <label for="invest-amount50" class="d-flex align-items-center" >
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">£20,000 - £50,000</p>
                    </label>
                  </div>
                </div>
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="invest-amount"
                      class="invest-amount opacity-0 position-absolute form-control check-input error-on-button"
                      id="invest-amountover"
                      value="Over £50,000" 
                      required=""
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      data-parsley-group="block-0"
                      >
                    <label for="invest-amountover" class="d-flex align-items-center">
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">Over £50,000</p>
                    </label>
                  </div>
                </div>
                <div class="error-box row w-100 mx-0 align-items-center justify-content-center text-danger"></div>
                <div class="row w-100 m-0 align-items-center justify-content-between d-md-block d-none">
                  <button class="btn back-btn text-white" type="button" onclick="backStep(-1)">Previous</button>
                  <button class="btn nextStep font-weight-bold text-white" type="button" onclick="nextStep(1)">Next</button>
                </div>
                <div class="row w-100 m-0 align-items-center justify-content-between d-md-none d-block">
                  <div class="col text-center mx-auto">
                    <button class="btn nextStep font-weight-bold text-white w-100 mx-0" type="button" onclick="nextStep(1)">Next</button>
                    <button class="btn back-btn text-white w-100 mx-0" type="button" onclick="backStep(-1)">Previous</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- third tab -->
            <div class="tab mt-2 disp-none form-group text-left">
              <h4 class="my-3 f-30">Step 3 of 4</h4>
              <label class="">Are you looking to transfer an existing ISA?</label>
              <div class="row align-items-center justify-content-between mt-5">
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="existingIsa"
                      class="existingIsa opacity-0 position-absolute form-control check-input error-on-button"
                      id="existingIsa"
                      value="Yes" 
                      required=""
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      data-parsley-group="block-2"
                      >
                    <label for="existingIsa" class="d-flex align-items-center" >
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">Yes</p>
                    </label>
                  </div>
                </div>
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="existingIsa"
                      class="existingIsa opacity-0 position-absolute form-control check-input error-on-button"
                      id="existingIsano"
                      value="No" 
                      required=""
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      data-parsley-group="block-2"
                      >
                    <label for="existingIsano" class="d-flex align-items-center">
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">No</p>
                    </label>
                  </div>
                </div>
                <div class="error-box row w-100 mx-0 align-items-center justify-content-center text-danger"></div>
                <div class="row w-100 m-0 align-items-center justify-content-between d-md-block d-none">
                  <button class="btn back-btn text-white" type="button" onclick="backStep(-1)">Previous</button>
                  <button class="btn nextStep font-weight-bold text-white" type="button" onclick="nextStep(1)">Next</button>
                </div>
                <div class="row w-100 m-0 align-items-center justify-content-between d-md-none d-block">
                  <div class="col text-center mx-auto">
                    <button class="btn nextStep font-weight-bold text-white w-100 mx-0" type="button" onclick="nextStep(1)">Next</button>
                    <button class="btn back-btn text-white w-100 mx-0" type="button" onclick="backStep(-1)">Previous</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- forth tab -->
            <div class="tab mt-2 disp-none form-group text-left">
              <h4 class="my-3 f-30">Step 4 of 4</h4>
              <label class="">Do you want to get paid interest annually or monthly?</label>
              <div class="row align-items-center justify-content-between mt-5">
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="interest-paid-type"
                      class="interest-paid-type opacity-0 position-absolute form-control check-input error-on-button"
                      id="interest-paid-type"
                      value="Annually" 
                      required=""
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      data-parsley-group="block-3"
                      >
                    <label for="interest-paid-type" class="d-flex align-items-center" >
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">Annually</p>
                    </label>
                  </div>
                </div>
                <div class="col-12 text-left">
                  <div class="input-checker-div">
                    <input 
                      type="radio" 
                      name="interest-paid-type"
                      class="interest-paid-type opacity-0 position-absolute form-control check-input error-on-button"
                      id="interest-paid-type-monthly"
                      value="Monthly" 
                      required=""
                      data-parsley-required="true"
                      data-parsley-required-message="Please Select One"
                      data-parsley-group="block-3"
                      >
                    <label for="interest-paid-type-monthly" class="d-flex align-items-center">
                      <i class="fas fa-check check-icon"></i>
                      <p class="m-0 pl-3">Monthly</p>
                    </label>
                  </div>
                </div>
                <div class="error-box row w-100 mx-0 align-items-center justify-content-center text-danger"></div>
                <div class="row w-100 m-0 align-items-center justify-content-between d-md-block d-none">
                  <button class="btn back-btn text-white" type="button" onclick="backStep(-1)">Previous</button>
                  <button class="btn nextStep font-weight-bold text-white" type="button" onclick="nextStep(1)">Next</button>
                </div>
                <div class="row w-100 m-0 align-items-center justify-content-between d-md-none d-block">
                  <div class="col text-center mx-auto">
                    <button class="btn nextStep font-weight-bold text-white w-100 mx-0" type="button" onclick="nextStep(1)">Next</button>
                    <button class="btn back-btn text-white w-100 mx-0" type="button" onclick="backStep(-1)">Previous</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- five step last -->
            <div class="tab mt-2 disp-none form-group text-left">
              <h4 class="my-3 f-30">Simply Enter Your Details To Get The Best ISA Rates Now</h4>
              <div class="row align-items-center justify-content-between mt-5">
                <div class="col-md-6 text-left">
                  <div class="">
                    <input 
                      placeholder="First Name"
                      id="first_name" 
                      name="first_name" 
                      class="first_name pl-lg-5 pl-2 input-checker-div text-secondary form-control error-on-button"
                      data-parsley-required="true"
                      data-parsley-required-message="Please Enter First Name"  
                      data-parsley-group="block-4" 
                      data-parsley-pattern-message="First Name Must Be Alphabetic" data-parsley-pattern="/^[a-zA-Z ]*$/" 
                      type="text"
                      required=""
                      >
                  </div>
                </div>
                <div class="col-md-6 text-left">
                  <div class="">
                    <input 
                      placeholder="Last Name"
                      id="last_name" 
                      name="last_name" 
                      class="last_name pl-lg-5 pl-2 input-checker-div text-secondary form-control error-on-button"
                      data-parsley-required="true"
                      data-parsley-required-message="Please Enter Last Name"  
                      data-parsley-group="block-4" 
                      data-parsley-pattern-message="Last Name Must Be Alphabetic" 
                      data-parsley-pattern="/^[a-zA-Z ]*$/" 
                      type="text"
                      required=""
                      >
                  </div>
                </div>
                <div class="col-md-6 text-left">
                  <div class="">
                    <input 
                      placeholder="Email"
                      id="email" 
                      name="email" 
                      class="email pl-lg-5 pl-2 input-checker-div text-secondary form-control error-on-button"
                      data-parsley-required="true"
                      data-parsley-required-message="Please Enter Valid Email Address"  
                      data-parsley-group="block-4"
                      data-parsley-type="email"
                      data-parsley-validemail
                      type="email" 
                      required
                      >
                  </div>
                </div>
                <div class="col-md-6 text-left">
                  <div class="">
                    <input 
                      placeholder="Mobile Number"
                      id="phone" 
                      name="phone" 
                      class="phone pl-lg-5 pl-2 input-checker-div text-secondary form-control error-on-button"
                      data-parsley-required="true"
                      type="tel" 
                      required>
                  </div>
                    <!--<input -->
                    <!--  placeholder="Phone number"-->
                    <!--  id="phone" -->
                    <!--  name="phone" -->
                    <!--  class="phone pl-lg-5 pl-2 input-checker-div text-secondary form-control error-on-button"-->
                    <!--  data-parsley-required="true"-->
                    <!--  data-parsley-required-message="Please Enter Valid UK Phone Number"  -->
                    <!--  data-parsley-group="block-4"-->
                    <!--  data-parsley-type="number"-->
                    <!--  data-parsley-validphone-->
                    <!--  data-parsley-minlength-message="Phone Number Should Have Minimum 10 Digits"-->
                    <!--  data-parsley-minlength='10'-->
                    <!--  minlength='10'-->
                    <!--  data-parsley-maxlength='11'-->
                    <!--  data-parsley-maxlength-message="Phone Number Should Have Maximum 11 Digits"-->
                    <!--  type="tel" -->
                    <!--  required>-->
                  </div>
                </div>
                <div class="custom-control custom-checkbox mx-md-3 d-flex">
                  <input 
                  type="checkbox" 
                  class="custom-control-input error-on-button" 
                  id="customcheck"
                  data-parsley-group="block-4"
                  data-parsley-required="true"
                  data-parsley-required-message="Please Check Opt-In To Proceed">
                  <label class="custom-control-label fs-14 mb-0" for="customcheck">By checking this box you confirm you understand the IsaFinder privacy policy and opt-in that you may be contacted.</label>
                </div>
                <div class="row w-100 mx-0 align-items-center text-danger">
                  <div class="col-12 mx-auto text-center text-danger error-box">
                  </div>
                </div>

                <input type="hidden" name="lid" id="clickid" 
                value="<?php echo $_GET['lid'];?>">
                <input type="hidden" name="aid" id="aid" 
                value="<?php echo $_GET['aid'];?>">
                
<div class="row w-100 mx-0 align-items-center">
       <div class="col-12 mx-auto text-center">
          <div id="Invalid_error"></div>
        </div>
</div>
                <div class="row w-100 m-0 align-items-center justify-content-between d-none d-md-block">
                  <button class="btn back-btn text-white" type="button" onclick="backStep(-1)">Back</button>
                  <input type="submit" value="Get Your Quotes" id="btn-submit" class="get_your_quotes font-weight-bold text-white">
                </div>
                <div class="row w-100 m-0 align-items-center justify-content-between d-md-none d-block">
                  <div class="col text-center mx-auto">
                    <input type="submit" value="Get Your Quotes" id="btn-submit" class="get_your_quotes w-100 mx-0 font-weight-bold text-white">
                    <button class="btn back-btn text-white w-100 mx-0" type="button" onclick="backStep(-1)">Back</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="contact-space">
      <p>&nbsp;</p>
    </div>
  </section>

  <footer class="position-relative">
    <div class="bg-clip-path">
      <div class="container">
        <div class="row w-100 m-0 z-indx position-relative">
          <div class="col">
            <p class="footer-text py-3 border-bottom">
              We do not provide advice to investors and the information on this website should not be construed as such. The information which appears on our website is for information purposes only and does not constitute specific advice. Neither does it constitute a solicitation, offer or recommendation to invest in or dispose of, any investment. If you are in any doubt as to the suitability of an investment, you should seek independent financial advice from a suitable financial advisor
            <br>
              <a href="terms_conditions.php" class="text-white text-decoration-none">Terms And Conditions </a>|
              <a href="privacy_policy.php" class="text-white text-decoration-none" > Privacy Policy</a>
            </p>
            
            <p class="text-center py-2 footer-bottom">© 2020 ISAFinder.com</p>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/parsley.js"></script>
  <script src="assets/js/form.js?v=5.3"></script>
  <script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key= PP27-IHBA-VYEW-9HJZ&load=InternationalTelephoneValidation">
</script>
</body>
</html>