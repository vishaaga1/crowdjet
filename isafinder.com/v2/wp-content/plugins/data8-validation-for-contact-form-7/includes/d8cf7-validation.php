<?php

if (get_option('d8cf7_telephone_validation')) {
	add_filter( 'wpcf7_validate_tel', 'd8_validate_tel', 10, 2 );
	add_filter( 'wpcf7_validate_tel*', 'd8_validate_tel', 10, 2 );
}

function d8_validate_tel($result, $tag) {
	$tag = new WPCF7_Shortcode( $tag );

	$name = $tag->name;

	$value = isset( $_POST[$name] )
		? trim( strtr( (string) $_POST[$name], "\n", " " ) )
		: '';

	$country = $tag->get_option( 'country', '', true );
	$line = $tag->get_option( 'landline', '(true|false)', true );
	$mobile = $tag->get_option( 'mobile', '(true|false)', true );

	// Get the allowed prefixes
	$allowedPrefixesInput = $tag->get_option( 'allowedPrefixes', '', true );
	$allowedPrefixArray = [];
	if ($allowedPrefixesInput != '') {
		$allowedPrefixArray = explode("_", $allowedPrefixesInput);
	}
	$allowedPrefixes = '';
	foreach ($allowedPrefixArray as &$prefix) {
		$allowedPrefixes = ($allowedPrefixes == '' ? $prefix : $allowedPrefixes.','.$prefix);
	}

	// Get the barred prefixes
	$barredPrefixesInput = $tag->get_option( 'barredPrefixes', '', true );
	$barredPrefixArray = [];
	if ($barredPrefixesInput != '') {
		$barredPrefixArray = explode("_", $barredPrefixesInput);
	}
	$barredPrefixes = '';
	foreach ($barredPrefixArray as &$prefix) {
		$barredPrefixes = ($barredPrefixes == '' ? $prefix : $barredPrefixes.','.$prefix);
	}

	if ($country == '')
		$country = '44';
	if ($line == '')
		$line = 'false';
	if ($mobile == '')
		$mobile = 'false';

	if ( $tag->is_required() && '' == $value ) {
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	} elseif ( '' != $value) {
		$d8cf7_username = get_option('d8cf7_username');
		$d8cf7_password = get_option('d8cf7_password');
	
		// Set up the parameters for the web service call:
		// https://www.data-8.co.uk/support/service-documentation/international-telephone-validation-part-of-the-phone-validation-suite/reference/isvalid
		$params = array(
			'username' => $d8cf7_username,
			'password' => $d8cf7_password,
			'telephoneNumber' => $value,
			'defaultCountry' => $country,
			'options' => array ('Option' => array(
				array('Name' => 'UseMobileValidation', 'Value' => $mobile),
				array('Name' => 'UseLineValidation', 'Value' => $line),
				array('Name' => 'AllowedPrefixes', 'Value' => $allowedPrefixes),
				array('Name' => 'BarredPrefixes', 'Value' => $barredPrefixes)
			))
		);
		
		$client = new SoapClient('https://webservices.data-8.co.uk/internationaltelephonevalidation.asmx?WSDL', array('features' => SOAP_SINGLE_ELEMENT_ARRAYS));
		$wsresult = $client->IsValid($params);
		
		if ($wsresult->IsValidResult->Status->Success && $wsresult->IsValidResult->Result->ValidationResult == 'Invalid')
			$result->invalidate( $tag, wpcf7_get_message( 'invalid_tel' ) );
	}

	return $result;
}

if (get_option('d8cf7_email_validation')) {
	add_filter( 'wpcf7_validate_email', 'd8_validate_email', 10, 2 );
	add_filter( 'wpcf7_validate_email*', 'd8_validate_email', 10, 2 );
}

function d8_validate_email($result, $tag) {
	$tag = new WPCF7_Shortcode( $tag );

	$name = $tag->name;

	$value = isset( $_POST[$name] )
		? trim( strtr( (string) $_POST[$name], "\n", " " ) )
		: '';

	$level = $tag->get_option( 'level', '(Syntax|MX|Domain|Address)', true );
	
	if ($level == '')
		$level = 'MX';

	if ( $tag->is_required() && '' == $value ) {
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	} elseif ( '' != $value) {
		$d8cf7_username = get_option('d8cf7_username');
		$d8cf7_password = get_option('d8cf7_password');
		
		// Set up the parameters for the web service call:
		// https://www.data-8.co.uk/support/service-documentation/email-validation/reference/isvalid
		$params = array(
			'username' => $d8cf7_username,
			'password' => $d8cf7_password,
			'email' => $value,
			'level' => $level,
			'options' => null
		);
		
		$client = new SoapClient('https://webservices.data-8.co.uk/emailvalidation.asmx?WSDL', array('features' => SOAP_SINGLE_ELEMENT_ARRAYS));
		$wsresult = $client->IsValid($params);
		
		if ($wsresult->IsValidResult->Status->Success && $wsresult->IsValidResult->Result == 'Invalid')
			$result->invalidate( $tag, wpcf7_get_message( 'invalid_email' ) );
	}

	return $result;
}

if (get_option('d8cf7_salaciousName')) {
	add_filter( 'wpcf7_validate_text', 'd8_validate_name', 10, 2 );
	add_filter( 'wpcf7_validate_text*', 'd8_validate_name', 10, 2 );
}

function d8_validate_name ($result, $tag) {
	$tag = new WPCF7_Shortcode( $tag );

	$name = $tag->name;
	
	$value = isset( $_POST[$name] )
		? trim( strtr( (string) $_POST[$name], "\n", " " ) )
		: '';
		
	$nameType = $tag->get_option( 'name_type', '', true );
	
	if ( $tag->is_required() && '' == $value ) {
		$result->invalidate( $tag, wpcf7_get_message( 'invalid_required' ) );
	} elseif ( '' != $value && $nameType == 'FullName') {
		$d8cf7_username = get_option('d8cf7_username');
		$d8cf7_password = get_option('d8cf7_password');
		
		// Set up the parameters for the web service call:
		// https://www.data-8.co.uk/support/service-documentation/email-validation/reference/isvalid
		$params = array(
			'username' => $d8cf7_username,
			'password' => $d8cf7_password,
			'title' => null,
			'forename' => $value,
			'middlename' => null,
			'surname' => null
		);
		
		$client = new SoapClient('https://webservices.data-8.co.uk/salaciousname.asmx?WSDL', array('features' => SOAP_SINGLE_ELEMENT_ARRAYS));
		$wsresult = $client->IsUnusableNameSimple($params);
		
		if ($wsresult->IsUnusableNameSimpleResult->Status->Success && $wsresult->IsUnusableNameSimpleResult->Result != '')
			$result->invalidate( $tag, "Check your name" );			
	}
	
    return $result;
}

?>