<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ISA FINDER</title>
	<link media="all" rel="stylesheet" href="https://isafinder.com/assets/css/main.css">
	<style type="text/css">
		table {
			font-family: arial, sans-serif;
			border-collapse: collapse;
			width: 80%;
			margin-left: 50px;
		}

		td, th {
			border: 1px solid #dddddd;
			text-align: left;
			padding: 8px;
		}

		tr:nth-child(even) {
			background-color: #dddddd;
		}
		.right-box{
		    float: right;
            width: 60%;
            padding: -4%;
            margin-top: -6%;
            margin-right: 5%;
        }
        .left-box{
            margin-left: 5%;
        }
		}
	
		/* Extra small devices (phones, 600px and down) */
		@media only screen and (max-width: 600px) and (min-width: 300px) {
			table{
				width: 20% !important;
				height: 10% !important;
				margin-left: 0px!important;
				margin-right: 5px!important;
			}
		}

		/* Small devices (portrait tablets and large phones, 600px and up) */
		@media only screen and (min-width: 600px) {
			table{width: 30% !important;}
		}


		/* Medium devices (landscape tablets, 768px and up) */
		@media only screen and (min-width: 768px) {
			table{width: 30% !important;}
		}

		/* Large devices (laptops/desktops, 992px and up) */
		@media only screen and (min-width: 992px) {
			table{width: 30% !important;}
		}

		/* Extra large devices (large laptops and desktops, 1200px and up) */
		@media only screen and (min-width: 1200px) {
			table{width: 80% !important;}
		}
			@media only screen and (min-width: 1000px){
			.right-box{
			    width: 60%;
                margin-top: -6%;
			}
		}
		@media only screen and (max-width: 1000px) and (min-width: 700px) {
			.right-box{
			    width: 45%;
                margin-top: -9%;
			}
		}
		
		@media only screen and (max-width: 700px) {
            .left-box {
                margin-left: 10%;
                text-align: center;
            }
            .right-box{
                    width: unset;
                    margin-top: unset;
                    margin-left: 10%;
            }
		}
	</style>
</head>
<body>
	<div id="wrapper">
		<header class="header">
			<div class="container">
				<strong class="logo"><a href="index.php"><img src="images/isafinder/logo.png" alt="isafinder.com FINd THE BEST ISA FOR YOURSELF TODAY"></a></strong>
			</div>
		</header>
		<main class="main">
			<section >
				<div class="container">
					<center><h1 class="text-center">Terms and conditions of use</h1>
					</center>
					<br>
					<h2>Introduction</h2>
					<div class="bg-white">
						<div class="container">
							These terms and conditions apply between you, the User of this Website (including any sub-domains, unless expressly excluded by their own terms and conditions), and  isafinder.com,  the owner and operator of this Website. Please read these terms and conditions carefully, as they affect your legal rights. Your agreement to comply with and be bound by these terms and conditions is deemed to occur upon your first use of the Website. If you do not agree to be bound by these terms and conditions, you should stop using the Website immediately.
							In these terms and conditions, User or Users means any third party that accesses the Website and is not either (i) employed by  isafinder.com  and acting in the course of their employment or (ii) engaged as a consultant or otherwise providing services to  isafinder.com  and accessing the Website in connection with the provision of such services.
							You must be at least 18 years of age to use this Website. By using the Website and agreeing to these terms and conditions, you represent and warrant that you are at least 18 years of age.
						</div>
					</div>

					<ol type="1">

						<h2>Intellectual property and acceptable use</h2>
						<li>All Content included on the Website, unless uploaded by Users, is the property of  isafinder.com,  our affiliates or other relevant third parties. In these terms and conditions, Content means any text, graphics, images, audio, video, software, data compilations, page layout, underlying code and software and any other form of information capable of being stored in a computer that appears on or forms part of this Website, including any such content uploaded by Users. By continuing to use the Website you acknowledge that such Content is protected by copyright, trademarks, database rights and other intellectual property rights. Nothing on this site shall be construed as granting, by implication, estoppel, or otherwise, any license or right to use any trademark, logo or service mark displayed on the site without the owner's prior written permission
						</li>
						<li>You may, for your own personal, non-commercial use only, do the following:
						</li>
						<li>You must not otherwise reproduce, modify, copy, distribute or use for commercial purposes any Content without the written permission of  isafinder.com. </li>

						<h2>Prohibited use</h2>
						<li>You may not use the Website for any of the following purposes:
							<ol type="a">
								<li>in any way which causes, or may cause, damage to the Website or interferes with any other person's use or enjoyment of the Website;</li>
								<li>in any way which is harmful, unlawful, illegal, abusive, harassing, threatening or otherwise objectionable or in breach of any applicable law, regulation, governmental order;</li>
								<li>making, transmitting or storing electronic copies of Content protected by copyright without the permission of the owner.</li>
							</ol> 
						</li>

						<h2>Registration</h2>
						<li>You must ensure that the details provided by you on registration or at any time are correct and complete.</li>
						<li>You must inform us immediately of any changes to the information that you provide when registering by updating your personal details to ensure we can communicate with you effectively.</li>
						<li>We may suspend or cancel your registration with immediate effect for any reasonable purposes or if you breach these terms and conditions.</li>
						<li>You may cancel your registration at any time by informing us in writing to the address at the end of these terms and conditions. If you do so, you must immediately stop using the Website. Cancellation or suspension of your registration does not affect any statutory rights.</li>
						

						<h2>Availability of the Website and disclaimers</h2>
						
						<li>Any online facilities, tools, services or information that  isafinder.com  makes available through the Website (the Service) is provided "as is" and on an "as available" basis. We give no warranty that the Service will be free of defects and/or faults. To the maximum extent permitted by the law, we provide no warranties (express or implied) of fitness for a particular purpose, accuracy of information, compatibility and satisfactory quality.  isafinder.com  is under no obligation to update information on the Website.</li>
						<li>Whilst  isafinder.com  uses reasonable endeavours to ensure that the Website is secure and free of errors, viruses and other malware, we give no warranty or guaranty in that regard and all Users take responsibility for their own security, that of their personal details and their computers.</li>
						<li>isafinder.com  accepts no liability for any disruption or non-availability of the Website.</li>
						<li>isafinder.com  reserves the right to alter, suspend or discontinue any part (or the whole of) the Website including, but not limited to, any products and/or services available. These terms and conditions shall continue to apply to any modified version of the Website unless it is expressly stated otherwise.</li>


						<h2>Limitation of liability</h2>
						<li>Nothing in these terms and conditions will: (a) limit or exclude our or your liability for death or personal injury resulting from our or your negligence, as applicable; (b) limit or exclude our or your liability for fraud or fraudulent misrepresentation; or (c) limit or exclude any of our or your liabilities in any way that is not permitted under applicable law.</li>
						<li>We will not be liable to you in respect of any losses arising out of events beyond our reasonable control.</li>
						<li>To the maximum extent permitted by law,  isafinder.com  accepts no liability for any of the following:
							<ol type="a">
								<li>any business losses, such as loss of profits, income, revenue, anticipated savings, business, contracts, goodwill or commercial opportunities;</li>
								<li>loss or corruption of any data, database or software;</li>
								<li>any special, indirect or consequential loss or damage.</li>
							</ol>

						</li>


						<h2>General</h2>
						<li>You may not transfer any of your rights under these terms and conditions to any other person. We may transfer our rights under these terms and conditions where we reasonably believe your rights will not be affected.
						</li>
						<li>These terms and conditions may be varied by us from time to time. Such revised terms will apply to the Website from the date of publication. Users should check the terms and conditions regularly to ensure familiarity with the then current version.</li>
						<li>These terms and conditions contain the whole agreement between the parties relating to its subject matter and supersede all prior discussions, arrangements or agreements that might have taken place in relation to the terms and conditions.
						</li>
						<li>The Contracts (Rights of Third Parties) Act 1999 shall not apply to these terms and conditions and no third party will have any right to enforce or rely on any provision of these terms and conditions.</li>
						<li>If any court or competent authority finds that any provision of these terms and conditions (or part of any provision) is invalid, illegal or unenforceable, that provision or part-provision will, to the extent required, be deemed to be deleted, and the validity and enforceability of the other provisions of these terms and conditions will not be affected.
						</li>
						<li>Unless otherwise agreed, no delay, act or omission by a party in exercising any right or remedy will be deemed a waiver of that, or any other, right or remedy.</li>
						<li>This Agreement shall be governed by and interpreted according to the law of England and Wales and all disputes arising under the Agreement (including non-contractual disputes or claims) shall be subject to the exclusive jurisdiction of the English and Welsh courts.</li>

						<h2>isafinder.com  details</h2>
						<li>You can contact isafinder.com by email on info@isafinder.com.
						</li>

					</ol>

				</div>
			</section>
		</main>
	  <footer class="footer">
			<div class="container-fluid">
			     <div class="row justify-content-center">
				    <div class="col-xl-9 col-lg-11">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="left-box">
                                <p>
                                    <a href="terms_conditions.php" class="_link">Terms And Conditions</a>  &nbsp;    
                                    <a href="privacy_policy.php" class="_link">Privacy Policy</a> <br />
                                    &copy; 2021 <a href="index.html">ISAFinder.com</a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="right-box">
                                <p>
                                    We do not provide advice to investors and the information on this website should not be construed as such. The information which appears on our website is for information purposes only and does not constitute specific advice. Neither does it constitute a solicitation, offer or recommendation to invest in or dispose of, any investment. If you are in any doubt as to the suitability of an investment, you should seek independent financial advice from a suitable financial advisor.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
			</div>
		</footer>
       
	</div>
</body>
</html>
