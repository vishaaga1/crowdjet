<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Fans Manager</title>
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" media="screen" href="css/bootstrap.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/owl.carousel.min.css">
  <link rel="stylesheet" type="text/css" media="screen" href="css/owl.theme.default.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@400;500;600;700&family=Montserrat:wght@400;500;700;800;900&family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="style.css">
   <!--- intlTelInput CSS for Country Code --->
 <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css">
  <!-- FAVICONS -->
  <link rel="shortcut icon" href="img/favicon/favicon.png" type="image/png">
  <link rel="icon" href="img/favicon/favicon.png" type="image/png">
</head>

<body>
  <header>
      <div class="header-top">
        <div class="container">
          <div class="header-wrap">
             <div class="row">
              <div class="col-lg-3 col-md-3 d-none d-lg-block d-md-block d-none d-sm-none">
                <div class="header-logo text-center">
                  <a href="index.html"><img src="img/logo.png" class="img-fluid" alt="logo" /></a>                
                </div>
              </div>   

            <div class="header-phone d-block d-md-none d-lg-none d-sm-block"> 
                  <a href="#">Free Consultation</a>
                </div>               
              <div class="col-lg-7 col-md-6">
                <div class="mainmenu text-right">
                  <nav class="navbar navbar-expand-md">
                    <div class="container">
                      <a class="navbar-brand d-block d-md-none d-lg-none d-sm-block" href="index.html"><img src="img/logo.png" class="img-fluid" alt="logo" /></a>
                      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span> 
                      </button>
                      <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                          <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="index.html">Home</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">Services </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="#">About </a>
                          </li>   
                          <li class="nav-item">
                            <a class="nav-link" href="#"> FAQ</a>
                          </li> 
                        </ul>
                      </div>
                    </div>
                  </nav>
                </div>
              </div> 
              <div class="col-lg-2 col-md-3 d-none d-lg-block d-md-block d-none d-sm-none">
                <div class="header-phone"> 
                  <a href="#"><span>Free Consultation</span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="header-bottom">
        <img src="img/banner/banner.jpg" class="img-fluid" width="100%" alt="banner">            
            <div class="banner-contents">
              <div class="container">
                <div class="banner-details">
                  <div class="row">
                    <div class="col-md-7 col-lg-6 col-sm-12">
                      <div class="banner-info">
                        <h1>Fans Manager is a full - service OnlyFans management agency that specialises in chat management, promotion and traffic analysis. </h1>
                        <p>Our strategies have helped creators reach the top 1% of OnlyFans. <br> <br> Our nothing to pay upfront approach makes it accessible for both smaller and bigger creators.</p>
                      </div>                     
                    </div>
                    <div class="col-md-5 col-lg-6 col-sm-12">
                      <div class="banner-img text-center">
                        
                      </div>
                    </div>                     
                  </div>                              
                </div>
              </div>
            </div>

            <div class="banner-shape">
              <img src="img/others/banner-shape.png" class="img-fluid" width="100%" alt="banner-shape" />
            </div>



      </div>
  </header>

    <div class="holder">

      <div class="stonger-section text-center heading"> 
        <div class="stonger-heading heading">
          <h3><span class="text-col"> <span class="text-bg-img">Stronger.</span></span> Together.</h3>
        </div>
        <div class="container">
          <div class="stonger-wrap">
            <div class="row">
              <div class="col-md-4">
                <div class="stonger-content">                  
                  <div class="stonger-icon">
                    <img src="img/others/power-icon.png" class="img-fluid over-img" alt="stonger-icon" />
                  </div>
                  <h5>Power to creators</h5>
                  <img src="img/others/strip-img.png" class="img-fluid" alt="strip-img" />
                  <p>We take care of your OnlyFans account, so you can focus on creating content for your fans. Fans Manager makes your experience as a content creator simple and stress free.</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="stonger-content">                  
                  <div class="stonger-icon">
                    <img src="img/others/build-icon.png" class="img-fluid over-img" alt="stonger-icon" />
                  </div>
                  <h5>Building together</h5>
                  <img src="img/others/strip-img.png" class="img-fluid" alt="strip-img" />
                  <p>Connecting to our creators on a personal level is essential to us. We need to know you in order to understand what is best for you. We work together as a team</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="stonger-content">                  
                  <div class="stonger-icon">
                    <img src="img/others/person-icon.png" class="img-fluid over-img" alt="stonger-icon" />
                  </div>
                  <h5>Personalised growth</h5>
                  <img src="img/others/strip-img.png" class="img-fluid" alt="strip-img" />
                  <p>Every creator is unique and requires a distinct approach. Using powerful analytic tools we are able to optimise your social media outlets and reach millions of potential clients</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="our-section">
        <div class="our-heading heading text-center">
          <h3><span class="text-col"> <span class="text-bg-img">OUR</span></span> SERVICES.</h3>
        </div>
        <div class="container">
          <div class="our-wrap">
            <div class="row"> 
              <div class="col-md-6">
                <div class="our-content heading">
                  <div class="our-img text-center">                    
                    <img src="img/others/our-img1.png" class="img-fluid" alt="our-img" />
                  </div>
                  <h4>Viral strategies.</h4>
                  <p>We offer a personalised growth plan to every creator in order to maximise your reach. Our strategies have made creators go viral and amass millions of views on TikTok. Our creators have gained thousands of paid fans from viral TikTok video’s. </p>
                </div>
              </div>
              <div class="col-md-6 co-border">
                <div class="our-content heading">
                  <div class="our-img text-center d-block d-md-none d-lg-none d-sm-block">                    
                    <img src="img/others/our-img2.png" class="img-fluid" alt="our-img" />
                  </div>
                  <h4>Earning 24/7.</h4>
                  <p>Our trained team manages your chat 7 days a week to retain and gain more subscribers. The constant stream of messages can become hard to keep up with for creators. We take care of your chats and have shown to increase income from messages by up to a 100 times. </p>                  
                  <div class="our-img text-center d-none d-lg-block d-md-block d-none d-sm-none">
                    <img src="img/others/our-img2.png" class="img-fluid" alt="our-img" />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="our-shape">
          <img src="img/others/banner-shape.png" class="img-fluid" width="100%" alt="banner-shape" />
        </div>
      </div>


      <div class="faq-section">
        <div class="faq-heading heading  text-center">                    
          <h3><span class="text-col"> <span class="text-bg-img">faq</span></span></h3>
        </div>
        <div class="container">
          <div class="faq-wrap-cont">            
            <div class="row">
              <div class="col-md-5 col-lg-6">
                <div class="faq-img">
                  <img src="img/others/faq-img1.png" class="img-fluid" alt="faq-img" />
                </div>
              </div>
              <div class="col-md-7 col-lg-6">
                <div class="faq-wrap">
                 <div class="accordion" id="accordionExample">
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        When can I get started?
                      </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                      <div class="accordion-body">
                        <p>Right away! Just send us an email by using the contact form below.</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="headingTwo">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">                        What do your services include?
                      </button>
                    </h2>
                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                      <div class="accordion-body">
                        <p>Chat management, copywriting, content scheduling, viral social media strategies, content plans and more! Our services are tailored specifically for you.</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="headingThree">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        I don’t have an OnlyFans account or I have a very small subscriber base, is this service right for me?
                      </button>
                    </h2>
                    <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                      <div class="accordion-body">
                        <p>We are currently only taking on accounts with established accounts. This is due to the high amount of applications we are receiving.</p>
                      </div>
                    </div>
                  </div>
                  <div class="accordion-item">
                    <h2 class="accordion-header" id="headingfour">
                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                        I already have a large following, but I cannot manage it on my own anymore, is this service right for me?
                      </button>
                    </h2>
                    <div id="collapsefour" class="accordion-collapse collapse" aria-labelledby="headingfour" data-bs-parent="#accordionExample">
                      <div class="accordion-body">
                        <p>We have proven ways to increase revenue for top 1% creators who are looking to take their page to the next level. We have teams working 24/7 to make the most out of a large fanbase.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>                
              </div>
            </div>
          </div>
        </div>
      </div>


      <div class="contact-section">
        <div class="container">
          <div class="contact-heading heading text-center">      
            <h3><span class="text-col"> <span class="text-bg-img"> GET IN</span></span> TOUCH</h3> 
            <p class="removable-media">Want to become a top earner or need help managing a large subscriber base?</p>                
          </div>
          <div class="contact-wrap">
              
            <div class="contact-form">
              <form class="removable-media" action="" method="post" autocomplete="off">
              <div class="row">
                <div class="col-md-6">
                  <div id="firstNameDiv" class="form-group">
                    <input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name *" required >
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="lastNameDiv" class="form-group">
                    <input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name *" required >
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="emailDiv" class="form-group">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Email *" required >
                  </div>
                </div>
                <div class="col-md-6">
                  <div id="phoneDiv" class="form-group">
                    <input type="text" name="phone" id="phone" class="form-control" placeholder="WhatsApp" >
                    <input type="hidden" name="country_code" id="country_code" value="44" />
                    <input type="hidden" name="country_name" id="country_name" value="United Kingdom" placeholder="Country">
                    <span id="valid-msg" class="hide">✓ Valid</span>
                    <span id="error-msg" class="hide"></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="telegram" id="telegram" class="form-control" placeholder="Telegram" >
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input type="text" name="tiktok" id="tiktok" class="form-control" placeholder="TikTok" >
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <input type="text" name="onlyfansurl" id="onlyfansurl" class="form-control" placeholder="Your OnlyFans URL" >
                  </div>            
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <textarea class="form-control" name="meassage" id="meassage"  placeholder="Meassage" ></textarea>
                  </div>            
                </div>
                <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>" />
                <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>" />
                <div class="col-md-12">
                  <div class="form-group1 text-center">
                    <button type="submit" id="btn-submit" class="btn">Send</button>
                  </div>
                </div>
               </div>
              </form>
            </div>
            </div>            
          </div>
        </div>
      </div>



    </div>
    
    <footer>
      <div class="footer-section">        
        <div class="footer-shape">
          <img src="img/others/footer-shape.png" class="img-fluid" width="100%" alt="footer-shape" />
        </div>
        <div class="container">
          <div class="footer-wrap">
            <div class="row">
              <div class="col-md-4 col-lg-4 col-sm-12 col-12">
                <div class="footer-logo">
                  <a href="index.html"><img src="img/footer-logo.png" class="img-fluid" alt="footer-logo" /></a>
                </div>
              </div>
              <div class="col-md-8 col-lg-8 col-sm-12 col-12">
                <div class="footer-menu">
                  <ul class="list-unstyled d-flex d-inline-flex">
                    <li><a href="index.html" class="active">Home</li>
                    <li><a href="#">Services</li>                      
                    <li><a href="#">About</li>
                    <li><a href="#">FAQ</li>
                  </ul>

                  <div class="copyrights text-right">
                    <p>Copyright &copy; 2022 Fans Manager | All Rights Reserved</p>                  
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
      </div>
  </footer>

  <!-- JavaScript Libraries -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/owl.carousel.min.js"></script>
  <script type="text/javascript" src="js/main.js"></script>
  <script src="https://kit.fontawesome.com/d547ebcf09.js" crossorigin="anonymous"></script>
  <!-- teleinput for country code js -->
<script src="intlTelInput/js/intlTelInput.min.js"></script>
<script src="intlTelInput/js/utils.js"></script>
<script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=CMEB-LG5E-KAGF-4BG6&load=InternationalTelephoneValidation,EmailValidation"></script>
<script type="text/javascript">
  var validEmail = false;
  var validPhone = false;
  $("#btn-submit").click(function(e) {
      var first_name = $("#first_name").val();
      var last_name = $("#last_name").val();
      var email = $("#email").val();
      var phoneNumber = $("#phone").val();
      var telegram = $("#telegram").val();
      var tiktok = $("#tiktok").val();
      var onlyfansurl = $("#onlyfansurl").val();
      var meassage = $("#meassage").val();
  
      if (first_name.length <= 1) {
          $('#firstNameDiv p').remove('');
          $('#firstNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >First Name is required!</p>');
          return false;
      } else {
          $('#firstNameDiv p').remove('');
      }
      if (last_name.length <= 1) {
          $('#lastNameDiv p').remove('');
          $('#lastNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >Last Name is required!</p>');
          return false;
      } else {
          $('#lastNameDiv p').remove('');
      }
      if (email.length <= 1) {
          $('#emailDiv p').remove('');
          $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Email is required!</p>');
          return false;
      } else {
          $('#emailDiv p').remove('');
      }
      if (phoneNumber.length <= 1) {
          $('#phoneDiv p').remove('');
          $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Phone Number is required!</p>');
          return false;
      } else {
          $('#phoneDiv p').remove('');
      }
    
      if (validEmail == true && validPhone == true) {
          $.ajax({
              type: "POST",
              url: "formToLead.php",
              data: {
                  first_name: $("#first_name").val(),
                  last_name: $("#last_name").val(),
                  email: $("#email").val(),
                  phone: $("#phone").val(),
                  country: $("#country_name").val(),
                  telegram: $("#telegram").val(),
                  tiktok: $("#tiktok").val(),
                  onlyfansurl: $("#onlyfansurl").val(),
                  meassage: $("#meassage").val(),
                  lid: $("#lid").val(),
                  aid: $("#aid").val()
              },
              success: function(result) {
                  console.log(result);
                  if (result == "success") {
                    $("p,form").remove(".removable-media");
                    $(".contact-heading").append('<p>Thank you!</p>');
                  }
              },
              error: function(result) {
                  console.log(result);
                  $("p,form").remove(".removable-media");
                  $(".contact-heading").append('<p>Thank you!</p>');
              }
          });
      }
  });
  
  $(document).ready(function() {
      //country code script
      var input = document.querySelector("#phone"),
          errorMsg = document.querySelector("#error-msg"),
          validMsg = document.querySelector("#valid-msg");
  
      // Here, the index maps to the error code returned from getValidationError
      var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
  
      // Get country code
      var countryData = window.intlTelInputGlobals.getCountryData(),
          country_code = document.querySelector("#country_code");
      // Initialise plugin
      var iti = window.intlTelInput(input, {
          utilsScript: "intlTelInput/js/utils.js",
          initialCountry: 'gb'
      });
      // Set it's initial value
      var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
      // console.log(all_countrycode);
    
      // Listen to the telephone input for changes
      input.addEventListener('countrychange', function(e) {
          var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
          // console.log(selected_countrycode);
          var selected_countryname = country_code.value = iti.getSelectedCountryData().name;
          // console.log(selected_countryname);
          $("#country_code").val(selected_countrycode);
          $("#country_name").val(selected_countryname);
      });
    
      var reset = function() {
          input.classList.remove("error");
          errorMsg.innerHTML = "";
          errorMsg.classList.add("hide");
          validMsg.classList.add("hide");
      };
    
      // On blur: validate
      input.addEventListener('blur', function() {
          reset();
      });
    
      // on keyup / change flag: reset
      input.addEventListener('change', reset);
      input.addEventListener('keyup', reset);
    
      // Validate email
      $("#email").change(function() {
          var email = $("#email").val();
          level = 'Address';
          // alert(email);
          var emailvalidation = new data8.emailvalidation();
          emailvalidation.isvalid(
              email,
              level,
              [
              
              ],
              showIsEmailValid
          );
      });
    
      function showIsEmailValid(result) {
          console.log(result.Result);
          if (result.Result == 'Valid') {
              validEmail = true;
              $('#emailDiv p').remove('');
              $("#btn-submit").attr('disabled', false);
          } else {
            validEmail = false;
            $('#emailDiv p').remove('');
            $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Email Address</p>');
            $("#btn-submit").attr('disabled', true);
          }
      }
    
      $('select#country_code').on('change', function() {
          var country_code = $(this).children("option:selected").text();
          var code = country_code.replace('+', '');
          // console.log(code);
          $('#country_code').val(code);
      })
    
      $("#phone").bind("change", function(e) {
          var telephoneNumber = $("#phone").val();
          if ($("#country_code").val() == 'gb') {
              defaultCountry = 'GB';
              var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
              internationaltelephonevalidation.isvalid(
                  telephoneNumber,
                  defaultCountry,
                  [
                      new data8.option('UseMobileValidation', 'false'),
                      new data8.option('UseLineValidation', 'false'),
                      new data8.option('RequiredCountry', ''),
                      new data8.option('AllowedPrefixes', ''),
                      new data8.option('BarredPrefixes', ''),
                      new data8.option('UseUnavailableStatus', 'true'),
                      new data8.option('UseAmbiguousStatus', 'true'),
                      new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                      new data8.option('ExcludeUnlikelyNumbers', 'false')
                  ],
                  showIsPhoneNumberValid
              );
          } else {
              defaultCountry = 'GB';
              var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
              internationaltelephonevalidation.isvalid(
                  telephoneNumber,
                  $("#country_code").val(),
                  [
                      new data8.option('UseMobileValidation', 'false'),
                      new data8.option('UseLineValidation', 'false'),
                      new data8.option('RequiredCountry', ''),
                      new data8.option('AllowedPrefixes', ''),
                      new data8.option('BarredPrefixes', ''),
                      new data8.option('UseUnavailableStatus', 'true'),
                      new data8.option('UseAmbiguousStatus', 'true'),
                      new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                      new data8.option('ExcludeUnlikelyNumbers', 'false')
                  ],
                  showIsPhoneNumberValid
              );
          }
      });
    
      function showIsPhoneNumberValid(result) {
          console.log(result.Result.ValidationResult);
          if (result.Result.ValidationResult == 'Valid') {
              validPhone = true;
              $('#phoneDiv p').remove('');
              $("#btn-submit").attr('disabled', false);
          } else {
              validPhone = false;
              $('#phoneDiv p').remove('');
              $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Phone Number</p>');
              $("#btn-submit").attr('disabled', true);
          }
      }
  });
</script>
</body>
</html>
