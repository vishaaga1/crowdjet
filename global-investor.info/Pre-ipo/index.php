
<?php
// $url =  (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// // Use parse_url() function to parse the URL  
// // and return an associative array which 
// // contains its various components 
// $url_components = parse_url($url); 
// parse_str($url_components['query'], $params); 
$country_list_file = file_get_contents('country.json');
$country = json_decode($country_list_file);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Global Investor</title>
		<link rel="stylesheet" href="assets/css/main.css">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Open+Sans:wght@300;400;600;700;800&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
	</head>
	<body>
		<header class="header">
			<img src="assets/img/logo.png" alt="">
		</header>
		<main>
			<section class="banner">
				<div class="container col-2">
					<div class="banner-col-1">
						<h1 class="title-1">Pre-IPO Alert!</h1>
						<p>This Rare Opportunity Is Poised For Massive Amazonian-Like Growth …</p>
						<h3>Which Means Early Investors <br> Could See 174,552% Returns In 2 Decades!</h3>
						<button type="button" class="btn" id="btn-submit-top">Learn More</button>
						
					</div>
					<form method="POST" action="form_data.php" id="global_investor_form" class="form form-box" enctype="multipart/form-data">
						<h2>Contact Us for More Information</h2>
			<!-- 	 <input type="text" name="name" id="name" placeholder="Name" required>-->
						<input type="text" name="first_name" id="first_name" placeholder="First Name" required>
						<input type="text" name="last_name" id="last_name" placeholder="Last Name" required>
						<input type="email" name="email" id="email" placeholder="Email" required>
						<input type="text" name="phone" id="phone" placeholder="Phone Number"  required>
						<!-- <input type="text" name="country" id="country" placeholder="County" required> -->
						<div class="autocomplete" style="width:300px;">
    						<input  type="text" name="country" id="country" placeholder="Country" required>
  						</div>
						<div id="Invalid_error"></div>
						<input type="hidden" name="clickID" id="clickID"  value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] :''?>">
						<input type="hidden" name="aid" id="aid"  value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] :''?>">
						<button type="submit" class="btn" id="btn-submit">Learn More</button>
						<p>By submitting your details, you agree to be contacted by one of our partners. You also acknowledge that you have read and understood our <a href="terms_conditions.php" target="_blank">Terms & Conditions </a> and <a href="privacy_policy.php" target="_blank">Privacy Policy </a></p>
					</form>
				</div>
			</section>

			<section class="section-1 section">
				<div class="container">
					<h2 class="title-2">While Amazonian-growth seems like a long shot for any stock in any industry, the opportunity is very realistic for this tiny startup.</h2>
					<p>Because it's positioning itself to be a key organization for an industry that's starting to boom across the globe.  It has a vital resource that every company in this emerging sector needs.  And it will be one of the only organizations that will happily provide it to them.</p>
					<p>The industry is currently set to rapidly expand across the United States of America.  And early investors who are wise enough to invest while the company is still in PRE-IPO phase, will have a chance to accumulate life-changing wealth.</p>
				</div>
			</section>

			<section class="section-2 section">

				<div class="container">

					<h2 class="title-2">We'll get to the company and the industry in just a moment.</h2>



					<div class="col-2 section-cols">

						<div class="col-img">

							<p>

								First, let's review how your financial situation would look if you had gotten in on the ground floor with Amazon. <br>

								The stock would have opened to the public at an affordable $1.73 per share on May 16, 1997 …

							</p>

							<img src="assets/img/graf1.png" alt="">

						</div>

						<div class="col-img">

							<p>

								And in less than 3 years each share would have skyrocketed to $107.13 as seen below …

							</p>

							<img src="assets/img/graf2.png" alt="">

						</div>

					</div>



					<h3 class="title-3">

						That's a 6092% increase!

					</h3>

				</div>

			</section>

			<section class="section section-3">

				<div class="container">

					<h2 class="title-2">An increase that would have made you look like one savvy investor.</h2>



					<p>In fact, you could have pulled your initial investment out of the stock and continued riding the bull run for the next 20 years.</p>



					<p>This means from here on you would be enjoying infinite returns because none of your own money would be at risk in the volatile stock market.</p>



					<p>The novelty of the stock would have cooled off over the next decade as the company worked to establish its place in the market.  And this wouldn't have mattered to you one bit if you were in the opportunity for the long haul.</p>



					<h2 class="title-2 title-short">In fact, once July of 2011 hit your stock would have been worth more than $200 in value …</h2>



					<img src="assets/img/graf3.png" alt="">

				</div>

			</section>
			<section class="section section-4">

				<div class="container">

					<h3 class="title-3">

						That's more than double what it was valued at back in 1999!

					</h3>



					<p>And these spectacular gains would have been peanuts in comparison to what the future was going to hold.</p>



					<p>Because once your investment reaches the present year of 2020, the stock value becomes mind-blowing </p>

					<img src="assets/img/graf4.png" alt="">



					<h4 class="title-4">

						You're seeing that number correctly.

					</h4>



					<p class="text-1">

						Your Amazon stock would be worth $2,986.55 per share. That's a 174,552% increase in value!

					</p>

				</div>

			</section>
			<section class="section section-41">

				<div class="container">

					<div class="col-2">

						<div>

							<p><b>The tiny company that is the subject of this letter has the same type of future growth potential.</b></p>



							<p>It's a company that will be heavily involved in an emerging industry.  An industry that is poised for explosive growth across the globe.  <br>

							America is the next destination where this industry will emerge.</p>



							<p>At present, it is available for restricted use in 33 of the 50 states.</p>



							<p>But the shackles are coming off of this industry in short order.</p>



							<p>In fact, there are 11 states that have removed the heavy restrictions from this industry.  Even conservative states like Pennsylvania are starting to fall in line with acceptance.</p>

						</div>



						<div>

							<img src="assets/img/map.png" alt="">

						</div>

					</div>



					<div class="col-2 col-map">

						<div>

							<img src="assets/img/map2.png" alt="">

						</div>



						<div>

							<p>In the state of Colorado, the emerging industry is already more popular than Starbucks.</p>



							<p>And according to a recent Pew poll, 67% of Americans think that the restrictions need to be lifted.</p>



							<p>But America is only one of several countries with plans to remove restrictions on this industry.  Once the domino falls there, there are at least 16 additional countries that are ready to follow suit.</p>



							<p>According to the World Population Review, the industry is still restricted in most countries around the world.</p>

						</div>



					</div>



					<div>

						<p>When those dominoes fall, the opportunity for companies in the sector will be wide open.</p>



						<p>Grand View Research predicts that the market will be worth $73.6 billion by 2027.  Remember, that's only after 16 countries decide to lift restrictions.</p>



						<p>There are 195 countries across the globe.  It's highly likely that even more of these companies will lift restrictions on the industry as it evolves in the other markets.  That fact alone should give you an idea of the enormous market potential for this industry.</p>



						<p>And at the center of it all, there's a tiny startup that these companies will reach out to when they need to facilitate research and growth.</p>



						<p><b>You'll find out the name of that company in just a moment.</b></p>

					</div>

				</div>

			</section>
			<section class="section section-5">

				<div class="container">

					<h3 class="title-3">First, let's review how your Amazon investment would have performed over the last 2 decades.</h3>



					<div class="col-2 section-cols">

						<div class="col-img">

							<p>

								If you had made a tiny investment of just $1,000 in Amazon at the initial public offering …

							</p>

							<img src="assets/img/graf5.png" alt="">

						</div>

						<div class="col-img">

							<p>

								… that grubstake would have ballooned into $61,921.14 in less than 3 years …

							</p>

							<img src="assets/img/graf6.png" alt="">

						</div>

					</div>



					<h2 class="title-2">But that's just the tip of the iceberg.</h2>



					<div class="col-2 section-cols">

						<div class="col-img">

							<p>

								Let's say things are going well for you financially.  So, you decide to leave your initial grubstake in the investment.

							</p>



							<p>Your investment goes on a bit of a roller coaster over the next decade.  However, it never dips into the red.  You remain profitable through it all.</p>



							<p>Then we hit July of 2011 and the stock price shoots to over $200 per share …</p>

							<img src="assets/img/graf7.png" alt="">

						</div>

						<div class="col-img">

							<p>

								Now your shares are worth $128,616.56 and you're starting to feel like an investing guru.

							</p>



							<p>But to your complete and utter delight, this stock is still at the tip of the iceberg.  Because it's about to go on a bull run unlike anything else you've ever seen.</p>



							<p>It skyrockets to $2,986.55 per share in less than 10 years.</p>



							<img src="assets/img/graf8.png" alt="">

						</div>

					</div>



				</div>

			</section>
			<section class="section section-6">

				<div class="container">

					<h3 class="title-3">Your grubstake investment would be worth $1,726,225.90.</h3>



					<h2 class="title-2">That's right you'd be on your way to being a multi-millionaire!</h2>



					<p>The tiny company that you are about to discover has an opportunity for a bull run similar to this. <br>

					And even if it performs only half as good, the return on your investment would be mind-boggling.</p>



					<p>Here's why this tiny company is so important to this emerging global industry …</p>



					<p>When each of the companies across the world need to expand- which they most certainly will- this tiny company will be ready to lend each of the most promising ones money. </p>



					<p>

						The more loans that this company gets on the books … the more profitable the business will become. <br>

						And Pre-IPO investors will likely benefit from rising stock prices due to rock solid company financials.<br>

						And executives at traditional lending institutions will be watching enviously from the sidelines while things develop.

					</p>	



					<p>Make no mistake!  The tiny company will have a competitor or two.  But the competitors won't have what this particular company has at immediate disposal.  A team of Israeli's who have been at the forefront of this emerging industry.</p>



					<p><b>Discover more about the tiny company at the center of this budding industry today.</b></p>

				</div>

			</section>
			<section class=" section-7">

				<div class="container">

					
					<button type="button" class="btn" id="btn-submit-bottom">Learn More</button>

				</div>

			</section>
			<section class="section section-8">

				<div class="container">

					<p>Disclaimer This content is provided for informational purposes only. Information on this website is not intended to be a solicitation of any kind. Nothing herein shall be construed as financial, tax, legal or accounting advice. Potential clients should consult their legal team, accountant or financial advisors before requesting the introduction of any product. if you are unsure of the terms or potential risk, please contact the necessary expert or professional. Past performance is not a guarantee of future performance or returns.</p>

				</div>

			</section>
		</main>
		<footer class="footer">
			<p>© 2020 All rights reserved.</p>
		</footer>
	</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script type="text/javascript" src="https://webservices.data-8.co.uk/javascript/loader.ashx?key=4Z6P-LMTT-SIB9-C432&load=InternationalTelephoneValidation,EmailValidation"></script>
<script type="text/javascript" src="https://webservices.data-8.co.uk/javascript/jqueryvalidation_min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">
	var countries = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua &amp; Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia &amp; Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre &amp; Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts &amp; Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad &amp; Tobago","Tunisia","Turkey","Turkmenistan","Turks &amp; Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];
	autocomplete(document.getElementById("country"), countries);
	function autocomplete(inp, arr) {
	/*the autocomplete function takes two arguments,
	the text field element and an array of possible autocompleted values:*/
	var currentFocus;
	/*execute a function when someone writes in the text field:*/
	inp.addEventListener("input", function(e) {
		var a, b, i, val = this.value;
		/*close any already open lists of autocompleted values*/
		closeAllLists();
		if (!val) { return false;}
		currentFocus = -1;
		/*create a DIV element that will contain the items (values):*/
		a = document.createElement("DIV");
		a.setAttribute("id", this.id + "autocomplete-list");
		a.setAttribute("class", "autocomplete-items");
		/*append the DIV element as a child of the autocomplete container:*/
		this.parentNode.appendChild(a);
		/*for each item in the array...*/
		for (i = 0; i < arr.length; i++) {
			/*check if the item starts with the same letters as the text field value:*/
			if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
			/*create a DIV element for each matching element:*/
			b = document.createElement("DIV");
			/*make the matching letters bold:*/
			b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
			b.innerHTML += arr[i].substr(val.length);
			/*insert a input field that will hold the current array item's value:*/
			b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
			/*execute a function when someone clicks on the item value (DIV element):*/
				b.addEventListener("click", function(e) {
				/*insert the value for the autocomplete text field:*/
				inp.value = this.getElementsByTagName("input")[0].value;
				/*close the list of autocompleted values,
				(or any other open lists of autocompleted values:*/
				closeAllLists();
			});
			a.appendChild(b);
			}
		}
	});
	/*execute a function presses a key on the keyboard:*/
	inp.addEventListener("keydown", function(e) {
		var x = document.getElementById(this.id + "autocomplete-list");
		if (x) x = x.getElementsByTagName("div");
		if (e.keyCode == 40) {
			/*If the arrow DOWN key is pressed,
			increase the currentFocus variable:*/
			currentFocus++;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 38) { //up
			/*If the arrow UP key is pressed,
			decrease the currentFocus variable:*/
			currentFocus--;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 13) {
			/*If the ENTER key is pressed, prevent the form from being submitted,*/
			e.preventDefault();
			if (currentFocus > -1) {
			/*and simulate a click on the "active" item:*/
			if (x) x[currentFocus].click();
			}
		}
	});
	function addActive(x) {
		/*a function to classify an item as "active":*/
		if (!x) return false;
		/*start by removing the "active" class on all items:*/
		removeActive(x);
		if (currentFocus >= x.length) currentFocus = 0;
		if (currentFocus < 0) currentFocus = (x.length - 1);
		/*add class "autocomplete-active":*/
		x[currentFocus].classList.add("autocomplete-active");
	}
	function removeActive(x) {
		/*a function to remove the "active" class from all autocomplete items:*/
		for (var i = 0; i < x.length; i++) {
		x[i].classList.remove("autocomplete-active");
		}
	}
	function closeAllLists(elmnt) {
		/*close all autocomplete lists in the document,
		except the one passed as an argument:*/
		var x = document.getElementsByClassName("autocomplete-items");
		for (var i = 0; i < x.length; i++) {
		if (elmnt != x[i] && elmnt != inp) {
		x[i].parentNode.removeChild(x[i]);
		}
	}
	}
	/*execute a function when someone clicks in the document:*/
	document.addEventListener("click", function (e) {
		closeAllLists(e.target);
	});
	}
</script>
<!-- Data8 Validation  -->
<script type="text/javascript">
//$("#email").change(function(){ 
$("#email").bind("keyup change", function(e) {
      var email = $("#email").val();
      level='Address';
   
    var emailvalidation = new data8.emailvalidation();
    emailvalidation.isvalid(
      email,
      level,
      [
      ],
      showIsValidResult
      );
  });
  function showIsValidResult(result) {
    console.log(result.Result);
    if(result.Result == 'Valid') {
      $('#Invalid_error').hide();
	   $('#btn-submit').prop('disabled', false);
    }
    else
    {
      $('#Invalid_error').show();
      $('#Invalid_error').html('<p style="color: #dc3545;padding:0px !important; font-size:15px;" >Please Enter Valid Email.</p>');
     $('#btn-submit').prop('disabled', true);
	}
  }
$("#phone").bind("keyup change", function(e) {
  //$("").change(function(){   // 1st
    var telephoneNumber = $("#phone").val();
    defaultCountry='GB';
    var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
      internationaltelephonevalidation.isvalid(
        telephoneNumber ,
        defaultCountry,
        [
          new data8.option('UseMobileValidation', 'false'),
          new data8.option('UseLineValidation', 'false'),
          new data8.option('RequiredCountry', ''),
          new data8.option('AllowedPrefixes', ''),
          new data8.option('BarredPrefixes', ''),
          new data8.option('UseUnavailableStatus', 'true'),
          new data8.option('UseAmbiguousStatus', 'true'),
          new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
          new data8.option('ExcludeUnlikelyNumbers', 'false')
        ],
        showIsValidResults
      );
});

function showIsValidResults(result) {
	console.log(result.Result.ValidationResult);
    if(result.Result.ValidationResult == 'Valid') {
            // isPhone = true
            // return true
            $('#btn-submit').prop('disabled', false);
            $('#Invalid_error').hide();
          }
          else
          {
            $('#Invalid_error').show();
            $('#Invalid_error').html('<p style="color: #dc3545;font-size:15px;" >Please Enter Valid Phone Number</p>');
            $('#btn-submit').prop('disabled', true);
        // return $.Deferred().reject("Please Enter Valid Phone Number");
        }
}
 $("#btn-submit-top").click(function(){        
       // $("#global_investor_form").submit(); // Submit the form
		$("#btn-submit").click();
}); 
 $("#btn-submit-bottom").click(function(){        
       // $("#global_investor_form").submit(); // Submit the form
		$("#btn-submit").click();
}); 
</script>

