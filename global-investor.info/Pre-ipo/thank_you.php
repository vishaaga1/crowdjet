<!DOCTYPE html>

<html lang="en">

<head>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Global Investor</title>

	<link rel="stylesheet" href="assets/css/main.css">

	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&family=Open+Sans:wght@300;400;600;700;800&family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">

</head>
<style>
     .thankyou{ 
		text-align: center !important;
		background-color: #000000 !important;
		margin-top: 20px;
	  }
</style>
<body>

	<header class="header">

		<img src="assets/img/logo.png" alt="">

	</header>

	<main>
	
		<section class="section thankyou">
			<img src="assets/img/Thank_you-removebg-preview.png" alt="">
		</section>

	</main>

	<footer class="footer">

		<p>© 2020 All rights reserved.</p>

	</footer>

</body>

</html>