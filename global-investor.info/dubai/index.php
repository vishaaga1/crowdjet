<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<title>Global Investor | DOWNLOAD YOUR FREE DUBAI PROPERTY INVESTMENT GUIDE
</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="css/stylesheet.css">
<link href="favicon.png" rel="apple-touch-icon" />
<link href="images/favicon.png" rel="icon" type="image/png" />
 <!--- intlTelInput CSS for Country Code --->
 <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css">
<style type="text/css">
    .middle-content .container .row .col-lg-7 .row .col-lg-10 .left-side .check-list {
        font-size: 24px;
    }
</style>
</head>
<body>
<header>
  <div class="container">
  	<div class="row">
    	<div class="col-sm-6 my-auto">
        	<div class="logo">
            	<a href="index.html"><img src="images/logo.png" class="img-fluid" /></a>
            </div>
        </div>
    	<div class="col-sm-6 my-auto">
        	<div class="top-download">
            	<a href="#btn-submit">
                	<img src="images/download-icon.png" class="img-fluid" />
                    DOWNLOAD GUIDE
                </a>
            </div>
        </div>
    </div>
  </div>
</header>
<section class="middle-content">
	<div class="container">
    	<div class="row">
        	<div class="col-lg-7 hiddenContentMobile">
            	<div class="row">
                	<div class="col-lg-10">
                        <div class="left-side">
                          <h1>Did you know Dubai has, worldwide, <br>one of the best-performing economies?</h1>                            
                            <h2>It is easy to see why <span>DUBAI</span> is seen as an <span>attractive option</span> for investors with:</h2>
                            <br>
                            <ul class="check-list">
                                <li><span><i class="fa fa-check" aria-hidden="true"></i></span><strong>   Highly paid jobs</strong></li>
								<li><span><i class="fa fa-check" aria-hidden="true"></i></span><strong>   Tax-free salaries</strong></li>
								<li><span><i class="fa fa-check" aria-hidden="true"></i></span><strong>   A luxury lifestyle</strong></li>
                                <li><span><i class="fa fa-check" aria-hidden="true"></i></span><strong>   Sun throughout the year</strong></li>
						  </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
            	<div class="right-side">
                	<h2>DOWNLOAD YOUR FREE<br /> DUBAI PROPERTY INVESTMENT GUIDE</h2>
                    
                    <form action="" method="post" autocomplete="off">
                        <div class="row contatc-box">
                            <div id="firstNameDiv" class="col-md-6">
                            	<input type="text" name="first_name" id="first_name" placeholder="First Name" required />
                            </div>
                            <div id="lastNameDiv" class="col-md-6">
                            	<input type="text" name="last_name" id="last_name" placeholder="Last Name" required />
                            </div>
                            <div id="emailDiv" class="col-md-12">
                            	<input type="text" name="email" id="email" placeholder="Email Address" class="email" required />
                            </div>
                            <div id="phoneDiv" class="col-md-12">
                            	<input type="text" name="phone" id="phone" placeholder="Telephone Number" class="telephone" required />
                                <input type="hidden" name="country_code" id="country_code" value="44" />
                                <input type="hidden" name="country_name" id="country_name" value="United Kingdom" placeholder="Country">
                                <span id="valid-msg" class="hide">✓ Valid</span>
                                <span id="error-msg" class="hide"></span>
                            </div>
                            <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>" />
                            <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>" />
                            <div class="col-md-12">
                            	<button type="submit" id="btn-submit">
                                	<img src="images/download-icon.png" class="img-fluid" />
                                    DOWNLOAD GUIDE
                                </button>
                            </div>
                            <div class="col-md-12">
                            	<p>
                               	By submitting your details, you agree to be contacted by one of our partners. You also acknowledge that you have read and understood our <a href="http://global-investor.info/Gold/terms_conditions.php" target="_blank">Terms & Conditions</a> </p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-7 hiddenContentDesktop">
            	<div class="row">
                	<div class="col-lg-10">
                        <div class="left-side">
                          <h1>Did you know Dubai has, worldwide, <br>one of the best-performing economies?</h1>                            
                            <h2>It is easy to see why <span>DUBAI</span> is seen as an <span>attractive option</span> for investors with:</h2>
                            <br>
                            <ul class="check-list">
                                <li><span><i class="fa fa-check" aria-hidden="true"></i></span><strong>   Highly paid jobs</strong></li>
								<li><span><i class="fa fa-check" aria-hidden="true"></i></span><strong>   Tax-free salaries</strong></li>
								<li><span><i class="fa fa-check" aria-hidden="true"></i></span><strong>   A luxury lifestyle</strong></li>
                                <li><span><i class="fa fa-check" aria-hidden="true"></i></span><strong>   Sun throughout the year</strong></li>
						  </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- jQuery library --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/function.js"></script>
<!-- teleinput for country code js -->
<script src="intlTelInput/js/intlTelInput.min.js"></script>
<script src="intlTelInput/js/utils.js"></script>
<script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=4Z6P-LMTT-SIB9-C432&load=InternationalTelephoneValidation,EmailValidation"></script>
<script type="text/javascript">
  var validEmail = false;
  var validPhone = false;
  $("#btn-submit").click(function(e) {
      var first_name = $("#first_name").val();
      var last_name = $("#last_name").val();
      var email = $("#email").val();
      var phoneNumber = $("#phone").val();
  
      if (first_name.length <= 1) {
          $('#firstNameDiv p').remove('');
          $('#firstNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >First Name is required!</p>');
          return false;
      } else {
          $('#firstNameDiv p').remove('');
      }
      if (last_name.length <= 1) {
          $('#lastNameDiv p').remove('');
          $('#lastNameDiv').append('<p style="color: #dc3545;font-size: 15px;" >Last Name is required!</p>');
          return false;
      } else {
          $('#lastNameDiv p').remove('');
      }
      if (email.length <= 1) {
          $('#emailDiv p').remove('');
          $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Email is required!</p>');
          return false;
      } else {
          $('#emailDiv p').remove('');
      }
      if (phoneNumber.length <= 1) {
          $('#phoneDiv p').remove('');
          $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Phone Number is required!</p>');
          return false;
      } else {
          $('#phoneDiv p').remove('');
      }
    
      if (validEmail == true && validPhone == true) {
          $.ajax({
              type: "POST",
              url: "formToLead.php",
              data: {
                  first_name: $("#first_name").val(),
                  last_name: $("#last_name").val(),
                  email: $("#email").val(),
                  phone: $("#phone").val(),
                  country: $("#country_name").val(),
                  lid: $("#lid").val(),
                  aid: $("#aid").val()
              },
              success: function(result) {
                  console.log(result);
                  if (result == "success") {
                    location.href = "thankyou.html";
                  }
              },
              error: function(result) {
                  console.log(result);
                  location.href = "thankyou.html";
              }
          });
      }
  });
  
  $(document).ready(function() {
      //country code script
      var input = document.querySelector("#phone"),
          errorMsg = document.querySelector("#error-msg"),
          validMsg = document.querySelector("#valid-msg");
  
      // Here, the index maps to the error code returned from getValidationError
      var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
  
      // Get country code
      var countryData = window.intlTelInputGlobals.getCountryData(),
          country_code = document.querySelector("#country_code");
      // Initialise plugin
      var iti = window.intlTelInput(input, {
          utilsScript: "intlTelInput/js/utils.js",
          initialCountry: 'gb'
      });
      // Set it's initial value
      var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
      // console.log(all_countrycode);
    
      // Listen to the telephone input for changes
      input.addEventListener('countrychange', function(e) {
          var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
          // console.log(selected_countrycode);
          var selected_countryname = country_code.value = iti.getSelectedCountryData().name;
          // console.log(selected_countryname);
          $("#country_code").val(selected_countrycode);
          $("#country_name").val(selected_countryname);
      });
    
      var reset = function() {
          input.classList.remove("error");
          errorMsg.innerHTML = "";
          errorMsg.classList.add("hide");
          validMsg.classList.add("hide");
      };
    
      // On blur: validate
      input.addEventListener('blur', function() {
          reset();
      });
    
      // on keyup / change flag: reset
      input.addEventListener('change', reset);
      input.addEventListener('keyup', reset);
    
      // Validate email
      $("#email").change(function() {
          var email = $("#email").val();
          level = 'Address';
          // alert(email);
          var emailvalidation = new data8.emailvalidation();
          emailvalidation.isvalid(
              email,
              level,
              [
              
              ],
              showIsEmailValid
          );
      });
    
      function showIsEmailValid(result) {
          console.log(result.Result);
          if (result.Result == 'Valid') {
              validEmail = true;
              $('#emailDiv p').remove('');
              $("#btn-submit").attr('disabled', false);
          } else {
            validEmail = false;
            $('#emailDiv p').remove('');
            $('#emailDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Email Address</p>');
            $("#btn-submit").attr('disabled', true);
          }
      }
    
      $('select#country_code').on('change', function() {
          var country_code = $(this).children("option:selected").text();
          var code = country_code.replace('+', '');
          // console.log(code);
          $('#country_code').val(code);
      })
    
      $("#phone").bind("change", function(e) {
          var telephoneNumber = $("#phone").val();
          if ($("#country_code").val() == 'gb') {
              defaultCountry = 'GB';
              var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
              internationaltelephonevalidation.isvalid(
                  telephoneNumber,
                  defaultCountry,
                  [
                      new data8.option('UseMobileValidation', 'false'),
                      new data8.option('UseLineValidation', 'false'),
                      new data8.option('RequiredCountry', ''),
                      new data8.option('AllowedPrefixes', ''),
                      new data8.option('BarredPrefixes', ''),
                      new data8.option('UseUnavailableStatus', 'true'),
                      new data8.option('UseAmbiguousStatus', 'true'),
                      new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                      new data8.option('ExcludeUnlikelyNumbers', 'false')
                  ],
                  showIsPhoneNumberValid
              );
          } else {
              defaultCountry = 'GB';
              var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
              internationaltelephonevalidation.isvalid(
                  telephoneNumber,
                  $("#country_code").val(),
                  [
                      new data8.option('UseMobileValidation', 'false'),
                      new data8.option('UseLineValidation', 'false'),
                      new data8.option('RequiredCountry', ''),
                      new data8.option('AllowedPrefixes', ''),
                      new data8.option('BarredPrefixes', ''),
                      new data8.option('UseUnavailableStatus', 'true'),
                      new data8.option('UseAmbiguousStatus', 'true'),
                      new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
                      new data8.option('ExcludeUnlikelyNumbers', 'false')
                  ],
                  showIsPhoneNumberValid
              );
          }
      });
    
      function showIsPhoneNumberValid(result) {
          console.log(result.Result.ValidationResult);
          if (result.Result.ValidationResult == 'Valid') {
              validPhone = true;
              $('#phoneDiv p').remove('');
              $("#btn-submit").attr('disabled', false);
          } else {
              validPhone = false;
              $('#phoneDiv p').remove('');
              $('#phoneDiv').append('<p style="color: #dc3545;font-size: 15px;" >Please Enter Valid Phone Number</p>');
              $("#btn-submit").attr('disabled', true);
          }
      }
  });
</script>
</body>
</html>
