<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <title>Global Investor | DOWNLOAD YOUR FREE GOLD INVESTMENT GUIDE</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
 <link rel="stylesheet" href="css/stylesheet.css">
 <link href="favicon.png" rel="apple-touch-icon" />
 <link href="images/favicon.png" rel="icon" type="image/png" />
</head>
<body>
  <header>
   <div class="container">
    <div class="row">
     <div class="col-sm-6 my-auto">
      <div class="logo">
       <a href="index.php"><img src="images/logo.png" class="img-fluid" /></a>
     </div>
   </div>
   <div class="col-sm-6 my-auto">
    <div class="top-download">
     <a href="#">
       <img src="images/download-icon.png" class="img-fluid" />
       Download Your Report
     </a>
   </div>
 </div>
</div>
</div>
</header>
<section class="middle-content">
 <div class="container">
  <div class="row">
   <div class="col-lg-12">
    <div class="row">
     <div class="col-lg-12" style="left: 25% !important";>
      <div class="left-side">
     <a href="index.php"><img src="images/Thank you_preview_rev_1.jpeg" class="img-fluid" /></a>
     </div>
   </div>
 </div>
</div>

</div>
</div>
</section>
<!-- jQuery library --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/function.js"></script>
</body>
</html>