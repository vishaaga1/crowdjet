<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1); 
error_reporting(E_ALL);
session_start();

if(isset($_POST['submit']) && !empty($_POST))
{
  $_SESSION['post-data'] = $_POST;
  $_SESSION['lid'] = (isset($_GET['lid'])) ? $_GET['lid'] : "null";
  $_SESSION['aid'] = (isset($_GET['aid'])) ? $_GET['aid'] : "null";
  include ('zoho/ZohoCrmClient_new.php');
  $crmObj = new ZohoCrmClient();
  $number = $_POST['phone'];
  $country_code = $_POST['country_code'];
  if($_SESSION['post-data'] != null)
  {
    $postData=[];
		$postData['First_Name']=$_SESSION['post-data']['first_name'];
		$postData['Last_Name']=$_SESSION['post-data']['last_name'];
		$postData['Email']=$_SESSION['post-data']['email'];
		$postData['Phone']=$_SESSION['post-data']['phone'];
		// $postData['Age']=$_SESSION['post-data']['age'];
		$postData['Lead_Source']="EIS";
		// $postData['Investor_knowledge']=$_SESSION['post-data']['investor_knowledge'];
		$postData['ClickID']=(isset($_SESSION['lid'])) ? $_SESSION['lid'] : "null";
		$postData['Company']=(isset($_SESSION['aid'])) ? $_SESSION['aid'] : "null";
		$zohoPost=[];
		$zohoPost['data'] = array($postData);
		$create=$crmObj->createZohoCrmRecord('Leads', $zohoPost);
		$res=json_decode($create);
		header('location: download_guide.php');
		//echo '<pre>';print_r($res);die;
		// if(isset($res->data[0]->code) && $res->data[0]->code=="SUCCESS"){
				
		// }
  }
  else
  {
    echo "<span style='color:red;margin-top:10px;'>Please enter valid details.</span>";
  }
}
?>
<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Global Investor | DOWNLOAD YOUR FREE EIS GUIDE</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link href="favicon.png" rel="apple-touch-icon" />
    <!--- teleinput for country code --->
    <link rel="stylesheet" href="intlTelInput/css/intlTelInput.css">
    <link href="images/favicon.png" rel="icon" type="image/png" />
  </head>
  <body>
    <header>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 my-auto">
            <div class="logo">
              <a href="index.php"><img src="images/logo.png" class="img-fluid" /></a>
            </div>
          </div>
          <div class="col-sm-6 my-auto">
            <div class="top-download">
              <a href="#download_guide">
                <img src="images/download-icon.png" class="img-fluid" />
                Download Your Guide
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
    <section class="middle-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <div class="row">
              <div class="col-lg-10">
                <div class="left-side">
                  <h1>Would you like to get upto <span>“50% tax relief”</span></h1>
                  <h2>Many people do not know the actual benefits of EIS & SEIS investing.</h2>
                  <h2>Benefits include:</h2>
                  <h2> <img src="images/Group_1_object.png" class="img-fluid" /> Up to 50% income tax relief </h2>
                  <h2> <img src="images/Group_1_object.png" class="img-fluid" /> Capital Gains Tax exemption </h2>
                  <h2> <img src="images/Group_1_object.png" class="img-fluid" /> Loss Relief  </h2>
                  <h2> <img src="images/Group_1_object.png" class="img-fluid" /> Inheritance Tax Relief</h2>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5" id="download_guide">
            <div class="right-side">
              <h2>For more information about the EIS & SEIS schemes</h2>
              <h3>EIS & SEIS Guide - Register Here & Free Gain Access The Latest Information On EIS & SEIS</h3>
              <form action="index.php" method="post">
                <div class="row contatc-box">
                  <input type="hidden" name="lid" id="lid" value="<?php echo isset($_REQUEST['lid']) ? $_REQUEST['lid'] : '' ?>">
                  <input type="hidden" name="aid" id="aid" value="<?php echo isset($_REQUEST['aid']) ? $_REQUEST['aid'] : '' ?>">
                  <div class="col-md-6">
                    <input type="text" name="first_name" placeholder="First Name" value="" required="" />
                  </div>
                  <div class="col-md-6">
                    <input type="text" name="last_name" placeholder="Last Name" value="" required="" />
                  </div>
                  <div class="col-md-12">
                    <input type="text" name="email" id="email" placeholder="Email Address" class="email" value="" required=""/>
                    <span id="Invalid_error"></span>
                  </div>
                  <div class="col-md-12">
                    <input type="hidden" name="country_code" id="country_code" value="">
                    <input type="text" name="phone" placeholder="Telephone Number" class="telephone" value="" id="phone" required=""/>
                    <span id="valid-msg" class="hide">✓ Valid</span>
                    <span id="error-msg" class="hide"></span>
                  </div>
                  <!-- <div class="col-md-4">
                    <input type="number" name="age" placeholder="Age" class="age" value="" required=""/>
                    <span class="error"></span>
                  </div>
                  <div class="col-md-8">
                    <select name="investor_knowledge" required>
                      <option value="" disabled selected>Your Investor Knowledge</option>
                      <option value="Extensive Knowledge">Extensive Knowledge</option>
                      <option value="Limited Knowledge">Limited Knowledge</option>
                      <option value="Moderate Knowledge">Moderate Knowledge</option>
                      <option value="Seasoned Professional">Seasoned Professional</option>
                    </select>
                  </div> -->
                  <div class="col-md-12">
                    <div id="Invalid_error"></div>
                    <button type="submit" id="submit" name="submit">
                      <img src="images/download-icon.png" class="img-fluid" />
                      Download Your Guide
                    </button>
                  </div>
                  <div class="col-md-12">
                    <p>By submitting your details, you agree to be contacted by one of our partners. You also acknowledge that you have read and understood our
                      <a href="terms_conditions.php">Terms & Conditions</a>.
                    </p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://rawgit.com/kswedberg/jquery-smooth-scroll/master/jquery.smooth-scroll.js"></script>
    <script src="https://kenwheeler.github.io/slick/slick/slick.js"></script>
    <script src="js/function.js"></script>
    <script type="text/javascript">
      $( document ).ready(function() {
        //country code script
        var input = document.querySelector("#phone"),
        errorMsg = document.querySelector("#error-msg"),
        validMsg = document.querySelector("#valid-msg");
        
        // here, the index maps to the error code returned from getValidationError
        var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];
        
        //get country code
        var countryData = window.intlTelInputGlobals.getCountryData(),
        country_code = document.querySelector("#country_code");
        // initialise plugin
        var iti = window.intlTelInput(input, {
          utilsScript: "intlTelInput/js/utils.js",
          initialCountry: 'gb'
        });
        // set it's initial value
        var all_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
        
        // listen to the telephone input for changes
        input.addEventListener('countrychange', function(e) {
          var selected_countrycode = country_code.value = iti.getSelectedCountryData().dialCode;
          console.log(selected_countrycode);
        });
        
        var reset = function() {
          input.classList.remove("error");
          errorMsg.innerHTML = "";
          errorMsg.classList.add("hide");
          validMsg.classList.add("hide");
        };
        
        // on blur: validate
        input.addEventListener('blur', function() {
          reset();
          if (input.value.trim()) {
            if (iti.isValidNumber()) {
              validMsg.classList.remove("hide");
            } else {
              input.classList.add("error");
              var errorCode = iti.getValidationError();
              errorMsg.innerHTML = errorMap[errorCode];
              errorMsg.classList.remove("hide");
            }
          }
        });
        
        // on keyup / change flag: reset
        input.addEventListener('change', reset);
        input.addEventListener('keyup', reset);
        
        //validate email
        $("#email").change(function(){
          var email = $("#email").val();
          level='Address';
          var emailvalidation = new data8.emailvalidation();
          
          emailvalidation.isvalid(
            email,
            level,
            [

            ],
            showIsValidResult
          );
        });
        
        function showIsValidResult(result) {
          console.log(result.Result);
          if(result.Result == 'Valid') {
            $('#Invalid_error').hide();
          }
          else
          {
            $('#Invalid_error').show();
            $('#Invalid_error').html('<p style="color: #dc3545;padding:0px !important;" >Please Enter Valid Email.</p>');
          }
        }
        
        //  validate age not less than 18
        $('.age').on('keyup',function(){
          $age = $(this).val();   
          if($age<18){
            $('.error').show();
            $('.error').html('<p style="color: #dc3545;padding:0px !important;" >Minimum Age Required Should be 18.</p>');
          }
          else
          {
            $('.error').hide();
          }
        });
        
        $('select#country_code').on('change',function(){
          var country_code =  $(this). children("option:selected").text();
          var code = country_code.replace('+','');
          console.log(code);
          $('#code_of_country').val(code);
        })
      });
    </script>
    <!--- teleinput for country code js --->
    <script type="text/javascript">
      window.onload = function onPageLoad() {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++)
        {
          var sParameterName = sURLVariables[i].split('=');
          if(sParameterName[0] == "lid")
          {
            $("#clickid").val(sParameterName[1]);
          }
          if(sParameterName[0] == "aid")
          {
            $("#aid").val(sParameterName[1]);
          }
        }
        //$("#submit").attr("href",  "index.php"+ window.location.search);
        // $("#aid").val(getUrlParameter("aid")  || "");
      }
    </script>
    <script src="intlTelInput/js/intlTelInput.min.js"></script>
    <script src="intlTelInput/js/utils.js"></script>
    <script type="text/javascript" src="https://webservices.data-8.co.uk/Javascript/Loader.ashx?key=4Z6P-LMTT-SIB9-C432&load=EmailValidation"></script>
  </body>
</html>