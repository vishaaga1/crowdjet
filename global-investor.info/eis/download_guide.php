<?php 
session_start(); 
?>
<!DOCTYPE HTML>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Global Investor | DOWNLOAD YOUR FREE EIS Guide</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link href="favicon.png" rel="apple-touch-icon" />
    <link href="images/favicon.png" rel="icon" type="image/png" />
  </head>
  <body>
    <header>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 my-auto">
            <div class="logo">
              <a href="index.php"><img src="images/logo.png" class="img-fluid" /></a>
            </div>
          </div>
          <div class="col-sm-6 my-auto">
            <div class="top-download">
              <a href="EIS_Guide.pdf">
                <img src="images/download-icon.png" class="img-fluid" />
                Open Free Guide
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
    <section class="middle-content">
      <div class="container">
        <div class="row">
          <div class="col-lg-7">
            <div class="row">
              <div class="col-lg-10">
                <div class="left-side">
                  <h1>Would you like to get upto <span>“50% tax relief”</span></h1>
                  <h2>Many people do not know the actual benefits of EIS & SEIS investing.</h2>
                  <h2>Benefits include:</h2>
                  <h2> <img src="images/Group_1_object.png" class="img-fluid" /> Up to 50% income tax relief</h2>
                  <h2> <img src="images/Group_1_object.png" class="img-fluid" /> Capital Gains Tax exemption</h2>
                  <h2> <img src="images/Group_1_object.png" class="img-fluid" /> Loss Relief</h2>
                  <h2> <img src="images/Group_1_object.png" class="img-fluid" /> Inheritance Tax Relief</h2>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="right-side">
              <h2>DOWNLOAD YOUR FREE<br />EIS & SEIS GUIDE</h2>
              <h3>EIS & SEIS Guide - Register Here & Free Gain Access The Latest Information On EIS & SEIS</h3>
              <form action="" method="post">
                <div class="row send_sms">
                  <div class="error"></div>
                  <div class="col-md-12 alert alert-success">
                    <img src="images/checked_icon.png" class="checked_icon"><br>
                    <p style="font-size: 20px;font-family: arial;">The guide you requested is now available.</p>
                    <p>Thank you for registering your interest in our free guide. Please click the button below to start downloading your free guide.</p>
                  </div>
                </div>
                <div class="col-md-12">
                  <button type="submit" id="submit" name="submit">
                    <a href="EIS_Guide.pdf" download style="color: #fff !important;">
                    <img src="images/download-icon.png" class="img-fluid" />
                    Download Your Guide</a>
                  </button>
                  <p>By submitting your details, you agree to be contacted by one of our partners. You also acknowledge that you have read and understood our
                    <a href="terms_conditions.php">Terms & Conditions</a>.
                  </p>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="js/function.js"></script>
    <script type="text/javascript">
      function verifyOTP() {
        $(".error").html("").hide();
        $(".success").html("").hide();
        var code = "";
        $("input[class *= 'code']").each(function(){
          code = code + $(this).val();
        });
        var otp = parseInt(code);
        var input = {
          "otp" : otp,
          "action" : "verify_otp"
        };
        if (otp.toString().length == 6 && otp != null) {
          console.log(otp.toString().length);
          $.ajax({
            url : 'verify_otp.php',
            type : 'POST',
            dataType : "json",
            data : input,
            success : function(response) {
              console.log(response);
              $("." + response.type).html(response.message)
              $("." + response.type).show();
              window.location.href = "/download_guide.php";
            },
            error : function() {
              alert("You have entered wrong OTP");
            }
          });
        } else {
          $(".error").html('You have entered wrong OTP.')
          $(".error").show();
        }
      }
    </script>
  </body>
</html>