<?php
/*
Template Name: Styles
*/
$GLOBALS['theme_current_template_info'] = array('name' => 'styles');
?>
<?php if (!defined('ABSPATH')) exit; // Exit if accessed directly
?>
<!DOCTYPE html>
<html <?php echo !is_rtl() ? 'dir="ltr" ' : ''; language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>" />
    
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <script>
    var themeHasJQuery = !!window.jQuery;
</script>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/jquery.js?ver=' . wp_get_theme()->get('Version'); ?>"></script>
<script>
    window._$ = jQuery.noConflict(themeHasJQuery);
</script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--[if lte IE 9]>
<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.css' ?>" />
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.ie.js' ?>"></script>
<![endif]-->
<link class="" href='//fonts.googleapis.com/css?family=Raleway:100,200,300,regular,500,600,700,800,900&subset=latin' rel='stylesheet' type='text/css'>
<script src="<?php echo get_bloginfo('template_url', 'display') . '/layout.core.js' ?>"></script>
<script src="<?php echo get_bloginfo('template_url', 'display'); ?>/CloudZoom.js?ver=<?php echo wp_get_theme()->get('Version'); ?>" type="text/javascript"></script>
    
    <?php wp_head(); ?>
    
</head>
<?php do_action('theme_after_head'); ?>
<?php ob_start(); // body start ?>
<body <?php body_class(' hfeed bootstrap bd-body-10  bd-pagebackground bd-margins'); /*   */ ?>>
<header class=" bd-headerarea-1 bd-margins">
        <section class=" bd-section-3 hidden-md hidden-sm hidden-xs hidden-lg bd-tagstyles" id="section3" data-section-title="Menu">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutbox-3 bd-no-margins clearfix">
    <div class="bd-container-inner">
        <?php
    if (theme_get_option('theme_use_default_menu')) {
        wp_nav_menu( array('theme_location' => 'primary-menu-1') );
    } else {
        theme_hmenu_1();
    }
?>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-5 bd-tagstyles" id="section5" data-section-title="Header">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-8 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-7 
 col-lg-2
 col-xs-12">
    <div class="bd-layoutcolumn-7 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-2', '16_13');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-2', ' bd-sidebar-13 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-18 
 col-lg-10
 col-xs-12">
    <div class="bd-layoutcolumn-18 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar('Area-4', '9_15');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'Area-4', ' bd-sidebar-15 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
</header>
	
		<section class=" bd-section-10 bd-page-width bd-tagstyles " id="section10" data-section-title="2 Columns">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-16 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-24 
 col-sm-4">
    <div class="bd-layoutcolumn-24 bd-column" ><div class="bd-vertical-align-wrapper"><a 
 href="" class="bd-linkbutton-2  bd-button-20 bd-icon bd-icon-25 bd-own-margins bd-content-element"    >
    Button
</a></div></div>
</div>
	
		<div class=" bd-columnwrapper-49 
 col-sm-4">
    <div class="bd-layoutcolumn-49 bd-column" ><div class="bd-vertical-align-wrapper"><a 
 href="" class="bd-linkbutton-4  bd-button-22  bd-own-margins bd-content-element"    >
    Input Button
</a></div></div>
</div>
	
		<div class=" bd-columnwrapper-26 
 col-sm-4">
    <div class="bd-layoutcolumn-26 bd-column" ><div class="bd-vertical-align-wrapper"><div class=" bd-bootstrapprogressbars-2 progress ">
    <div class="progress-bar" role="progressbar" style="width: 50%;">
        
    </div>
</div></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-14 bd-page-width bd-tagstyles " id="section14" data-section-title="2 Columns">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-33 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-51 
 col-sm-6">
    <div class="bd-layoutcolumn-51 bd-column" ><div class="bd-vertical-align-wrapper"><a 
 onclick="jQuery(this).closest('form').submit();" href="#" class="bd-linkbutton-9  bd-button-25  bd-own-margins bd-content-element"    >
    Button
</a></div></div>
</div>
	
		<div class=" bd-columnwrapper-53 
 col-sm-6">
    <div class="bd-layoutcolumn-53 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-2 bd-own-margins bd-icon-30 bd-icon "></span>
	
		<a 
 onclick="jQuery(this).closest('form').submit();" href="#" class="bd-linkbutton-6  bd-button-23  bd-own-margins bd-content-element"    >
    Button
</a></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-26 bd-tagstyles" id="section4" data-section-title="ISA Example 2">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-34 bd-page-width  bd-no-margins bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-70 
 col-md-2">
    <div class="bd-layoutcolumn-70 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-51 animated bd-animation-3 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
£5,000
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-15 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Initial Amount Invested
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-88 
 col-md-1">
    <div class="bd-layoutcolumn-88 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-24 animated bd-animation-4 bd-own-margins bd-icon-79 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-13 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-72 
 col-md-2">
    <div class="bd-layoutcolumn-72 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-53 animated bd-animation-5 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
8%
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-19 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Annual Interest Rate
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-76 
 col-md-1">
    <div class="bd-layoutcolumn-76 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-26 animated bd-animation-6 bd-own-margins bd-icon-81 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-15 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-93 
 col-md-2">
    <div class="bd-layoutcolumn-93 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-54 animated bd-animation-7 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
£2,346.64
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-47 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Your Tax-free Interest
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-91 
 col-md-1">
    <div class="bd-layoutcolumn-91 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-27 animated bd-animation-8 bd-own-margins bd-icon-82 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-16 clearfix"></div></div></div>
</div>
	
		<div class=" bd-columnwrapper-78 
 col-md-2">
    <div class="bd-layoutcolumn-78 bd-column" ><div class="bd-vertical-align-wrapper"><p class=" bd-textblock-56 animated bd-animation-9 bd-content-element" data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false">
    <?php
echo <<<'CUSTOM_CODE'
£7346.64
CUSTOM_CODE;
?>
</p>
	
		<h2 class=" bd-textblock-27 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Total Amount After 5 Years
CUSTOM_CODE;
?>
</h2></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<section class=" bd-section-27 bd-tagstyles" id="section4" data-section-title="ISA Comparison 2">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-36 bd-page-width  bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row 
 bd-row-flex 
 bd-row-align-middle">
                <div class=" bd-columnwrapper-80 
 col-sm-3">
    <div class="bd-layoutcolumn-80 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-14 animated bd-animation-1 bd-own-margins bd-icon-68 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-5 clearfix"></div>
	
		<h2 class=" bd-textblock-31 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Cash<br>
ISA
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-82 
 col-sm-3">
    <div class="bd-layoutcolumn-82 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-29 animated bd-animation-14 bd-own-margins bd-icon-84 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-7 clearfix"></div>
	
		<h2 class=" bd-textblock-35 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Easy Access&nbsp;<br>ISA
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-84 
 col-sm-3">
    <div class="bd-layoutcolumn-84 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-30 animated bd-animation-15 bd-own-margins bd-icon-85 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-9 clearfix"></div>
	
		<h2 class=" bd-textblock-39 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Stocks &amp; Shares&nbsp;<br>ISA
CUSTOM_CODE;
?>
</h2></div></div>
</div>
	
		<div class=" bd-columnwrapper-86 
 col-sm-3">
    <div class="bd-layoutcolumn-86 bd-column" ><div class="bd-vertical-align-wrapper"><span class="bd-iconlink-31 animated bd-animation-16 bd-own-margins bd-icon-86 bd-icon " data-animation-name="pulse" data-animation-event="hover" data-animation-duration="1000ms" data-animation-delay="0ms" data-animation-infinited="false"></span>
	
		<div class=" bd-spacer-11 clearfix"></div>
	
		<h2 class=" bd-textblock-43 bd-content-element">
    <?php
echo <<<'CUSTOM_CODE'
Innovative Finance&nbsp;
ISA
CUSTOM_CODE;
?>
</h2></div></div>
</div>
            </div>
        </div>
    </div>
</div>
    </div>
</section>
	
		<div class="bd-contentlayout-10 bd-sheetstyles   bd-no-margins bd-margins" >
    <div class="bd-container-inner">

        <div class="bd-flex-vertical bd-stretch-inner bd-no-margins">
            
            <div class="bd-flex-horizontal bd-flex-wide bd-no-margins">
                
                <div class="bd-flex-vertical bd-flex-wide bd-no-margins">
                    

                    <div class=" bd-layoutitemsbox-3 bd-flex-wide bd-margins">
    <div class=" bd-content-2">
    
    <?php theme_print_content(); ?>
</div>
</div>

                    
                </div>
                
            </div>
            
        </div>

    </div>
</div>
	
		<footer class=" bd-footerarea-1">
    <?php if (theme_get_option('theme_override_default_footer_content')): ?>
        <?php echo do_shortcode(theme_get_option('theme_footer_content')); ?>
    <?php else: ?>
        <section class=" bd-section-2 bd-page-width bd-tagstyles " id="section2" data-section-title="">
    <div class="bd-container-inner bd-margins clearfix">
        <div class=" bd-layoutcontainer-28 bd-columns bd-no-margins">
    <div class="bd-container-inner">
        <div class="container-fluid">
            <div class="row ">
                <div class=" bd-columnwrapper-62 
 col-xs-12">
    <div class="bd-layoutcolumn-62 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer1", 'footer_2_3');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer1', ' bd-footerwidgetarea-3 clearfix', '');
?>
	
		<div class=" bd-customcmscode-2 bd-tagstyles">
<?php include get_template_directory() . "/fragments/code-2.php"; ?>
</div></div></div>
</div>
	
		<div class=" bd-columnwrapper-63 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-63 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer2", 'footer_8_4');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer2', ' bd-footerwidgetarea-4 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-64 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-64 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer3", 'footer_6_6');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer3', ' bd-footerwidgetarea-6 clearfix', '');
?></div></div>
</div>
	
		<div class=" bd-columnwrapper-65 
 col-md-4
 col-xs-12">
    <div class="bd-layoutcolumn-65 bd-column" ><div class="bd-vertical-align-wrapper"><?php
    ob_start();
    theme_print_sidebar("footer4", 'footer_7_8');
    $current_sidebar_content = trim(ob_get_clean());

    if (isset($theme_hide_sidebar_area)) {
        $theme_hide_sidebar_area = $theme_hide_sidebar_area && !$current_sidebar_content;
    }

    theme_print_sidebar_content($current_sidebar_content, 'footer4', ' bd-footerwidgetarea-8 clearfix', '');
?></div></div>
</div>
            </div>
        </div>
    </div>
</div>
	
		
    </div>
</section>
    <?php endif; ?>
</footer>
<div id="wp-footer">
    <?php wp_footer(); ?>
    <!-- <?php printf(__('%d queries. %s seconds.', 'default'), get_num_queries(), timer_stop(0, 3)); ?> -->
</div>
</body>
<?php echo apply_filters('theme_body', ob_get_clean()); // body end ?>
</html>