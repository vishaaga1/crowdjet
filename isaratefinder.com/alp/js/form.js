var isEmail = false;
var isPhone = false;
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab
var formValidation = {};
if($("form").length > 0){
    validate();
}

$( ".haveIsa" ).change(function() {
  if ($("input[name='haveisa']:checked").val() !== "") {
    nextStep(1);
  }
});

$( ".invest-amount" ).change(function() {
  if ($("input[name='invest-amount']:checked").val() !== "") {
    nextStep(1);
  }
});

$( ".existingIsa" ).change(function() {
  if ($("input[name='existingIsa']:checked").val() !== "") {
    nextStep(1);
  }
});

$( ".interest-paid-type" ).change(function() {
  if ($("input[name='interestPaidType']:checked").val() !== "") {
    nextStep(1);
  }
});

window.onload = function onPageLoad() {
  $("#clickid").val(getUrlParameter("lid")  || "");
  $("#aid").val(getUrlParameter("aid")  || "");
  $(".comp_btn").attr("href", "index.php"+ window.location.search);
}

function showTab(n) {
  // This function will display the specified tab of the form ...
  var x = document.getElementsByClassName("tab");
  if(x.length > 0){
    x[n].style.display = "block";
  }
}

function backStep(n){
  if (currentTab > 0) {
    var x = document.getElementsByClassName("tab");
    x[currentTab].style.display = "none";
    currentTab = currentTab + n;
    showTab(currentTab);
  }
}

function nextStep(n) {
  $('#dealform').parsley().whenValidate({
    group: 'block-' + currentTab
  }).done(function() {
    var x = document.getElementsByClassName("tab");
    x[currentTab].style.display = "none";
    // Increase or decrease the current tab by 1:
    currentTab = currentTab + n;
    if (currentTab >= x.length) {
      if (anOtherValidate() == true){
        $('.nextStep').prop('disabled', true);
        postData()
      }else{
        $('#dealform').parsley().validate()
      }
      return true
    }
    showTab(currentTab);
  })
}

function anOtherValidate() {
  var selectedHaveIsa = $("input[name='haveIsa']:checked"). val();
  var selectedExistingIsa = $("input[name='existingIsa']:checked").val();
  var selectedInterstPaidType = $("input[name='interestPaidType']:checked").val();
  var selectedInvestAmount = $("input[name='invest-amount']:checked").val();
  
  if (selectedHaveIsa == "") {
    var x = document.getElementsByClassName("tab");
    x[0].style.display = "block";
    x[1].style.display = "none";
    x[2].style.display = "none";
    x[3].style.display = "none";
    x[4].style.display = "none";
    currentTab = 0;
    $('#dealform').parsley().validate()
    return false
  }else if(selectedInvestAmount == "") {
    var x = document.getElementsByClassName("tab");
    x[1].style.display = "block";
    x[0].style.display = "none";
    x[2].style.display = "none";
    x[3].style.display = "none";
    x[4].style.display = "none";
    currentTab = 1;
    $('#dealform').parsley().validate()
    return false
  }else if(selectedExistingIsa == "") {
    var x = document.getElementsByClassName("tab");
    x[2].style.display = "block";
    x[0].style.display = "none";
    x[1].style.display = "none";
    x[3].style.display = "none";
    x[4].style.display = "none";
    currentTab = 2;
    $('#dealform').parsley().validate()
    return false
  }else if(selectedInterstPaidType == "") {
    var x = document.getElementsByClassName("tab");
    x[3].style.display = "block";
    x[0].style.display = "none";
    x[1].style.display = "none";
    x[2].style.display = "none";
    x[4].style.display = "none";
    currentTab = 3;
    $('#dealform').parsley().validate()
    return false
  }else if(document.getElementsByClassName('first_name')[0].value.length < 2 || document.getElementsByClassName('last_name')[0].value.length < 2 || document.getElementsByClassName('email')[0].value.length < 4 || document.getElementsByClassName('phone')[0].value.length < 10) {
    var x = document.getElementsByClassName("tab");
    x[4].style.display = "block";
    x[0].style.display = "none";
    x[1].style.display = "none";
    x[2].style.display = "none";
    x[3].style.display = "none";
    currentTab = 4;
    $('#dealform').parsley().validate()
    return false
  }
  return true
}

function validate() {
  formValidation = $('#dealform').parsley({
    trigger: "focusout",
    errorClass: 'error',
    triggerAfterFailure: "focusout",
    successClass: 'valid',
    errorsWrapper: '<div class="parsley-error-list"></div>',
    errorTemplate: '<label class="error"></label>',
    errorsContainer (field) {
      if(field.$element.hasClass('error-on-button')){
        return $(field.element.closest(".tab").querySelector(".error-box"))
      }
      if(field.$element.hasClass('approve')) {
        return $('.error-checkbox')
      }
      return field.$element.parent()
    },
  })
  // validateEmail()
}

var phone = document.getElementById("email").value;
if(phone){
  console.log(phone);
}

$("#phone").change(function() {   // 1st
  var telephoneNumber = $("#phone").val();
  defaultCountry='GB';
  var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
  internationaltelephonevalidation.isvalid(
    telephoneNumber,
    defaultCountry,
    [
      new data8.option('UseMobileValidation', 'true'),
      new data8.option('UseLineValidation', 'false'),
      new data8.option('RequiredCountry', ''),
      new data8.option('AllowedPrefixes', ''),
      new data8.option('BarredPrefixes', ''),
      new data8.option('UseUnavailableStatus', 'true'),
      new data8.option('UseAmbiguousStatus', 'true'),
      new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
      new data8.option('ExcludeUnlikelyNumbers', 'false')
    ],
    showIsValidResult
  );
});

function showIsValidResult(result) {
  if(result.Result.ValidationResult == 'Valid') {
    if(result.Result.NumberType == 'Mobile') {
      console.log(result.Result.NumberType);
      $('#btn-submit').prop('disabled', false);
      $('#Invalid_Phone').hide();
      isPhone = true;
    }
    else {
      console.log(result.Result.NumberType);
      $('#Invalid_Phone').show();
      $('#Invalid_Phone').html('<p style="color: #dc3545;" >Please Enter Valid Mobile Number</p>');
      $('#btn-submit').prop('disabled', true);
    }
  }
  else {
    console.log(result.Result.ValidationResult);
    $('#Invalid_Phone').show();
    $('#Invalid_Phone').html('<p style="color: #dc3545;" >Please Enter Valid Mobile Number</p>');
    $('#btn-submit').prop('disabled', true);
  }
}

// function validateEmail() {
//   window.Parsley.addValidator('validemail', {
//     validateString: function(value) {
//       var options = {};
//       var params = {
//         email: $("#email").val(),
//         level: "MX",
//         options: options
//       };
//       var email = $("#email").val();
//       if(email) {
//         var xhr = $.ajax("https://webservices.data-8.co.uk/EmailValidation/IsValid.json?key=RX95-DNG3-VK3T-AA3G",
//         {
//           method: "POST",
//           data: JSON.stringify(params)
//         });
//         return xhr.then(function(json) {
//           console.log(json.Result);
//           if (json.Result == "Valid") {
//             isEmail = true;
//             return true
//           }else {
//             return $.Deferred().reject("Please Enter Valid Email Address");
//           }
//         })
//       }
//     },
//     messages: {
//       en: 'Please Enter Valid Email Address',
//     }
//   });
// }

  function getData() {
    var e = JSON.parse(localStorage.getItem("parameters"))
    return {
      firstname: $("#first_name").val(),
      lastname: $("#last_name").val(),
      email: $("#email").val(),
      phone1: $("#phone").val(),
      investing_amount: $("input[name='invest-amount']").val(),
      have_Isa: $("input[name='haveIsa']:checked"). val(),
      existing_Isa : $("input[name='existingIsa']:checked").val(),
      interst_paid_type : $("input[name='interestPaidType']:checked").val(),
      timestamp: new Date,
      user_agent: window.navigator.userAgent,
      parameters: e
    };
  }
  function postData() {
    var e = getData();
    e['before_send'] = JSON.stringify(getData());
    console.log(e)
    $.ajax({
      type: "POST",
      url: "",
      data: e,
      success: function(e) {
        console.log(e);
        $('.but_loader').hide()
        window.location = "../thankyou.php";
      },
      dataType: "json"
    })
  }

  function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;
    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
    }
  }

  function getFormattedCurrentDate() {
    var date = new Date();
    var day = addZero(date.getDate());
    var monthIndex = addZero(date.getMonth() + 1);
    var year = date.getFullYear();
    var min = addZero(date.getMinutes());
    var hr = addZero(date.getHours());
    var ss = addZero(date.getSeconds());

    return day + '/' + monthIndex + '/' + year + ' ' + hr + ':' + min + ':' + ss;
  }

  function addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

$( document ).ready(function() {
  // Validate email
  var email = $("#email").val();
  console.log(email);
  if(email) { 
    var email = $("#email").val();
    level='Address';
    var emailvalidation = new data8.emailvalidation();
    emailvalidation.isvalid(
        email,
        level,
        [

        ],
        showIsEmailValid
    );

    function showIsEmailValid(result) {
      if(result.Result == "Valid") {
        console.log(result.Result);
        $('#btn-submit').prop('disabled', false);
        $('#Invalid_Email').hide();
        isEmail = true;
      }else {
        console.log(result.Result);
        $('#Invalid_Email').show();
        $('#Invalid_Email').html('<p style="color: #dc3545;" >Please Enter Valid Email Address</p>');
        $('#btn-submit').prop('disabled', true);
      }
    }
  }

  var phone = $("#phone").val();
  console.log(phone);
  if(phone) {
    var telephoneNumber = phone;
    defaultCountry='GB';
    var internationaltelephonevalidation = new data8.internationaltelephonevalidation();
    internationaltelephonevalidation.isvalid(
      telephoneNumber,
      defaultCountry,
      [
        new data8.option('UseMobileValidation', 'true'),
        new data8.option('UseLineValidation', 'false'),
        new data8.option('RequiredCountry', ''),
        new data8.option('AllowedPrefixes', ''),
        new data8.option('BarredPrefixes', ''),
        new data8.option('UseUnavailableStatus', 'true'),
        new data8.option('UseAmbiguousStatus', 'true'),
        new data8.option('TreatUnavailableMobileAsInvalid', 'false'),
        new data8.option('ExcludeUnlikelyNumbers', 'false')
      ],
      showIsValidResult
    );
  }
});