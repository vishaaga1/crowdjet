// JavaScript Document
$(document).ready(function(e) {
    var url_string = window.location.href;
    var url = new URL(url_string);
    var haveIsa = url.searchParams.get("haveIsa");
    // console.log(haveIsa);
    var investAmount = url.searchParams.get("investAmount");
    var existingIsa = url.searchParams.get("existingIsa");
    var interestPaidType = url.searchParams.get("interestPaidType");
    var firstName = url.searchParams.get("firstName");
    var lastName = url.searchParams.get("lastName");
    var email = url.searchParams.get("email");
    var phone = url.searchParams.get("phone");
    
    if(haveIsa == "Yes"){
        document.dealForm.haveIsa[0].checked=true;
    }
    if(haveIsa == "No"){
        document.dealForm.haveIsa[1].checked=true;
    }
    if(investAmount == "£500 - £5,000"){
        document.dealForm.investAmount[0].checked=true;
    }
    if(investAmount == "£5,000 - £20,000"){
        document.dealForm.investAmount[1].checked=true;
    }
    if(investAmount == "£20,000 - £50,000"){
        document.dealForm.investAmount[2].checked=true;
    }
    if(investAmount == "Over £50,000"){
        document.dealForm.investAmount[3].checked=true;
    }
    if(existingIsa == "Yes"){
        document.dealForm.existingIsa[0].checked=true;
    }
    if(existingIsa == "No"){
        document.dealForm.existingIsa[1].checked=true;
    }
    if(interestPaidType == "Annually"){
        document.dealForm.interestPaidType[0].checked=true;
    }
    if(interestPaidType == "Monthly"){
        document.dealForm.interestPaidType[1].checked=true;
    }
    document.getElementById("first_name").value=firstName;
    document.getElementById("last_name").value=lastName;
    document.getElementById("email").value=email;
    document.getElementById("phone").value=phone;
    // document.getElementById("customcheck").value="checked";
});
