<?php

if (get_option('d8cf7_predictiveaddress')) {
	add_filter( 'gform_pre_render', 'd8_predictiveaddress_gform_pre_render', 10, 2 );
	add_action( 'wp_footer', 'd8_predictiveaddress_gform_enqueue_scripts');
}

$d8pagf_script_vars = array(
	'ajaxKey' => get_option('d8cf7_ajax_key'),
	'forms' => array()
);

function d8_predictiveaddress_gform_pre_render( $form, $is_ajax ) {
	global $d8pagf_script_vars;
	$forms = &$d8pagf_script_vars['forms'];
	$formId = $form['id'];
	$fields = array();
	
	foreach ( $form['fields'] as &$field ) {
		if ( $field->get_input_type() === 'address' )
			array_push($fields, $field);
	}
	
	$forms[$formId] = $fields;
	
	return $form;
}

function d8_predictiveaddress_gform_enqueue_scripts() {
	global $d8pagf_script_vars;
	
	if ( count($d8pagf_script_vars['forms']) > 0 ) {
		wp_register_script('d8pagf', 'https://webservices.data-8.co.uk/javascript/predictiveaddress_gf.js', array('jquery', 'd8pa'), null, true);
		wp_localize_script('d8pagf', 'd8pagf_script_vars', $d8pagf_script_vars);
		wp_enqueue_script('d8pagf', $in_footer = true);
	}
}

if (get_option('d8cf7_telephone_validation')) {
	add_filter( 'gform_field_validation', 'd8_validate_tel_gf', 10, 4 );
}

function startsWith($haystack, $needle) {
	$length = strlen($needle);
	return (substr($haystack, 0, $length) == $needle);
}

function d8_validate_tel_gf( $result, $value, $form, $field ) {
	if ( $field->get_input_type() === 'phone' && $result['is_valid'] && $value != '' ) {
		$classes = explode(' ', $field->cssClass);
		$country = '44'; 
		$line = 'false';
		$mobile = 'false';
		$allowedPrefixes = '';
		$barredPrefixes = '';
		
		foreach ($classes as $class) {
			if (startsWith($class, 'd8country_'))
				$country = substr($class, 10);
			
			if ($class == 'd8landline')
				$line = 'true';
			
			if ($class == 'd8mobile')
				$mobile = 'true';
			
			// Pull out a list of Allowed Prefixes (if specified)
			if (startsWith($class, 'd8AllowedPrefixes_')){
				$allowedPrefixesInput = substr($class, 10);
				$allowedPrefixArray = [];
				if ($allowedPrefixesInput != '') {
					$allowedPrefixArray = explode("_", $class);
				}
				foreach ($allowedPrefixArray as &$prefix) {
					if ($prefix != 'd8AllowedPrefixes'){
						$allowedPrefixes = ($allowedPrefixes == '' ? '+'.$prefix : $allowedPrefixes.','.'+'.$prefix);
					}
				}
			}

			// Pull out a list of Barred Prefixes (if specified)
			if (startsWith($class, 'd8BarredPrefixes_')){
				$barredPrefixesInput = substr($class, 10);
				$barredPrefixArray = [];
				if ($barredPrefixesInput != '') {
					$barredPrefixArray = explode("_", $class);
				}
				foreach ($barredPrefixArray as &$prefix) {
					if ($prefix != 'd8BarredPrefixes_'){
						$barredPrefixes = ($barredPrefixes == '' ? '+'.$prefix : $barredPrefixes.','.'+'.$prefix);
					}
				}
			}
		}
		
		$d8cf7_username = get_option('d8cf7_username');
		$d8cf7_password = get_option('d8cf7_password');
	
		// Set up the parameters for the web service call:
		// https://www.data-8.co.uk/support/service-documentation/international-telephone-validation-part-of-the-phone-validation-suite/reference/isvalid
		$params = array(
			'username' => $d8cf7_username,
			'password' => $d8cf7_password,
			'telephoneNumber' => $value,
			'defaultCountry' => $country,
			'options' => array ('Option' => array(
				array('Name' => 'UseMobileValidation', 'Value' => $mobile),
				array('Name' => 'UseLineValidation', 'Value' => $line),
				array('Name' => 'AllowedPrefixes', 'Value' => $allowedPrefixes),
				array('Name' => 'BarredPrefixes', 'Value' => $barredPrefixes)
			))
		);
		
		$client = new SoapClient('https://webservices.data-8.co.uk/internationaltelephonevalidation.asmx?WSDL', array('features' => SOAP_SINGLE_ELEMENT_ARRAYS));
		$wsresult = $client->IsValid($params);
		
		if ($wsresult->IsValidResult->Status->Success && $wsresult->IsValidResult->Result->ValidationResult == 'Invalid') {
			$result['is_valid'] = false;
			$result['message'] = $field->errorMessage;
		}
	}

	return $result;
}

if (get_option('d8cf7_email_validation')) {
	add_filter( 'gform_field_validation', 'd8_validate_email_gf', 10, 4 );
}

function d8_validate_email_gf( $result, $value, $form, $field ) {
	if ( $field->get_input_type() === 'email' && $result['is_valid'] && $value != '' ) {
		$classes = explode(' ', $field->cssClass);
		$level = 'MX';
		
		foreach ($classes as $class) {
			if (startsWith($class, 'd8level_'))
				$level = substr($class, 8);
		}
		
		$d8cf7_username = get_option('d8cf7_username');
		$d8cf7_password = get_option('d8cf7_password');
	
		// Set up the parameters for the web service call:
		// https://www.data-8.co.uk/support/service-documentation/email-validation/reference/isvalid
		$params = array(
			'username' => $d8cf7_username,
			'password' => $d8cf7_password,
			'email' => $value,
			'level' => $level,
			'options' => null
		);
		
		$client = new SoapClient('https://webservices.data-8.co.uk/emailvalidation.asmx?WSDL', array('features' => SOAP_SINGLE_ELEMENT_ARRAYS));
		$wsresult = $client->IsValid($params);
		
		if ($wsresult->IsValidResult->Status->Success && $wsresult->IsValidResult->Result == 'Invalid') {
			$result['is_valid'] = false;
			$result['message'] = $field->errorMessage;
		}
	}

	return $result;
}

if (get_option('d8cf7_salaciousName')) {
	add_filter( 'gform_field_validation', 'd8_validate_name_gf', 10, 4 );
}

function d8_validate_name_gf ( $result, $value, $form, $field ) {
    if ( $field->type == 'name' ) {
 
        // Input values
        $prefix = rgar( $value, $field->id . '.2' );
        $first  = rgar( $value, $field->id . '.3' );
        $middle = rgar( $value, $field->id . '.4' );
        $last   = rgar( $value, $field->id . '.6' );
		$suffix = rgar( $value, $field->id . '.8' );

		// Grab the username and passwords
		$d8cf7_username = get_option('d8cf7_username');
		$d8cf7_password = get_option('d8cf7_password');
	
		// Set up the parameters for the web service call:
		// http://webservices.data-8.co.uk/salaciousname.asmx?op=IsUnusableNameSimple
		$params = array(
			'username' => $d8cf7_username,
			'password' => $d8cf7_password,
			'title' => $prefix,
			'forename' => $first,
			'middlename' => $middle,
			'surname' => $last
		);
		
		$client = new SoapClient('https://webservices.data-8.co.uk/salaciousname.asmx?WSDL', array('features' => SOAP_SINGLE_ELEMENT_ARRAYS));
		$wsresult = $client->IsUnusableNameSimple($params);
		
		if ($wsresult->IsUnusableNameSimpleResult->Status->Success && $wsresult->IsUnusableNameSimpleResult->Result != '') {
			$result['is_valid'] = false;
			$result['message'] = $field->errorMessage;
		}
	}
	
    return $result;
}

?>