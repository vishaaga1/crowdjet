<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'isafinder_v2');

/** MySQL database username */
define('DB_USER', 'isafinder_v2');

/** MySQL database password */
define('DB_PASSWORD', 'isafinder_v2');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kz61s7Y0BuLAbBFyISf8styqnHgV3x0vKYbffASVKWZ6ExAmaHCC3Mo316GsMBPV');
define('SECURE_AUTH_KEY',  'LlsY2b8lujmJ8K8F9xF3pFJ6xh16Zl9dqmlB6EUtu7hzbBipoq9utgiOgT0afGU4');
define('LOGGED_IN_KEY',    'iGL1ytOu2PvuYaQ8geKw84762GeCHZ1K0rYicDvjIFz32V1l6UhUwyhi7Ps5s2Wj');
define('NONCE_KEY',        'fPqLiJueb7hbI4GOY1aSWyNNIrD4TjdKVpEBJAC8GRXNp9j36isqkuc3wn7TSWkz');
define('AUTH_SALT',        'PfaA2eCV4K3mZgwoohOSqbNz5PJjLBOP5zzgVrO8b77J0HzSxxKjy8sYzjujeTwF');
define('SECURE_AUTH_SALT', 'h3NWCEKHAaoPAz9by7GRXCFWsyh5vup3TM5UPTsEa59n01ZZ4VF9x6m0OxAyNDZY');
define('LOGGED_IN_SALT',   '5l4YfzV4q40dHgAnVhw0uDheFBUSwsVpBkUhvIboTBwSGoX93E9tZBhPr0hx1j6Z');
define('NONCE_SALT',       '18qzWzLqnuSodJ6dEV34GQAkyL527SqWLTm9Gg9ma3jOdCsk9nA4GTpQcB1pfQvj');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
