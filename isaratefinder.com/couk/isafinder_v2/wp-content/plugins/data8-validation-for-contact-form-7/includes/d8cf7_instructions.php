<table class="wp-list-table widefat fixed bookmarks">
    <thead>
        <tr>
            <th><strong>Instructions</strong></th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>
			<ol>
				<li>
					Enter your Data8 username and password to enable the enhanced validation for email and telephone number fields.
					This plugin will check which services you have credits for and enable the appropriate validation.
				</li>
				<li>
					Standard Data8 validation will be applied to all email and telephone number fields in Gravity Forms and Contact
					Form 7. PredictiveAddress&trade; will be applied to all address fields in Woocommerce and Gravity Forms, and can
					be selectively applied to fields in Contact Form 7. See the Validation Rules section below for details on how to refine the rules.
				</li>
			</ol>
		</td>
	</tr>
	</tbody>
</table>
<br/>

<table class="wp-list-table widefat fixed bookmarks">
    <thead>
        <tr>
            <th><strong>Gravity Forms Validation Rules</strong></th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>  
			<ol class="validation_rules">
				<li>
					<strong>Email validation</strong>
					Just use the standard Gravity Forms email field. For further control, include a "d8level_<i>xxx</i>" custom CSS class:
					<ul>
						<li><strong>d8level_Syntax</strong> - validates the syntax of the email address (quickest)</li>
						<li><strong>d8level_MX</strong> - validates the domain name (right hand part) the email address (default)</li>
						<li><strong>d8level_Server</strong> - validates the mail servers for the domain are alive</li>
						<li><strong>d8level_Address</strong> - validates the full email address (slowest)</li>
					</ul>
				</li>
				
				<li>
					<strong>Telephone number validation</strong>
					Just use the standard Gravity Forms phone field. For further control, include "d8mobile", "d8landline" and "d8country_<i>XX</i>" custom CSS classes:
					<ul>
						<li><strong>d8mobile</strong> - if the telephone number is a mobile, use the enhanced Mobile Validation service to perform more detailed validation</li>
						<li><strong>d8landline</strong> - if the telephone number is a UK landline, use the enhanced Landline Validation service to perform more detailed validation</li>
						<li><strong>d8country_US</strong> - if the telephone number does not include an explicit country code prefix, use the country specified in this option instead. Defaults to GB</li>
						<li><strong>d8AllowedPrefixes_441_442</strong> - if specified any prefixes (without the leading plus "+" and seperated by an underscore "_") are the only ones allowed</li>
						<li><strong>d8BarredPrefixes_44151_442</strong> - if specified any prefixes (without the leading plus "+" and seperated by an underscore "_") are barred</li>
					</ul>
				</li>
				
				<li>
					<strong>PredictiveAddress&trade;</strong>
					Just use the standard Gravity Forms address field.
				</li>
				
				<li>
					<strong>SalaciousName&trade;</strong>
					Just use the standard Gravity Forms name field.
				</li>
			</ol>
		</td>
	</tr>
	</tbody>
</table>
<br/>

<table class="wp-list-table widefat fixed bookmarks">
    <thead>
        <tr>
            <th><strong>Contact Form 7 Validation Rules</strong></th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td>  
			<ol class="validation_rules">
				<li>
					<strong>Email validation</strong>
					Just use the standard Contact Form 7 email field. For further control, include a "level" setting:
					<ul>
						<li><strong>[email* your-email level:Syntax]</strong> - validates the syntax of the email address (quickest)</li>
						<li><strong>[email* your-email level:MX]</strong> - validates the domain name (right hand part) the email address (default)</li>
						<li><strong>[email* your-email level:Server]</strong> - validates the mail servers for the domain are alive</li>
						<li><strong>[email* your-email level:Address]</strong> - validates the full email address (slowest)</li>
					</ul>
				</li>
				
				<li>
					<strong>Telephone number validation</strong>
					Just use the standard Contact Form 7 tel field. For further control, include "mobile", "landline" and "country" settings:
					<ul>
						<li><strong>[tel* your-tel mobile:true]</strong> - if the telephone number is a mobile, use the enhanced Mobile Validation service to perform more detailed validation</li>
						<li><strong>[tel* your-tel landline:true]</strong> - if the telephone number is a UK landline, use the enhanced Landline Validation service to perform more detailed validation</li>
						<li><strong>[tel* your-tel country:US]</strong> - if the telephone number does not include an explicit country code prefix, use the country specified in this option instead. Defaults to GB</li>
						<li><strong>[tel* your-tel allowedPrefixes:+441_+442]</strong> - if included only these prefixes will be allowed</li>
						<li><strong>[tel* your-tel barredPrefixes:+01_+447]</strong> - if included these prefixes will NOT be allowed</li>
					</ul>
				</li>
				
				<li>
					<strong>PredictiveAddress&trade;</strong>
					Use standard Contact Form 7 text fields to store your customer's address details. Add classes to indicate which fields should have PredictiveAddress&trade; functionality added to them:
					<ul>
						<li><strong>[text add1 class:d8pa_search class:d8pa_line1]</strong> - d8pa_search converts the text box into an auto-completing address box, and d8pa_line1 ensures the first line of the address is stored in this field once a full address is selected.</li>
						<li><strong>[text add2 class:d8pa_line2]</strong> - d8pa_line2 ensures the second line of the address is stored in this field once a full address is selected. Repeat with increasing numbers as required.</li>
						<li><strong>[text town class:d8pa_town]</strong> - d8pa_town ensures the town name from the selected address is stored in this field. Ignore this option if you do not mind which field holds the town name - it will be placed in the first available address line instead.</li>
						<li><strong>[text county class:d8pa_county]</strong> - d8pa_county ensures the county/state name from the selected address is stored in this field. Ignore this option if you do not mind which field holds the county name - it will be placed in the first available address line instead.</li>
						<li><strong>[text postcode class:d8pa_postcode]</strong> - d8pa_postcode ensures the postcode/zip code from the selected address is stored in this field.</li>
						<li><strong>[text country class:d8pa_country]</strong> - d8pa_country ensures the country name for the selected address is stored in this field.</li>
					</ul>
					If you have more than one set of address entry fields in the same form, keep them separate by using a unique number
					after d8pa, e.g. your billing address fields might be d8pa1_add1, d8pa1_add2, d8pa1_town, d8pa1_county and d8pa1_postcode
					while your shipping address fields might be d8pa2_add1, d8pa2_add2, d8pa2_town, d8pa2_county and d8pa2_postcode.
				</li>
				
				<li>
					<strong>SalaciousName&trade;</strong>
					Use standard Contact Form 7 text fields with the setting "name_type:FullName".
					<ul>
						<li><strong>[text txt_name name_type:FullName]</strong> - This indicates what field to apply the SalaciousName check on.</li>
					</ul>
				</li>
			</ol>
		</td>
	</tr>
	</tbody>
</table>
<br/>