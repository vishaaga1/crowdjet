<?php

if (get_option('d8cf7_predictiveaddress')) {
	add_filter( 'woocommerce_after_checkout_billing_form', 'd8_predictiveaddress_wc', 10, 2 );
	add_filter( 'woocommerce_after_checkout_shipping_form', 'd8_predictiveaddress_wc', 10, 2 );
	add_filter( 'woocommerce_after_edit_account_address_form', 'd8_predictiveaddress_wc', 10, 2 );
}

function d8_predictiveaddress_wc($checkout) {
	$ajaxKey = get_option('d8cf7_ajax_key');
	wp_register_script('d8pawc', 'https://webservices.data-8.co.uk/javascript/predictiveaddress_wc.js', array('jquery', 'd8pa'), null, true);
	wp_localize_script('d8pawc', 'd8pawc_script_vars', array('ajaxKey' => $ajaxKey));
	wp_enqueue_script('d8pawc');
}

?>