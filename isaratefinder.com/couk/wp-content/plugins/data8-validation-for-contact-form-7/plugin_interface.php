<?php
// create custom plugin settings menu
add_action('admin_menu', 'd8cf7_create_menu');
add_action('admin_init', 'register_d8cf7settings');

function d8cf7_create_menu() {
	add_menu_page("Data8 Validation", "Data8 Validation", "administrator", "d8_value_setting-panel", "d8cf7_settings_page", null, 66);
}

function register_d8cf7settings() {
	register_setting('d8cf7-settings-group', 'd8cf7_username');
	register_setting('d8cf7-settings-group', 'd8cf7_password');
	register_setting('d8cf7-autosettings-group', 'd8cf7_ajax_key');
	register_setting('d8cf7-autosettings-group', 'd8cf7_email_validation');
	register_setting('d8cf7-autosettings-group', 'd8cf7_telephone_validation');
	register_setting('d8cf7-autosettings-group', 'd8cf7_predictiveaddress');
	register_setting('d8cf7-autosettings-group', 'd8cf7_error');
}

function d8cf7_settings_page() {
	include('includes/d8cf7_header.php');
	include('includes/d8cf7_settings.php');
	include('includes/d8cf7_instructions.php');
	include('includes/d8cf7_footer.php');	
}

function d8cf7_settings_link($links) { 
  $settings_link = "<a href=\"admin.php?page=d8_value_setting-panel\">Settings</a>"; 
  array_unshift($links, $settings_link); 
  return $links; 
}

function d8cf7_password_changing($new_value, $old_value) {
	if ($new_value == '') {
		$new_value = $old_value;
	}
	
	$d8cf7_username = get_option('d8cf7_username');
	
	$params = array(
		'username' => $d8cf7_username,
		'password' => $new_value
	);
	
	$client = new SoapClient('https://webservices.data-8.co.uk/useradmin.asmx?WSDL', array('features' => SOAP_SINGLE_ELEMENT_ARRAYS));
	$wsresult = $client->GetAjaxKey($params);
	
	if (isset($wsresult->GetAjaxKeyResult->AjaxKey)) {
		update_option('d8cf7_ajax_key', $wsresult->GetAjaxKeyResult->AjaxKey);
		
		$wsresult = $client->GetAvailableServices($params);
		$emailValidation = false;
		$telephoneValidation = false;
		$predictiveAddress = false;
		$salaciousName = false;
		
		for ($i = 0; $i < count($wsresult->GetAvailableServicesResult->ServiceStatusOutputDesc); $i++) {
			if ($wsresult->GetAvailableServicesResult->ServiceStatusOutputDesc[$i]->Status->Success) {
				if ($wsresult->GetAvailableServicesResult->ServiceStatusOutputDesc[$i]->ServiceName == 'EmailValidation')
					$emailValidation = true;
				else if ($wsresult->GetAvailableServicesResult->ServiceStatusOutputDesc[$i]->ServiceName == 'InternationalTelephoneValidation')
					$telephoneValidation = true;
				else if ($wsresult->GetAvailableServicesResult->ServiceStatusOutputDesc[$i]->ServiceName == 'PredictiveAddress')
					$predictiveAddress = true;
				else if ($wsresult->GetAvailableServicesResult->ServiceStatusOutputDesc[$i]->ServiceName == 'SalaciousName')
					$salaciousName = true;
			}
		}
		
		update_option('d8cf7_email_validation', $emailValidation);
		update_option('d8cf7_telephone_validation', $telephoneValidation);
		update_option('d8cf7_predictiveaddress', $predictiveAddress);
		update_option('d8cf7_salaciousName', $salaciousName);
		
		$hostParts = explode(':', $_SERVER['HTTP_HOST']);
		$params['domain'] = $hostParts[0];
		$wsresult = $client->AddAllowedAjaxDomain($params);
		
		update_option('d8cf7_error', '');
	}
	else {
		$new_value = $old_value;
		update_option('d8cf7_error', $wsresult->GetAjaxKeyResult->Status->ErrorMessage);
	}
	
	return $new_value;
}

add_filter("pre_update_option_d8cf7_password", 'd8cf7_password_changing', 10, 2);

require_once plugin_dir_path(__FILE__) . '/includes/d8cf7-validation.php';
require_once plugin_dir_path(__FILE__) . '/includes/d8wc-validation.php';
require_once plugin_dir_path(__FILE__) . '/includes/d8gf-validation.php';