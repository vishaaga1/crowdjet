<?php
theme_register_sidebar('Area-3',  __('CTA - Bottom Above Footer Widget Area', 'default'));

function theme_block_1_85($title = '', $content = '', $class = '', $id = ''){
    ob_start();
?>
    <div class=" bd-block bd-own-margins <?php echo $class; ?>" id="<?php echo $id; ?>" data-block-id="<?php echo $id; ?>">
    <?php if (!theme_is_empty_html($title)){ ?>
    
    <div class=" bd-blockheader bd-tagstyles">
        <h4><?php echo $title; ?></h4>
    </div>
    
<?php } ?>
    <div class=" bd-blockcontent bd-tagstyles <?php if (theme_is_search_widget($id)) echo ' shape-only'; ?>">
<?php echo $content; ?>
</div>
</div>
<?php
    return ob_get_clean();
}
?>